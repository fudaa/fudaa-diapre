/*
 * @creation     2000-09-22
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.diapre;

import org.fudaa.dodico.corba.diapre.IResultatsDiapre;
import org.fudaa.dodico.corba.diapre.IResultatsDiapreOperations;
import org.fudaa.dodico.corba.diapre.SResultatsDiapre;
import org.fudaa.dodico.corba.diapre.SParametresDiapre;
import org.fudaa.dodico.corba.diapre.SPointDiagrammePression;
import org.fudaa.dodico.corba.diapre.SAngleRupture;
import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fortran.FortranReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Classe d'implentation de l'interface IResultatsDiapre gerant les resultats du code de calcul <code>diapre</code>.
 *
 * @version $Id: DResultatsDiapre.java,v 1.9 2006-09-19 14:42:29 deniger Exp $
 * @author Fouzia Elouafi , Jean de Malafosse
 */
public class DResultatsDiapre extends DResultats implements IResultatsDiapreOperations, IResultatsDiapre {
  /**
   * Le pas ???
   */
  // private int pasImp_;
  /**
   * variable indiquant si la lecture d'un fichier a ete correcte???
   */
  private boolean dirty05_;
  private boolean dirtyIO_;
  private boolean dirtyOUT_;
  private SResultatsDiapre resultats_;

  /**
   * Initialise des variables internes.
   */
  public DResultatsDiapre() {
    super();
    // pasImp_= 1;
    resultats_ = null;
    dirty05_ = true;
    dirtyIO_ = true;
    dirtyOUT_ = true;
  }

  /**
   * Teste 3 booleens internes ( ou logique).
   *
   * @return resultats du "ou logique" entre 3 variables internes.
   */
  public boolean isDirty() {
    return dirty05_ || dirtyIO_ || dirtyOUT_;
  }

  /**
   * Pas implenter completement.
   *
   * @return <code>new DResultatsDiapre()</code>
   */
  public final Object clone() throws CloneNotSupportedException {
    return new DResultatsDiapre();
  }

  /**
   * Renvoie la structure de resultats utilisee.
   */
  public SResultatsDiapre resultatsDiapre() {
    return resultats_;
  }

  /**
   * Modifie la structure de resultats utilisee.
   *
   * @param _r la nouvelle structure de resultat.
   */
  public void resultatsDiapre(final SResultatsDiapre _r) {
    resultats_ = _r;
  }

  /**
   * Affecte les 3 booleens internes a false.
   */
  public void setDirty() {
    dirty05_ = true;
    dirtyIO_ = true;
    dirtyOUT_ = true;
  }

  /**
   * @param _params les parametres correspondants
   * @param _fichier le fichier a lire
   * @return les resultats lus
   * @throws IOException erreurs io lors de la lecture
   */
  public static SResultatsDiapre litResultatsDiapre(final SParametresDiapre _params, final String _fichier) throws IOException {
    final SResultatsDiapre resultatDiapre = new SResultatsDiapre();
    System.out.println("lecture fichier d");
    litResultatsDiapreD(_params, resultatDiapre, _fichier);
    System.out.println("fin lecture fichier d");
    System.out.println("lecture fichier dat");
    litResultatsDiapreDat(_params, resultatDiapre, _fichier);
    System.out.println("fin lecture fichier dat");
    return resultatDiapre;
  }

  /**
   * Lit des resultats dans un fichier "resultat.d" pour construire la structure renvoyee <code>SResultatsDiapre</code>.
   * La structure <code>_param</code> sert a specifier le nombre de segments par ecran:
   * <code>SParametresDiapre.options.nombreSegmentsEcran</code> (cf diapre.idl).
   *
   * @param _params indique le nombre de segment par ecran.
   * @param _results les resultats partiels ?
   * @param _fichier le fichier a lire
   * @return structure construire a partir du fichier.
   * @throws IOException
   */
  public static SResultatsDiapre litResultatsDiapreD(final SParametresDiapre _params, final SResultatsDiapre _results,
      final String _fichier) throws IOException {
    if (_results == null) {
      throw new RuntimeException("SResultats non initialis�s");
    }
    final SResultatsDiapre resultatDiapre = _results;
    System.out.println("litResultatsDiapre_d");
    if (_params.options == null) {
      System.out.println("options nulles");
    }
    System.out.println("nb " + _params.options.nombreSegmentsEcran);
    resultatDiapre.pointsDiagrammePression = new SPointDiagrammePression[_params.options.nombreSegmentsEcran];
    for (int i = 0; i < _params.options.nombreSegmentsEcran; i++) {
      resultatDiapre.pointsDiagrammePression[i] = new SPointDiagrammePression();
    }
    final int[] fmt = { 14, 14, 14 };
    // ouverture du fichier
    final FortranReader fichier = new FortranReader(new FileReader(_fichier + ".d"));
    // lecture des donnees du fichier
    for (int i = 0; i < _params.options.nombreSegmentsEcran; i++) {
      fichier.readFields(fmt);
      resultatDiapre.pointsDiagrammePression[i].ordonneeMilieuSegment = fichier.doubleField(0);
      resultatDiapre.pointsDiagrammePression[i].pressionEffective = fichier.doubleField(1);
      resultatDiapre.pointsDiagrammePression[i].pressionInterstitielle = fichier.doubleField(2);
      resultatDiapre.pointsDiagrammePression[i].pressionTotale = resultatDiapre.pointsDiagrammePression[i].pressionEffective
          + resultatDiapre.pointsDiagrammePression[i].pressionInterstitielle;
    }
    return resultatDiapre;
  }

  /**
   * Lit des resultats dans un fichier "resultat.dat" pour construire la structure renvoyee
   * <code>SResultatsDiapre</code>. La structure <code>_param</code> sert a specifier le nombre de segments par
   * ecran: <code>SParametresDiapre.options.nombreSegmentsEcran</code> (cf diapre.idl).
   *
   * @param _params indique le nombre de segment par ecran.
   * @param _results les resultats a modifier et a retourner
   * @param _fichier le fichier a lire
   * @return structure construire a partir du fichier.
   * @throws IOException
   */
  public static SResultatsDiapre litResultatsDiapreDat(final SParametresDiapre _params, final SResultatsDiapre _results,
      final String _fichier) throws IOException {
    if (_results == null) {
      throw new RuntimeException("SResultats non initialis�s");
    }
    final SResultatsDiapre resultatDiapre = _results;
    final int nbSegments = _params.options.nombreSegmentsEcran;
    final int nbCouches = _params.sol.nombreCouches;
    System.out.println(nbSegments);
    System.out.println(nbCouches);
    final int[] fmt = new int[nbCouches + 1];
    fmt[0] = 4;
    for (int i = 1; i < nbCouches + 1; i++) {
      fmt[i] = 14;
    }
    resultatDiapre.anglesRupture = new SAngleRupture[nbSegments][nbCouches + 1];
    System.out.println("Lecture des r�sultats dans le fichier " + _fichier + ".dat");
    // ouverture du fichier
    final FortranReader fichier = new FortranReader(new FileReader(_fichier + ".dat"));
    // lecture des donnees du fichier
    for (int i = 0; i < nbSegments; i++) {
      fichier.readFields(fmt);
      // System.out.println("lecture ligne "+i);
      // System.out.println("lecture colonne 0...");
      resultatDiapre.anglesRupture[i][0] = new SAngleRupture();
      resultatDiapre.anglesRupture[i][0].numeroSegment = fichier.intField(0);
      for (int j = 1; j < nbCouches + 1; j++) {
        // System.out.println("lecture colonne "+j);
        resultatDiapre.anglesRupture[i][j] = new SAngleRupture();
        try {
          resultatDiapre.anglesRupture[i][j].angleRupture = fichier.doubleField(j);
        } catch (final NumberFormatException _e) {
          resultatDiapre.anglesRupture[i][j].angleRupture = 0;
          System.err.println("L'angle du segment n�" + resultatDiapre.anglesRupture[i][0].numeroSegment
              + "pour la couche " + j + " n'est pas lue ( mise �  0)");
        }
      }
    }
    return resultatDiapre;
  }
}
