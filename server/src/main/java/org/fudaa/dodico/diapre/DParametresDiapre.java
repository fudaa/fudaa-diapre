/*
 * @creation 2000-09-21
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.diapre;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.diapre.*;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Classe implementant l'interface IParametresDiapre gerant les parametres du code de calcul diapre.
 *
 * @version $Id: DParametresDiapre.java,v 1.13 2006-09-19 14:42:29 deniger Exp $
 * @author Fouzia Elouafi , Jean de Malafosse
 */
public class DParametresDiapre extends DParametres implements IParametresDiapreOperations, IParametresDiapre {

  /**
   * Structure decrivant les parametres du calcul.
   */
  private SParametresDiapre parametreDiapre_;

  public DParametresDiapre() {
    super();
  }

  /**
   * Lit le fichier <code>nomFichierEntree</code> specifie dans la structure <code>SParametresDiapre</code>
   * utilisee.
   *
   * @return true si la fichier a ete lu.
   */
  /*
   * private boolean chargeFichiers() { boolean charge = false; try { File f01 = new
   * File(parametreDiapre_.nomFichierEntree); if(f01.exists() && f01.canRead()) {
   * parametresDiapre(litParametresDiapre(parametreDiapre_.nomFichierEntree)); charge = true; } } catch(Exception e) {
   * charge = false; } return charge; }
   */
  /**
   * Pas implanter completement.
   *
   * @return <code>DParametresDiapre()</code>
   */
  public final Object clone() throws CloneNotSupportedException {
    return new DParametresDiapre();
  }

  /**
   * Chaine descriptive de l'objet.
   *
   * @return "DParametresDiapre"
   */
  public String toString() {
    final String s = "DParametresDiapre";
    return s;
  }

  /**
   * La structure utilisee pour decrire les parametres.
   */
  public SParametresDiapre parametresDiapre() {
    return parametreDiapre_;
  }

  /**
   * Modifie la structure des parametres.
   *
   * @param _p la nouvelle structure.
   */
  public void parametresDiapre(final SParametresDiapre _p) {
    parametreDiapre_ = _p;
  }

  /**
   * Ecrit les parametres <code>_params</code> dans le fichier de nom <code>_nomfichier</code>, d'extension ".dia"
   * et de chemin <code>_path</code>. Le format "fortran" est utilise.
   *
   * @param _nomFichier le fichier destination(sans extension).
   * @param _params les parametres a ecrire dans le fichier.
   * @throws IOException clair!
   */
  public static void ecritParametresDiapre(final SParametresDiapre _params, final String _nomFichier) throws IOException {
    // String path = new String("/home/users/malafosse/devel/dodico/sources/");
    System.out.println("Ecriture des parametres de DIAPRE");
    final FortranWriter f01 = new FortranWriter(new FileWriter(_nomFichier + ".dia"));
    if (System.getProperty("os.name").startsWith("Windows")) {
      f01.setLineSeparator("\r\n");
    } // fichiers DOS (\r)
    else {
      f01.setLineSeparator("\n");
    }
    final int[] fmtI = new int[] { 82, 80 };
    ecritTitres(_params, f01, fmtI);
    f01.stringField(0, _params.parametresGeneraux.choixButeePoussee + "");
    f01.stringField(1, "   TYPE DE CALCUL (P:POUSSEE, B:BUTEE))");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.parametresGeneraux.deltaSurPhi + "");
    f01.stringField(1, "DELTA/PHI (POUSSEE, DEF=BUTEE)");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.parametresGeneraux.choixCalcul + "");
    f01.stringField(1, "L:LONG TERME, C:COURT TERME");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.sol.nombreCouches + "");
    f01.stringField(1, "NOMBRE DE COUCHES DE SOL[1, 10]");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.sol.interfaceCoucheSol + "");
    f01.stringField(1, "INTERFACE ENTRE LES COUCHES DE SOL");
    f01.writeFields(fmtI);
    // premiere couche (a part...)
    f01.stringField(0, _params.sol.premiereCouche.poidsVolumiqueTotal + "");
    f01.stringField(1, "   POIDS VOLUMIQUE TOTAL DE LA COUCHE  1 (t/m3)");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.sol.premiereCouche.poidsVolumique + "");
    f01.stringField(1, "POIDS VOLUMIQUE DE LA COUCHE  1 (t/m3");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.sol.premiereCouche.cohesion + "");
    f01.stringField(1, "COHESION DU SOL DE LA COUCHE  1 (t/m2)");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.sol.premiereCouche.angleFrottementInterne + "");
    f01.stringField(1, "FROTTEMENT INTERNE DU SOL DE LA COUCHE  1 (t/m2)");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.sol.premiereCouche.limiteInferieureCouche + "");
    f01.stringField(1, "LIMITE INFERIEURE DE LA COUCHE  1 (m)");
    f01.writeFields(fmtI);
    for (int i = 0; i < _params.sol.nombreCouches - 1; i++) {
      final int j = i + 2;
      f01.stringField(0, _params.sol.autresCouches[i].poidsVolumiqueTotal + "");
      f01.stringField(1, "   POIDS VOLUMIQUE TOTAL DE LA COUCHE  " + j + " (t/m3, DEF=END)");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.sol.autresCouches[i].poidsVolumique + "");
      f01.stringField(1, "POIDS VOLUMIQUE DE LA COUCHE  " + j + " (t/m3, DEF=END)");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.sol.autresCouches[i].cohesion + "");
      f01.stringField(1, "COHESION DU SOL DE LA COUCHE  " + j + " (t/m2)");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.sol.autresCouches[i].angleFrottementInterne + "");
      f01.stringField(1, "FROTTEMENT INTERNE DU SOL DE LA COUCHE " + j + " (t/m2)");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.sol.autresCouches[i].limiteInferieureCouche + "");
      f01.stringField(1, "LIMITE INFERIEURE DE LA COUCHE  " + j + " (m)");
      f01.writeFields(fmtI);
    }
    // nappe d'eau
    f01.stringField(0, _params.eau.presenceNappeEau + "");
    f01.stringField(1, "   PRESENCE D'UNE NAPPE D'EAU");
    f01.writeFields(fmtI);
    if (_params.eau.presenceNappeEau != 0) {
      f01.stringField(0, _params.eau.typeCalculEau + "");
      f01.stringField(1, "TYPE DE CALCUL (S: STATIQUE, D: DYNAMIQUE)");
      f01.writeFields(fmtI);
      final boolean test = _params.eau.typeCalculEau.equals("S");
      if (test) {
        f01.stringField(0, _params.eau.coteNappeEau + "");
        f01.stringField(1, "COTE DE LA NAPPE D'EAU (m)");
        f01.writeFields(fmtI);
      }
      final boolean test0 = _params.eau.typeCalculEau.equals("D");
      if (test0) {
        f01.stringField(0, _params.eau.moyenCalcul + "");
        f01.stringField(1, "MOYEN DE CALCUL (F:FICHIER, A:ABAQUE)");
        f01.writeFields(fmtI);
        final boolean test1 = _params.eau.moyenCalcul.equals("F");
        if (test1) {
          f01.stringField(0, _params.eau.nomFichier + "");
          f01.stringField(1, "NOM DU FICHIER POUR LA NAPPE D'EAU");
          f01.writeFields(fmtI);
        }
      }
    }
    // lineiques
    ecritParamLineique(_params, f01, fmtI);
    // uniforme (seule)
    ecritParamUniforme(_params, f01, fmtI);
    // trapezoidales
    f01.stringField(0, _params.surchargesProjet.nombreSurchargesTrapezoidales + "");
    f01.stringField(1, "   NOMBRE DE SURCHARGES TRAPEZOIDALES");
    f01.writeFields(fmtI);
    for (int i = 0; i < _params.surchargesProjet.nombreSurchargesTrapezoidales; i++) {
      final int j = i + 1;
      f01.stringField(0, _params.surchargesProjet.surchargesTrapezoidales[i].abscisseDebut + "");
      f01.stringField(1, "ABSCISSE DE DEBUT DE LA SURCHARGE TRAPEZOIDALE " + j + "(m)");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.surchargesProjet.surchargesTrapezoidales[i].abscisseFin + "");
      f01.stringField(1, "ABSCISSE DE FIN DE LA SURCHARGE TRAPEZOIDALE " + j + "(m)");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.surchargesProjet.surchargesTrapezoidales[i].forceVerticaleDebut + "");
      f01.stringField(1, "FORCE VERTICALE DE DEBUT DE LA SURCHARGE TRAPEZOIDALE " + j + " (t/m.m )");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.surchargesProjet.surchargesTrapezoidales[i].forceHorizontaleDebut + "");
      f01.stringField(1, "FORCE HORIZONTALE DE DEBUT DE LA SURCHARGE TRAPEZOIDALE " + j + " (t/m.m )");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.surchargesProjet.surchargesTrapezoidales[i].forceVerticaleFin + "");
      f01.stringField(1, "FORCE VERTICALE DE FIN DE LA SURCHARGE TRAPEZOIDALE " + j + " (t/m.m)");
      f01.writeFields(fmtI);
      f01.stringField(0, _params.surchargesProjet.surchargesTrapezoidales[i].forceHorizontaleFin + "");
      f01.stringField(1, "IFORCE HORIZONTALE DE FIN DE LA SURCHARGE TRAPEZOIDALE " + j + " (t/m.m)");
      f01.writeFields(fmtI);
    }
    // Options
    f01.stringField(0, _params.options.nombreSegmentsEcran + "");
    f01.stringField(1, "   NOMBRE DE SEGMENTS DISCRETISANT L'ECRAN");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.options.largeurSegmentDebut + "");
    f01.stringField(1, "LARGEUR DU SEGMENT DE DEBUT (m)");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.options.nombreSegmentsTalus + "");
    f01.stringField(1, "NOMBRE DE SEGMENTS DISCRETISANT LE TALUS");
    f01.writeFields(fmtI);
    f01.stringField(0, _params.options.ligneGlissement + "");
    f01.stringField(1, "LIGNE DE GLISSEMENT (D: DROITE, S: SEGMENT)");
    f01.writeFields(fmtI);
    f01.flush();
  }

  private static void ecritParamUniforme(final SParametresDiapre _params, final FortranWriter _writer, final int[] _fmtI) throws IOException {
    _writer.stringField(0, _params.surchargesProjet.nombreSurchargeUniforme + "");
    _writer.stringField(1, "   PRESENCE D'UNE SURCHARGE UNIFORME");
    _writer.writeFields(_fmtI);
    if (_params.surchargesProjet.nombreSurchargeUniforme != 0) {
      _writer.stringField(0, _params.surchargesProjet.surchargeUniforme.forceVerticale + "");
      _writer.stringField(1, "FORCE VERTICALE DE LA SURCHARGE UNIFORME   (t/m.m , DEF=END)");
      _writer.writeFields(_fmtI);
      _writer.stringField(0, _params.surchargesProjet.surchargeUniforme.forceHorizontale + "");
      _writer.stringField(1, "FORCE HORIZONTALE DE LA SURCHARGE UNIFORME   (t/m.m, DEF=END)");
      _writer.writeFields(_fmtI);
    }
  }

  private static void ecritParamLineique(final SParametresDiapre _params, final FortranWriter _writer, final int[] _fmtI) throws IOException {
    _writer.stringField(0, _params.surchargesProjet.nombreSurchargesLineiques + "");
    _writer.stringField(1, "   NOMBRE DE SURCHARGES LINEIQUES ");
    _writer.writeFields(_fmtI);
    for (int i = 0; i < _params.surchargesProjet.nombreSurchargesLineiques; i++) {
      final int j = i + 1;
      _writer.stringField(0, _params.surchargesProjet.surchargesLineiques[i].abscisse + "");
      _writer.stringField(1, "ABSCISSE DE LA SURCHARGE LINEIQUE " + j + " (m)");
      _writer.writeFields(_fmtI);
      _writer.stringField(0, _params.surchargesProjet.surchargesLineiques[i].forceVerticale + "");
      _writer.stringField(1, "FORCE VERTICALE DE LA SURCHARGE LINEIQUE  " + j + " (t/m lineaire, DEF=END)");
      _writer.writeFields(_fmtI);
      _writer.stringField(0, _params.surchargesProjet.surchargesLineiques[i].forceHorizontale + "");
      _writer.stringField(1, "FORCE HORIZONTALE DE LA SURCHARGE LINEIQUE  " + j + " (t/m lineaire, DEF=END)");
      _writer.writeFields(_fmtI);
    }
  }

  private static void ecritTitres(final SParametresDiapre _params, final FortranWriter _writer, final int[] _fmt) throws IOException {
    _writer.stringField(0, _params.commentairesDiapre.titre1 + "");
    _writer.stringField(1, "TITRE1");
    _writer.writeFields(_fmt);
    _writer.stringField(0, _params.commentairesDiapre.titre2 + "");
    _writer.stringField(1, "TITRE2");
    _writer.writeFields(_fmt);
    _writer.stringField(0, _params.commentairesDiapre.titre3 + "");
    _writer.stringField(1, "TITRE3");
    _writer.writeFields(_fmt);
    _writer.stringField(0, _params.commentairesDiapre.titre4 + "");
    _writer.stringField(1, "TITRE4");
    _writer.writeFields(_fmt);
    _writer.stringField(0, _params.commentairesDiapre.titre5 + "");
    _writer.stringField(1, "TITRE5");
    _writer.writeFields(_fmt);
    _writer.stringField(0, _params.talusDiapre.nombrePointsTalus + "");
    _writer.stringField(1, "NOMBRES DE POINTS DU TALUS");
    _writer.writeFields(_fmt);
    for (int i = 0; i < _params.talusDiapre.nombrePointsTalus; i++) {
      final int j = i + 1;
      _writer.stringField(0, _params.talusDiapre.pointsTalus[i].abscissePointDiapre + "");
      _writer.stringField(1, "ABSCISSE DU POINT  " + j + " (m, DEF=END)");
      _writer.writeFields(_fmt);
      _writer.stringField(0, _params.talusDiapre.pointsTalus[i].ordonneePointDiapre + "");
      _writer.stringField(1, "ORDONNEE DU POINT  " + j + " (m)");
      _writer.writeFields(_fmt);
    }
  }

  /**
   * Construit une structure <code>SParametresDiapre</code> a partir des donnees du fichier <code>_nomFichier</code>
   * et d'extension ".dia".
   *
   * @param _nomFichier le nom du fichier a lire.
   * @return la structure construite a partir des donnees du fichier.
   * @throws IOException clair!
   */
  public static SParametresDiapre litParametresDiapre(final String _nomFichier) throws IOException {
    System.out.println("Lecture des parametres Diapre");
    final SParametresDiapre params = new SParametresDiapre();
    final FortranReader f01 = new FortranReader(new FileReader(_nomFichier + ".dia"));
    // initialisation
    final int[] fmtI = preLitParamDiapre(params);
    // Commentaires
    f01.readFields(fmtI);
    params.commentairesDiapre.titre1 = f01.stringField(0);
    f01.readFields(fmtI);
    params.commentairesDiapre.titre2 = f01.stringField(0);
    f01.readFields(fmtI);
    params.commentairesDiapre.titre3 = f01.stringField(0);
    f01.readFields(fmtI);
    params.commentairesDiapre.titre4 = f01.stringField(0);
    f01.readFields(fmtI);
    params.commentairesDiapre.titre5 = f01.stringField(0);
    // talus
    f01.readFields(fmtI);
    params.talusDiapre.nombrePointsTalus = f01.intField(0);
    for (int i = 0; i < params.talusDiapre.nombrePointsTalus; i++) {
      f01.readFields(fmtI); // abscisse i
      params.talusDiapre.pointsTalus[i].abscissePointDiapre = f01.doubleField(0);
      f01.readFields(fmtI); // ordonnee i
      params.talusDiapre.pointsTalus[i].ordonneePointDiapre = f01.doubleField(0);
    }
    // parametres generaux
    f01.readFields(fmtI);
    params.parametresGeneraux.choixButeePoussee = f01.stringField(0);
    f01.readFields(fmtI);
    params.parametresGeneraux.deltaSurPhi = f01.doubleField(0);
    f01.readFields(fmtI);
    params.parametresGeneraux.choixCalcul = f01.stringField(0);
    // sols
    f01.readFields(fmtI);
    params.sol.nombreCouches = f01.intField(0);
    f01.readFields(fmtI);
    params.sol.interfaceCoucheSol = f01.stringField(0);
    // couche 1
    f01.readFields(fmtI);
    params.sol.premiereCouche.poidsVolumiqueTotal = f01.doubleField(0);
    f01.readFields(fmtI);
    params.sol.premiereCouche.poidsVolumique = f01.doubleField(0);
    f01.readFields(fmtI);
    params.sol.premiereCouche.cohesion = f01.doubleField(0);
    f01.readFields(fmtI);
    params.sol.premiereCouche.angleFrottementInterne = f01.doubleField(0);
    f01.readFields(fmtI);
    params.sol.premiereCouche.limiteInferieureCouche = f01.doubleField(0);
    // autres couches
    for (int i = 0; i < params.sol.nombreCouches - 1; i++) {
      f01.readFields(fmtI);
      params.sol.autresCouches[i].poidsVolumiqueTotal = f01.doubleField(0);
      f01.readFields(fmtI);
      params.sol.autresCouches[i].poidsVolumique = f01.doubleField(0);
      f01.readFields(fmtI);
      params.sol.autresCouches[i].cohesion = f01.doubleField(0);
      f01.readFields(fmtI);
      params.sol.autresCouches[i].angleFrottementInterne = f01.doubleField(0);
      f01.readFields(fmtI);
      params.sol.autresCouches[i].limiteInferieureCouche = f01.doubleField(0);
    }
    // nappe d'eau
    f01.readFields(fmtI);
    params.eau.presenceNappeEau = f01.intField(0);
    if (params.eau.presenceNappeEau != 0) {
      f01.readFields(fmtI);
      params.eau.typeCalculEau = f01.stringField(0);
      final boolean test2 = params.eau.typeCalculEau.equals("S");
      if (test2) {
        f01.readFields(fmtI);
        params.eau.coteNappeEau = f01.doubleField(0);
      }
      final boolean test21 = params.eau.typeCalculEau.equals("D");
      if (test21) {
        f01.readFields(fmtI);
        params.eau.moyenCalcul = f01.stringField(0);
        final boolean test3 = params.eau.moyenCalcul.equals("F");
        if (test3) {
          f01.readFields(fmtI);
          params.eau.nomFichier = f01.stringField(0);
        }
      }
    }
    // lineiques
    f01.readFields(fmtI);
    params.surchargesProjet.nombreSurchargesLineiques = f01.intField(0);
    for (int i = 0; i < params.surchargesProjet.nombreSurchargesLineiques; i++) {
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesLineiques[i].abscisse = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesLineiques[i].forceVerticale = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesLineiques[i].forceHorizontale = f01.doubleField(0);
    }
    // uniforme
    f01.readFields(fmtI);
    params.surchargesProjet.nombreSurchargeUniforme = f01.intField(0);
    if (params.surchargesProjet.nombreSurchargeUniforme != 0) {
      f01.readFields(fmtI);
      params.surchargesProjet.surchargeUniforme.forceVerticale = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargeUniforme.forceHorizontale = f01.doubleField(0);
    }
    // trapezoidales
    f01.readFields(fmtI);
    params.surchargesProjet.nombreSurchargesTrapezoidales = f01.intField(0);
    for (int i = 0; i < params.surchargesProjet.nombreSurchargesTrapezoidales; i++) {
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesTrapezoidales[i].abscisseDebut = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesTrapezoidales[i].abscisseFin = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesTrapezoidales[i].forceVerticaleDebut = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesTrapezoidales[i].forceHorizontaleDebut = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesTrapezoidales[i].forceVerticaleFin = f01.doubleField(0);
      f01.readFields(fmtI);
      params.surchargesProjet.surchargesTrapezoidales[i].forceHorizontaleFin = f01.doubleField(0);
    }
    // options
    f01.readFields(fmtI);
    params.options.nombreSegmentsEcran = f01.intField(0);
    f01.readFields(fmtI);
    params.options.largeurSegmentDebut = f01.doubleField(0);
    f01.readFields(fmtI);
    params.options.nombreSegmentsTalus = f01.intField(0);
    f01.readFields(fmtI);
    params.options.ligneGlissement = f01.stringField(0);
    System.out.println("fin lecture");
    return params;
  }

  private static int[] preLitParamDiapre(final SParametresDiapre _params) {
    System.out.println("debut initialisation");
    _params.commentairesDiapre = new SCommentairesDiapre();
    _params.talusDiapre = new STalusDiapre();
    _params.talusDiapre.pointsTalus = new SPointDiapre[20];
    for (int i = 0; i < 20; i++) {
      _params.talusDiapre.pointsTalus[i] = new SPointDiapre();
    }
    _params.sol = new SSol();
    _params.sol.premiereCouche = new SCoucheSol();
    _params.sol.autresCouches = new SCoucheSol[10];
    for (int i = 0; i < 10; i++) {
      _params.sol.autresCouches[i] = new SCoucheSol();
    }
    _params.eau = new SEau();
    _params.surchargesProjet = new SSurchargesProjet();
    _params.surchargesProjet.surchargesLineiques = new SSurchargeLineique[10];
    _params.surchargesProjet.surchargeUniforme = new SSurchargeUniforme();
    _params.surchargesProjet.surchargesTrapezoidales = new SSurchargeTrapezoidale[10];
    for (int i = 0; i < 10; i++) {
      _params.surchargesProjet.surchargesLineiques[i] = new SSurchargeLineique();
      _params.surchargesProjet.surchargesTrapezoidales[i] = new SSurchargeTrapezoidale();
    }
    _params.options = new SOptions();
    _params.parametresGeneraux = new SParametresGeneraux();
    int[] fmtI;
    fmtI = new int[] { 82, 80 };
    return fmtI;
  }
}