/*
 * @creation     2000-09-22
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.diapre;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.diapre.ICalculDiapre;
import org.fudaa.dodico.corba.diapre.IParametresDiapre;
import org.fudaa.dodico.corba.diapre.IParametresDiapreHelper;
import org.fudaa.dodico.corba.diapre.IResultatsDiapre;
import org.fudaa.dodico.corba.diapre.IResultatsDiapreHelper;
import org.fudaa.dodico.corba.diapre.ICalculDiapreOperations;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.dodico.calcul.DCalcul;
/**
 * Une encapsulation du programme Diapre ecrit en Fortran.
 *
 * @version      $Id: DCalculDiapre.java,v 1.8 2006-09-19 14:42:29 deniger Exp $
 * @author       Fouzia Elouafi , Jean de Malafosse
 */
public class DCalculDiapre extends DCalcul implements ICalculDiapreOperations,ICalculDiapre {
  /**
   *  Initialisation des extensions de fichier utilisees:".dia, .dat, .d" .
   */
  public DCalculDiapre() {
    super();
    setFichiersExtensions(new String[] { ".dia", ".dat", ".d" });
  }
  /**
   * Chaine descriptive de l'objet.
   *
   * @return     "DCalculDiapre()"
   */
  public String toString() {
    return "DCalculDiapre()";
  }
  /**
   * Chaine descriptive du serveur de calcul Diapre.
   */
  public String description() {
    return "Diapre, serveur de calcul " + super.description();
  }
  /**
   * Lance le code de calcul apres verification de l'existence des interfaces
   * parametres et resultats. A la fin, les resultats sont lus et stockes dans
   * l'interface IResultats.
   *
   * @param      _c  connexion concernee par le calcul.
   */
  public void calcul(final IConnexion _c) {
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresDiapre params= IParametresDiapreHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsDiapre results= IResultatsDiapreHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    log(_c, "lancement du calcul");
    final String os= System.getProperty("os.name");
    //String drive;
    final String path= cheminServeur();
    final String fichier= "diapre" + _c.numero();
    //A FAIRE EN DERNIER
    try {
      String nomexec;
      if (os.startsWith("Win")) {
        nomexec= "diapre.bat";
      } else {
        nomexec= "diapre.sh";
      }
      final String[] param= new String[3];
      param[0]= path + nomexec;
      //fichier de description des parametres
      param[1]= path;
      param[2]= fichier;
      //ecriture fichier
      DParametresDiapre.ecritParametresDiapre(
        params.parametresDiapre(),
        path + fichier);
      //lancement calcul
      final CExec ex= new CExec(param);
      //redirection de la sortie
      //exec.setOutStream(System.out);
      System.out.println("--Debut du calcul--");
      //Lancement de l'execution
      ex.exec();
      //recuperation des resultats.
      System.out.println("--Fin du calcul--");
      results.resultatsDiapre(
        DResultatsDiapre.litResultatsDiapre(
          params.parametresDiapre(),
          path + fichier));
      System.out.println("--Fin du calcul--");
      log(_c, "calcul termin�");
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
