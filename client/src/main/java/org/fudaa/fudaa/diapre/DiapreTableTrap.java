/*
 * @file         DiapreTableTrap.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       jean de Malafosse
 */
public class DiapreTableTrap
  implements TableModel //extends AbstactTableModel implements TableModel
{
  double[][] tab= new double[10][10];
  public DiapreTableTrap() {}
  public int getColumnCount() {
    return 7;
  }
  public int getRowCount() {
    return 10;
  }
  public boolean isCellEditable(final int row, final int col) {
    return true;
  }
  public Object getValueAt(final int row, final int col) {
    double ret= 0;
    switch (col) {
      case 0 :
        switch (row) {
          case 0 :
            ret= 1;
            break;
          case 1 :
            ret= 2;
            break;
          case 2 :
            ret= 3;
            break;
          case 3 :
            ret= 4;
            break;
          case 4 :
            ret= 5;
            break;
          case 5 :
            ret= 6;
            break;
          case 6 :
            ret= 7;
            break;
          case 7 :
            ret= 8;
            break;
          case 8 :
            ret= 9;
            break;
          case 9 :
            ret= 10;
            break;
        }
        break;
      case 1 :
        ret= tab[row][0];
        break;
      case 2 :
        ret= tab[row][1];
        break;
      case 3 :
        ret= tab[row][2];
        break;
      case 4 :
        ret= tab[row][3];
        break;
      case 5 :
        ret= tab[row][4];
        break;
      case 6 :
        ret= tab[row][5];
        break;
    }
    return new Double(ret);
  }
  public String getColumnName(final int col) {
    String nomCol= null;
    System.out.println(col);
    switch (col) {
      case 0 :
        nomCol= "N�";
        break;
      case 1 :
        nomCol= "Abscisse d�but(m)";
        break;
      case 2 :
        nomCol= "Abscisse fin(m)";
        break;
      case 3 :
        nomCol= "Force V d�but (t/m^2)";
        break;
      case 4 :
        nomCol= "Force V fin (t/m^2)";
        break;
      case 5 :
        nomCol= "Force H d�but (t/m^2)";
        break;
      case 6 :
        nomCol= "Force H fin (t/m^2)";
        break;
    }
    return nomCol;
  }
  public void setValueAt(final Object obj, final int row, final int col) {
    try {
      final double temp= Double.parseDouble((String)obj);
      switch (col) {
        case 1 :
          tab[row][0]= temp;
          System.out.println("setValueAt");
          break;
        case 2 :
          tab[row][1]= temp;
          break;
        case 3 :
          tab[row][2]= temp;
          break;
        case 4 :
          tab[row][3]= temp;
          break;
        case 5 :
          tab[row][4]= temp;
          break;
        case 6 :
          tab[row][5]= temp;
          break;
      }
      //        fireTableRowsUpdated(0,10);
    } catch (final Exception e) {}
  }
  public Class getColumnClass(final int col) {
    return Double.class;
  }
  public void addTableModelListener(final TableModelListener _l) {}
  public void removeTableModelListener(final TableModelListener _l) {}
}
