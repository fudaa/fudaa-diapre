/*
 * @file         DiapreConstruitGrapheRappel.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;

import org.fudaa.dodico.corba.diapre.SParametresDiapre;
/**
 * L'implementation du client Diapre.
 *
 * @version      $Id: DiapreConstruitGrapheRappel.java,v 1.7 2006-09-19 15:02:13 deniger Exp $
 * @author       Fouzia Elouafi & Jean de Malafosse 
 */
public class DiapreConstruitGrapheRappel {
  BuCommonInterface appli_;
  public DiapreConstruitGrapheRappel(
    final BuCommonInterface appli,
    final BuInformationsDocument _id,
    final SParametresDiapre _params,
    final int choix) {
    appli_= appli.getImplementation();
    final SParametresDiapre params= _params;
    boolean eau= false;
    int i= 0;
    final double[][] valeursX=
      new double[2
        + params.sol.nombreCouches][params.talusDiapre.nombrePointsTalus];
    final double[][] valeursY=
      new double[2
        + params.sol.nombreCouches][params.talusDiapre.nombrePointsTalus];
    //-------------------talus-------------------------//
    for (int j= 0; j < params.talusDiapre.nombrePointsTalus; j++) {
      valeursX[0][j]= params.talusDiapre.pointsTalus[j].abscissePointDiapre;
      valeursY[0][j]= params.talusDiapre.pointsTalus[j].ordonneePointDiapre;
    }
    //----------------------equation du dernier segments----------------//
    final double[] A= new double[2];
    final double[] B= new double[2];
    double m, p;
    //point A
    A[0]=
      params.talusDiapre.pointsTalus[params.talusDiapre.nombrePointsTalus
        - 2].abscissePointDiapre;
    A[1]=
      params.talusDiapre.pointsTalus[params.talusDiapre.nombrePointsTalus
        - 2].ordonneePointDiapre;
    //point B
    B[0]=
      params.talusDiapre.pointsTalus[params.talusDiapre.nombrePointsTalus
        - 1].abscissePointDiapre;
    B[1]=
      params.talusDiapre.pointsTalus[params.talusDiapre.nombrePointsTalus
        - 1].ordonneePointDiapre;
    //equation de la droite
    m= (A[1] - B[1]) / (A[0] - B[0]);
    p= A[1] - m * A[0];
    //--------------------------sol-------------------------//
    if (params.sol != null) {
      valeursX[1][0]= valeursX[0][0];
      valeursY[1][0]= params.sol.premiereCouche.limiteInferieureCouche;
      if (A[0] != B[0]) {
        valeursX[1][1]= (valeursY[1][0] - p) / m;
      } else {
        valeursX[1][1]= A[0];
      }
      valeursY[1][1]= valeursY[1][0];
      for (i= 0; i < params.sol.nombreCouches - 1; i++) {
        valeursX[i + 2][0]= valeursX[0][0];
        valeursY[i + 2][0]= params.sol.autresCouches[i].limiteInferieureCouche;
        if (A[0] != B[0]) {
          valeursX[i + 2][1]= (valeursY[i + 2][0] - p) / m;
        } else {
          valeursX[i + 2][1]= A[0];
        }
        valeursY[i + 2][1]= valeursY[i + 2][0];
      }
    }
    //---------------------------eau------------------------//
    if (params.eau != null && params.eau.presenceNappeEau == 1) {
      eau= true;
      valeursX[i + 2][0]= valeursX[0][0];
      valeursY[i + 2][0]= params.eau.coteNappeEau;
      if (A[0] != B[0]) {
        valeursX[i + 2][1]= (params.eau.coteNappeEau - p) / m;
      } else {
        valeursX[i + 2][1]= A[0];
      }
      valeursY[i + 2][1]= params.eau.coteNappeEau;
    }
    //--------------------------surcharges------------------//
    boolean surchargeUniforme= false;
    final double[] surchargeUniformeF= new double[2];
    int surchargesLineiques= 0;
    final double[][] surchargeLineique=
      new double[params.surchargesProjet.nombreSurchargesLineiques][3];
    int surchargesTrapezoidales= 0;
    final double[][] surchargeTrapezoidale=
      new double[params.surchargesProjet.nombreSurchargesTrapezoidales][6];
    if (params.surchargesProjet != null) {
      //uniforme
      if (params.surchargesProjet.nombreSurchargeUniforme > 0
        && (choix == 0 || choix == 2)) {
        surchargeUniforme= true;
        surchargeUniformeF[0]=
          params.surchargesProjet.surchargeUniforme.forceVerticale;
        surchargeUniformeF[1]=
          params.surchargesProjet.surchargeUniforme.forceHorizontale;
      }
      //lineiques
      if (params.surchargesProjet.nombreSurchargesLineiques > 0
        && (choix == 0 || choix == 1)) {
        surchargesLineiques= params.surchargesProjet.nombreSurchargesLineiques;
        for (int j= 0; j < surchargesLineiques; j++) {
          surchargeLineique[j][0]=
            params.surchargesProjet.surchargesLineiques[j].abscisse;
          surchargeLineique[j][1]=
            params.surchargesProjet.surchargesLineiques[j].forceVerticale;
          surchargeLineique[j][2]=
            params.surchargesProjet.surchargesLineiques[j].forceHorizontale;
        }
      }
      //trapezoidales
      if (params.surchargesProjet.nombreSurchargesTrapezoidales > 0
        && (choix == 0 || choix == 3)) {
        surchargesTrapezoidales=
          params.surchargesProjet.nombreSurchargesTrapezoidales;
        for (int j= 0; j < surchargesTrapezoidales; j++) {
          surchargeTrapezoidale[j][0]=
            params.surchargesProjet.surchargesTrapezoidales[j].abscisseDebut;
          surchargeTrapezoidale[j][1]=
            params.surchargesProjet.surchargesTrapezoidales[j].abscisseFin;
          surchargeTrapezoidale[j][2]=
            params
              .surchargesProjet
              .surchargesTrapezoidales[j]
              .forceVerticaleDebut;
          surchargeTrapezoidale[j][3]=
            params
              .surchargesProjet
              .surchargesTrapezoidales[j]
              .forceVerticaleFin;
          surchargeTrapezoidale[j][4]=
            params
              .surchargesProjet
              .surchargesTrapezoidales[j]
              .forceHorizontaleDebut;
          surchargeTrapezoidale[j][5]=
            params
              .surchargesProjet
              .surchargesTrapezoidales[j]
              .forceHorizontaleFin;
        }
      }
    }
    //Titre des Axes     
    final String nomaxeX= new String("X");
    final String nomaxeY= new String("Y");
    //Titre des courbes     
    final String[] nomcourbe= { "Talus et couche de sol", "Eau" };
    final DiapreGrapheResultats ng=
      new DiapreGrapheResultats(
        valeursX,
        valeursY,
        nomaxeX,
        nomaxeY,
        nomcourbe,
        eau,
        surchargeUniforme,
        surchargeUniformeF,
        surchargesLineiques,
        surchargeLineique,
        surchargesTrapezoidales,
        surchargeTrapezoidale);
    if (choix == 0) {
      final DiapreRappelDonnees g= new DiapreRappelDonnees(appli_, _id);
      g.setGraphe(ng);
      g.setVisible(true);
    } else {
      System.out.println("au revoir");
      final DiapreSurchargeGraphe graphe= new DiapreSurchargeGraphe(appli_);
      graphe.setGraphe(ng);
      graphe.setVisible(true);
    }
  }
}
