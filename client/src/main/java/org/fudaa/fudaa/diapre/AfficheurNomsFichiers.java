/*
 * @file         AfficheurNomsFichiers.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.fudaa.ctulu.CtuluLibString;
/**
 * Afficheur sp�cifique pour les noms des fichiers dans les cellules d'une liste.
 * Il retire le chemin d'acc�s et l'extension s'ils existent.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class AfficheurNomsFichiers implements ListCellRenderer {
  public Component getListCellRendererComponent(
    final JList list,
    final Object value,
    final int index,
    final boolean isSelected,
    final boolean cellHasFocus) {
    String nomFichier= (String)value;
    int position= nomFichier.lastIndexOf(File.separator) + 1;
    if (position != -1) {
      nomFichier= nomFichier.substring(position);
    }
    position= nomFichier.lastIndexOf(CtuluLibString.DOT);
    if (position != -1) {
      nomFichier= nomFichier.substring(0, position);
    }
    final JLabel label= new JLabel(nomFichier);
    label.setBackground(list.getBackground());
    label.setOpaque(true);
    if (isSelected) {
      label.setBackground(list.getSelectionBackground());
    }
    return label;
  }
}
