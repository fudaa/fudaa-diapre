/*
 * @file         DiapreSurchartrapdess.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPicture;
/**
 * Fenetre d'affichage d'un graphique.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Jean de Malafosse 
 */
public class DiapreSurchartrapdess extends JDialog implements ActionListener {
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  public DiapreSurchartrapdess() {
    setTitle("Surcharge trap�zo�dale inclin�e : dessin");
    setModal(true);
    setSize(500, 300);
    setLocation(300, 400);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    c.gridy= GridBagConstraints.RELATIVE;
    c.ipadx= 5;
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    //premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    Image img;
    img= DiapreResource.DIAPRE.getImage("diapre-logo.gif");
    placeComposant(lm, new BuPicture(img), c);
    c.gridy= 1;
    placeComposant(lm, new BuLabel("La version actuelle de diapre ne"), c);
    c.gridy= 2;
    placeComposant(lm, new BuLabel("permet pas l'affichage du graphique"), c);
    c.gridy= 3;
    final BuButton bok= new BuButton(" OK ");
    placeComposant(lm, bok, c);
    bok.addActionListener(new DiapreokListener());
  }
  public void actionPerformed(final ActionEvent e) {
    setVisible(true);
  }
  class DiapreokListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      dispose();
    }
  }
}
