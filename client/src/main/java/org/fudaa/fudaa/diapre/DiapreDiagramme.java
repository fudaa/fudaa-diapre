/*
 * @file         DiapreDiagramme.java
 * @creation     2000-11-28
 * @modification $Date: 2007-05-04 13:59:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuToggleButton;

import org.fudaa.dodico.corba.diapre.SParametresDiapre;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.EbliFilleGraphe;
/**
 * Fenetre d'affichage d'un graphique.
 *
 * @version      $Id: DiapreDiagramme.java,v 1.8 2007-05-04 13:59:05 deniger Exp $
 * @author       Jean de Malafosse 
 */
public class DiapreDiagramme
  extends EbliFilleGraphe
  implements /*ActionListener,*/ InternalFrameListener {
  private DiapreImplementation diapre_;
  SParametresDiapre parametresDiapre_;
  //  private BGraphe                         graphe_ = null;
  BuToggleButton valider_; //= new BuButton("Valider");
  public void placeComposant(
    final GridBagLayout _lm,
    final Component _composant,
    final GridBagConstraints _c) {
    _lm.setConstraints(_composant, _c);
    getContentPane().add(_composant);
  }
  public DiapreDiagramme(
    final BuCommonInterface _appli,
    final BuInformationsDocument _id) {
    super(
      "Diagramme des pressions : graphique",
      true,
      true,
      true,
      true,
      _appli,
      _id);
    diapre_= (DiapreImplementation)_appli.getImplementation();
    setBackground(Color.white);
    valider_= new BuToggleButton("Personnalisation du graphique");
    valider_.setActionCommand("BOUTON_PERSO");
    valider_.addActionListener(this);
    //SpecificTools
    //tools_ = new JComponent[1];
    //tools_[0] = btPerso_;
    getContentPane().setLayout(new BorderLayout());
    setPreferredSize(new Dimension(700, 600));
    pack();
    addInternalFrameListener(this);
    setDefaultCloseOperation(1);
    diapre_.addInternalFrame(this);
    setVisible(true);
  }
  public BGraphe getGraphe() {
    return graphe_;
  }
  public void setGraphe(final BGraphe _graphe) {
    //System.out.println("test dd setGraph");
    getContentPane().removeAll();
    graphe_= _graphe;
    diapre_.setGraph(graphe_);
    getContentPane().add(graphe_, BorderLayout.CENTER);
    pack();
    diapre_.setEnabledForAction("IMPRIMER", true);
    //diapre.setEnabledForAction("PHOTOGRAPHIE",true);
    diapre_.photoResult();
  }
  /* public JComponent[] getSpecificTools ()
   {
     return  tools_;
   }*/
  public void actionPerformed(final ActionEvent _evt) {}
  /**
   * Methode d'impression d'un graphe
   */
  /* public void print (PrintJob _job, Graphics _g)
  {
    BuPrinter.INFO_DOC = new BuInformationsDocument();
    BuPrinter.INFO_DOC.name = getTitle();
    BuPrinter.INFO_DOC.logo = null;
  
    BuPrinter.printComponent(_job,_g, graphe_);
  } */
  public void internalFrameClosed(final InternalFrameEvent _e) {
    setVisible(false);
    diapre_.removeInternalFrame(this);
    /*if (fperso_ != null) 
    {
      fperso_.setVisible(false);
      imp_.removeInternalFrame(fperso_);
    }*/
  }
  public void internalFrameActivated(final InternalFrameEvent _e) {}
  public void internalFrameDeactivated(final InternalFrameEvent _e) {
    diapre_.setEnabledForAction("FERMER", true);
    diapre_.setEnabledForAction("PHOTOGRAPHIE", false);
    diapre_.setEnabledForAction("IMPRIMER", false);
  }
  public void internalFrameOpened(final InternalFrameEvent _e) {}
  public void internalFrameClosing(final InternalFrameEvent _e) {}
  public void internalFrameIconified(final InternalFrameEvent _e) {}
  public void internalFrameDeiconified(final InternalFrameEvent _e) {}
  class DiapreValiderListener implements ActionListener {
    public void actionPerformed(final ActionEvent _e) {
      dispose();
    }
  }
}
