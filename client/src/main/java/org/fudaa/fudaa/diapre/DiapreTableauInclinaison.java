/*
 * @file         DiapreTableauInclinaison.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;

import org.fudaa.dodico.corba.diapre.SOptions;
import org.fudaa.dodico.corba.diapre.SParametresDiapre;
import org.fudaa.dodico.corba.diapre.SResultatsDiapre;
import org.fudaa.dodico.corba.diapre.SSurchargesProjet;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Fenetre d'affichage d'un tableau .
 *
 * @version      $Id: DiapreTableauInclinaison.java,v 1.9 2006-09-19 15:02:12 deniger Exp $
 * @author       Jean de Malafosse
 */
public class DiapreTableauInclinaison
  extends JDialog
  implements ActionListener {
  //private DiapreImplementation diapre;
  protected FudaaProjet projet_;
  // D�claration des variables
  SSurchargesProjet parametresProjet;
  SOptions options;
  //private BuDialogMessage message= null;
  //private BuCommonInterface appli_;
  //protected BuAssistant          assistant_;
  //private double minAbscisse;
  int nbCouches;
  String SouD;
  // Zones de saisie
  MyTableModel myModel= new MyTableModel(nbCouches);
  BuButton ok= new BuButton(" Valider ");
  BuLabel lab;
  BuLabel lab2;
  public void setProjet(final FudaaProjet _projet) {
    projet_= _projet;
    final SParametresDiapre SParametres=
      (SParametresDiapre)projet_.getParam(DiapreResource.PARAMETRES);
    SouD= SParametres.options.ligneGlissement;
    if (SouD.equals("D")) {
      nbCouches= 2;
    } else {
      nbCouches= SParametres.sol.nombreCouches + 1;
    }
    myModel.setResultatsProjet(
      (SResultatsDiapre)projet_.getResult(DiapreResource.RESULTATS),
      SParametres);
  }
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  // CONSTRUCTEUR /////////////////////////////////////////////////////////
  public DiapreTableauInclinaison(
    final BuCommonInterface _appli,
    final int nombreCouches_,
    final String SouD_) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Inclinaison des segments de glissement",
      true);
    SouD= SouD_;
    nbCouches= nombreCouches_;
    System.out.println("Nombre de couches = " + nbCouches);
    if (nbCouches == 1) {
      setSize(520, 350);
    }
    if (nbCouches == 2) {
      setSize(650, 350);
    }
    if (nbCouches >= 3) {
      setSize(800, 350);
    }
    setResizable(true);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    //MyTableModel  myModel   = new MyTableModel();
    nbCouches= nombreCouches_;
    myModel= new MyTableModel(nbCouches);
    final JTable table= new JTable(myModel);
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    //c.anchor      = GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 2; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    // premiere colonne de la fenetre
    c.gridx= 0;
    c.gridy= 0;
    if (SouD.equals("S")) {
      lab= new BuLabel("L'option S (succession de segments) a �t� choisie.");
      lab2=
        new BuLabel("L'inclinaison des polylignes de rupture est la suivante : ");
      placeComposant(lm, lab, c);
      c.gridy= 1;
      placeComposant(lm, lab2, c);
    } else {
      c.gridy= 0;
      lab= new BuLabel("L'option D (une seule droite) a �t� choisie.");
      lab2=
        new BuLabel("L'inclinaison de la surface de rupture par rapport � l'horizontale est la suivante : ");
      nbCouches= 1;
      placeComposant(lm, lab, c);
      c.gridy= 1;
      placeComposant(lm, lab2, c);
    }
    c.gridy= 2;
    c.gridwidth= 1;
    if (nbCouches == 1) {
      table.setPreferredScrollableViewportSize(new Dimension(250, 170));
    }
    if (nbCouches == 2) {
      table.setPreferredScrollableViewportSize(new Dimension(500, 170));
    }
    if (nbCouches >= 3) {
      table.setPreferredScrollableViewportSize(new Dimension(600, 170));
    }
    final JScrollPane scrollPane= new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //On regle la taille des colonnes
     (table.getColumn(table.getColumnName(0))).setPreferredWidth(50);
    placeComposant(lm, scrollPane, c);
    // deuxieme colonne
    c.gridx= 1;
    c.gridy= 2;
    placeComposant(lm, ok, c);
    ok.addActionListener(new DiapreokListener());
  }
  public void actionPerformed(final ActionEvent e) {
    setVisible(true);
  }
  // BOUTTONS
  class DiapreokListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      dispose();
    }
  }
}
