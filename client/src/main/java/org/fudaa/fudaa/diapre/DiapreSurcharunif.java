/*
 * @file         DiapreSurcharunif.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPicture;

import org.fudaa.dodico.corba.diapre.SSurchargeUniforme;
/**
 * Fenetre de saisie des parametres de la surcharge uniforme.
 * Ces parametres sont enregistr�s dans la structure SSurchargeUniforme.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreSurcharunif extends JDialog implements ActionListener {
  //DiapreSurcharunifdess unifdess;
  //SSurchargesProjet     parametresProjet      = null;
  SSurchargeUniforme surchargeUniforme;
  private BuDialogMessage message;
  //private    DiapreImplementation  diapre;
  BuCommonInterface appli_;
  protected BuAssistant assistant_;
  int nombreSurchargeUniforme;
  // Zones de saisie
  TextFieldsDiapre forceVerticale= new TextFieldsDiapre(true);
  TextFieldsDiapre forceHorizontale= new TextFieldsDiapre(true);
  BuButton graph= new BuButton(" Graphique ");
  BuButton bvalider= new BuButton(" Valider ");
  BuButton bannuler= new BuButton(" Supprimer ");
  // Bouleen pour une saisie totale
  boolean bool1;
  boolean bool2;
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  public DiapreSurcharunif(final BuCommonInterface _appli) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Surcharge uniforme semi infinie ",
      true);
    setSize(800, 300);
    setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    appli_= _appli;
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    c.anchor= GridBagConstraints.SOUTHWEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    // premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    c.anchor= GridBagConstraints.SOUTHEAST;
    final BuLabel vertic= new BuLabel("Force verticale : V    ");
    placeComposant(lm, vertic, c);
    c.gridy= 1;
    c.anchor= GridBagConstraints.EAST;
    final BuLabel horiz= new BuLabel("Force horizontale: H");
    placeComposant(lm, horiz, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(lm, graph, c);
    //unifdess = new DiapreSurcharunifdess(_appli);
    graph.addActionListener(new DiapreSurcharunifdessListener());
    //deuxieme colonne
    c.gridx= 1;
    c.gridy= 0;
    c.anchor= GridBagConstraints.SOUTHEAST;
    placeComposant(lm, forceVerticale, c);
    c.gridy= 1;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, forceHorizontale, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(lm, bvalider, c);
    bvalider.addActionListener(new DiapreValidListener());
    //troisieme colonne
    c.gridx= 2;
    c.gridy= 0;
    c.anchor= GridBagConstraints.SOUTHWEST;
    final BuLabel unite= new BuLabel("kN/m�");
    placeComposant(lm, unite, c);
    c.gridy= 1;
    c.anchor= GridBagConstraints.WEST;
    final BuLabel unit= new BuLabel("kN/m�");
    placeComposant(lm, unit, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(lm, bannuler, c);
    bannuler.addActionListener(new DiapreAnnulListener());
    //quatrieme colonne
    c.gridx= 3;
    c.gridy= 0;
    c.gridheight= 3; //nombre de lignes
    Image img;
    img= DiapreResource.DIAPRE.getImage("diapre_6.gif");
    placeComposant(lm, new BuPicture(img), c);
    // Pour voir si une case est remplie
    forceVerticale.addFocusListener(new ForceVerticaleAction());
    forceHorizontale.addFocusListener(new ForceHorizontaleAction());
    setParametresSurcharunif(surchargeUniforme);
  }
  public void actionPerformed(final ActionEvent e) {
    surchargeUniforme= getParametresSurcharunif();
    setVisible(true);
  }
  // Mutateur des parametres de surcharge uniforme
  public void setParametresSurcharunif(final SSurchargeUniforme surchargeUniforme_) {
    surchargeUniforme= surchargeUniforme_;
    if (surchargeUniforme != null) {
      if (nombreSurchargeUniforme == 1) {
        forceVerticale.setValue(surchargeUniforme.forceVerticale);
        forceHorizontale.setValue(surchargeUniforme.forceHorizontale);
        bool1= true;
        bool2= true;
      }
    }
  }
  // Accesseur des parametres de surcharge uniforme
  public SSurchargeUniforme getParametresSurcharunif() {
    boolean b1= false;
    boolean b2= false;
    if (surchargeUniforme == null) {
      surchargeUniforme= new SSurchargeUniforme();
    }
    if (!forceVerticale.getTextDiapre().equals("")) {
      surchargeUniforme.forceVerticale= forceVerticale.getValue();
      System.out.println(
        "FV surchar unif : " + surchargeUniforme.forceVerticale);
      b1= true;
    }
    if (!forceHorizontale.getTextDiapre().equals("")) {
      surchargeUniforme.forceHorizontale= forceHorizontale.getValue();
      System.out.println(
        "FH surchar unif : " + surchargeUniforme.forceHorizontale);
      b2= true;
    }
    if (b1 && b2) {
      nombreSurchargeUniforme= 1;
    } else {
      nombreSurchargeUniforme= 0;
    }
    System.out.println(
      "Pr�sence d'une surcharge uniforme : " + nombreSurchargeUniforme);
    return surchargeUniforme;
  }
  public int getNbreSurchargeUniforme() {
    return nombreSurchargeUniforme;
  }
  public void setNbreSurchargeUniforme(final int nbre) {
    nombreSurchargeUniforme= nbre;
  }
  void messageAssistant(final String s) {
    message= new BuDialogMessage(appli_, appli_.getInformationsSoftware(), s);
    message.setSize(600, 130);
    message.setTitle("ERREUR");
    message.setResizable(false);
    message.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    final Point pos= this.getLocationOnScreen();
    pos.x= pos.x + this.getWidth() / 2 - message.getWidth() / 2;
    pos.y= pos.y + this.getHeight() / 2 - message.getHeight() / 2;
    message.setLocation(pos);
    message.setVisible(true);
  }
  class DiapreSurcharunifdessListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (!bool1 && !bool2) {
        messageAssistant("Aucune surcharge uniforme semi infinie n'a �t� d�finie");
      } else {
        if (!bool1) {
          messageAssistant("Vous devez entrer une force verticale avant de visualiser le graphe");
        } else {
          if (!bool2) {
            messageAssistant("Vous devez entrer une force horizontale avant de visualiser le graphe");
          } else {
            getParametresSurcharunif();
            //new DiapreConstruitGrapheRappel(appli_,2);
          }
        }
      }
    }
  }
  class DiapreValidListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (!bool1 && !bool2) {
        messageAssistant("Aucune surcharge uniforme semi infinie n'a �t� d�finie");
      } else {
        if (!bool1) {
          messageAssistant("Vous devez entrer une force verticale avant de valider");
        } else {
          if (!bool2) {
            messageAssistant("Vous devez entrer une force horizontale avant de valider");
          } else {
            getParametresSurcharunif();
            dispose();
          }
        }
      }
    }
  }
  class DiapreAnnulListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (bool1 && bool2) {
        final BuDialogConfirmation c=
          new BuDialogConfirmation(
            appli_,
            appli_.getInformationsSoftware(),
            "Voulez-vous supprimer la surcharge uniforme ?");
        if (c.activate() == JOptionPane.YES_OPTION) {
          bool1= false;
          bool2= false;
          nombreSurchargeUniforme= 0;
          forceVerticale.setValue("");
          forceHorizontale.setValue("");
          getParametresSurcharunif();
          dispose();
        }
      } else {
        forceVerticale.setValue("");
        forceHorizontale.setValue("");
        dispose();
      }
    }
  }
  // Saisie obligatoire de tous les parametres !!!
  class ForceVerticaleAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool1= true;
    }
    public void focusLost(final FocusEvent e) {
      if (forceVerticale.getTextDiapre() == null
        || forceVerticale.getTextDiapre().equals("")) {
        bool1= false;
      }
    }
  }
  class ForceHorizontaleAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool2= true;
    }
    public void focusLost(final FocusEvent e) {
      if (forceHorizontale.getTextDiapre() == null
        || forceHorizontale.getTextDiapre().equals("")) {
        bool2= false;
      }
    }
  }
}
