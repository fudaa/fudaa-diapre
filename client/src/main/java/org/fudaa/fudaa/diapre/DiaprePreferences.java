/*
 * @file         DiaprePreferences.java
 * @creation     2000-11-17
 * @modification $Date: 2003-11-25 10:13:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import com.memoire.bu.BuPreferences;
/**
 * Préférences de sipor
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:13:39 $ by $Author: deniger $
 * @author       Nicolas Chevalier 
 */
public class DiaprePreferences extends BuPreferences {
  public final static DiaprePreferences DIAPRE= new DiaprePreferences();
}
