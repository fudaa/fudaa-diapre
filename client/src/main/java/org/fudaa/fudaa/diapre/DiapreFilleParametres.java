/*
 * @file         DiapreFilleParametres.java
 * @creation     1999-10-01
 * @modification $Date: 2006-09-19 15:02:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuResource;

import org.fudaa.dodico.corba.diapre.SOptions;
import org.fudaa.dodico.corba.diapre.SParametresDiapre;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:02:15 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreFilleParametres
  extends JInternalFrame
  implements ChangeListener, ActionListener {
  // D�claration du classeur � onglets
  JTabbedPane tpMain;
  int ongletCourant; // Num�ro de l'onglet courant
  //Booleen permettant l'activation des autres onglets
  //static private boolean geometrie = false;
/*  static private boolean eau= false;
  static private boolean sol= false;
  static private boolean surchargesProjet= false;*/
  private static final double[] ymin= new double[2];
  static private double xmin;
  static private double xmax;
  static private double y1;
  static private double y2;
  //static boolean blocus = true;
  // D�claration des panels du classeur � onglets
  DiapreGeometrieParametres pnGeometrie;
  DiapreSolsParametres pnSols;
  DiapreEauParametres pnEau;
  DiapreSurchargesParametres pnSurcharges;
  DiapreValidationParametres pnValidation;
  DiapreOptionsParametres pnOptions;
  DiapreCommentaires pnCommentaires;
  // Declaration de la structure contenant les parametres
  //SParametresDiapre parametresDiapre = new SParametresDiapre();
  FudaaProjet projet_;
  JComponent content_;
  BuCommonInterface appli_;
  // Constructeur
  public DiapreFilleParametres(final BuCommonInterface _appli, final FudaaProjet _projet) {
    super("Param�tres de calcul", false, false, true, true);
    // (title,resizable,closable,maximizable,iconifiable)
    setSize(700, 650);
    appli_= _appli;
    projet_= _projet;
    tpMain= new JTabbedPane();
    pnGeometrie= new DiapreGeometrieParametres(_appli);
    pnSols= new DiapreSolsParametres(_appli);
    pnEau= new DiapreEauParametres(_appli);
    pnSurcharges= new DiapreSurchargesParametres(_appli);
    pnValidation= new DiapreValidationParametres(_appli, this);
    pnOptions= new DiapreOptionsParametres(_appli);
    pnCommentaires= new DiapreCommentaires();
    tpMain.addTab(" G�om�trie ", null, pnGeometrie, "G�om�trie");
    tpMain.addTab(" Sols", null, pnSols, "Sols");
    tpMain.addTab(" Eau", null, pnEau, "Eau");
    tpMain.addTab(" Surcharges ", null, pnSurcharges, "Surcharges");
    tpMain.addTab(" Validation ", null, pnValidation, "Validation");
    tpMain.addTab(" Options", null, pnOptions, "Options");
    tpMain.addTab(" Commentaires ", null, pnCommentaires, "Commentaires");
    content_= (JComponent)getContentPane();
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add("Center", tpMain);
    setFrameIcon(BuResource.BU.getIcon("param�tre"));
    tpMain.setEnabledAt(2, false);
    tpMain.setEnabledAt(3, false);
    tpMain.setEnabledAt(4, false);
    tpMain.setEnabledAt(5, false);
    tpMain.setEnabledAt(6, false);
    // La fille param�tre est � l'�coute des changements d'onglet
    tpMain.addChangeListener(this);
    setParametresDiapre(
      (SParametresDiapre)_projet.getParam(DiapreResource.PARAMETRES));
    /*parametresDiapre.eau = new SEau();
    parametresDiapre.eau.presenceNappeEau =0;
    //parametresDiapre.talusDiapre = new STalusDiapre();
    //parametresDiapre.sol = new SSol();
    //parametresDiapre.parametresGeneraux = new SParametresGeneraux();

    parametresDiapre.surchargesProjet = new SSurchargesProjet();
    parametresDiapre.surchargesProjet.nombreSurchargesLineiques = 0;
    parametresDiapre.surchargesProjet.nombreSurchargesTrapezoidales = 0;

    /*
    parametresDiapre.surchargesProjet = new SSurchargesProjet();
    parametresDiapre.surchargesProjet.nombreSurchargesLineiques = 0;
    parametresDiapre.surchargesProjet.nombreSurchargeUniforme = 0;
    parametresDiapre.surchargesProjet.nombreSurchargesTrapezoidales = 0;
    */
  }
  public void actionPerformed(final ActionEvent e) {
    //implementation.setEnabledForAction("FERMER", true);
    setVisible(false);
  }
  public void degrisageOnglet() {
    tpMain.setEnabledAt(2, true);
    tpMain.setEnabledAt(3, true);
    tpMain.setEnabledAt(4, true);
    tpMain.setEnabledAt(5, true);
    tpMain.setEnabledAt(6, true);
    appli_.setEnabledForAction("GRAPHE", true);
    appli_.setEnabledForAction("VALEURS", true);
  }
  public int getHauteur() {
    final double h= 100 * (pnGeometrie.getOrdonnee1() - pnGeometrie.getOrdonnee2());
    int i= 0;
    while (i < h) {
      i++;
    }
    return (i);
  }
  public void enregistreParametres() {
    projet_.addParam(DiapreResource.PARAMETRES, getParametresDiapre());
  }
  public SParametresDiapre getParametresDiapre() {
    final SParametresDiapre parametresDiapre= new SParametresDiapre();
    parametresDiapre.talusDiapre= pnGeometrie.getGeometrie();
    parametresDiapre.sol= pnSols.getParametresSols();
    parametresDiapre.parametresGeneraux= pnSols.getParametresGeneraux();
    parametresDiapre.eau= pnEau.getParametresEau();
    parametresDiapre.surchargesProjet= pnSurcharges.getParametresProjet();
    if (pnOptions.getParametresOptions().nombreSegmentsEcran != 0) {
      parametresDiapre.options= pnOptions.getParametresOptions();
      System.out.println(parametresDiapre.options);
    } else {
      parametresDiapre.options= new SOptions();
      parametresDiapre.options.ligneGlissement= "D";
      parametresDiapre.options.nombreSegmentsEcran= getHauteur();
      parametresDiapre.options.largeurSegmentDebut= 0.01;
      parametresDiapre.options.nombreSegmentsTalus= 1000;
    }
    parametresDiapre.commentairesDiapre=
      pnCommentaires.getParametresCommentaires();
    return parametresDiapre;
  }
  public void setParametresDiapre(SParametresDiapre params) {
    //parametresDiapre=params;
    if (params == null) {
      params= new SParametresDiapre();
    }
    pnGeometrie.setGeometrie(params.talusDiapre);
    if (params.sol != null) {
      degrisageOnglet();
    }
    pnSols.setParametresSols(params.sol);
    pnSols.setParametresGeneraux(params.parametresGeneraux);
    pnEau.setParametresEau(params.eau);
    pnSurcharges.setParametresProjet(params.surchargesProjet);
    pnOptions.setParametresOptions(params.options);
    pnCommentaires.setParametresCommentaires(params.commentairesDiapre);
    //System.out.println(params.commentairesDiapre.titre1);
  }
  /**
    * Recepteur pour les changements d'onglets. A chaque fois que l'on change d'onglet, les donn�es de
    * l'onglet que l'on quitte sont enregistr�es. Si on entre sur les r�gles de s�curit�, cet onglet n'active
    * que certaines zones.
    */
  public void stateChanged(final ChangeEvent evt) {
    // A chaque changement d'onglet : met a jour les donn�es locales avec celles de l'onglet
    final SParametresDiapre parametresDiapre= getParametresDiapre();
    switch (ongletCourant) {
      case 0 :
        System.err.println("Quitte l'onglet g�om�trie");
        if (tpMain.getSelectedIndex() != 0) {
          parametresDiapre.talusDiapre= pnGeometrie.getGeometrie();
          //il faut avoir saisi au minimum 3 points
          if (parametresDiapre.talusDiapre.nombrePointsTalus < 3) {
            //on reste sur l'onglet Geometrie
            tpMain.setSelectedIndex(0);
            //message d'erreur
            new BuDialogMessage(
              appli_,
              appli_.getInformationsSoftware(),
              "Il faut saisir au minimum"
                + " \n3 points dans l'onglet G�om�trie")
              .activate();
          } else {
            for (int i= 0;
              i < parametresDiapre.talusDiapre.nombrePointsTalus - 2;
              i++) {
              final int j= i + 1;
              final double valpre=
                parametresDiapre.talusDiapre.pointsTalus[i].abscissePointDiapre;
              final double valpost=
                parametresDiapre.talusDiapre.pointsTalus[j].abscissePointDiapre;
              if (valpost <= valpre) {
                tpMain.setSelectedIndex(0);
                final String mess=
                  new String(
                    "Les abscisses doivent �tre croissantes"
                      + "\nsauf pour les deux derniers points ");
                new BuDialogMessage(
                  appli_,
                  appli_.getInformationsSoftware(),
                  mess)
                  .activate();
              }
            }
            if (parametresDiapre
              .talusDiapre
              .pointsTalus[parametresDiapre
              .talusDiapre
              .nombrePointsTalus
              - 2].ordonneePointDiapre
              <= parametresDiapre
                .talusDiapre
                .pointsTalus[parametresDiapre
                .talusDiapre
                .nombrePointsTalus
                - 1].ordonneePointDiapre) {
              tpMain.setSelectedIndex(0);
              new BuDialogMessage(
                appli_,
                appli_.getInformationsSoftware(),
                "Les deux derni�res ordonn�es"
                  + " \ndoivent �tre d�croissantes")
                .activate();
            }
            if ((pnGeometrie.getOrdonnee1() - pnGeometrie.getOrdonnee2())
              > 50) {
              tpMain.setSelectedIndex(0);
              new BuDialogMessage(
                appli_,
                appli_.getInformationsSoftware(),
                "La hauteur de l'�cran ne doit pas \n d�passer 50 m")
                .activate();
            }
            if (pnGeometrie.getMinTalus() < pnGeometrie.getOrdonnee2()) {
              tpMain.setSelectedIndex(0);
              new BuDialogMessage(
                appli_,
                appli_.getInformationsSoftware(),
                "L'ordonn�e du dernier point doit �tre \n inf�rieure � "
                  + pnGeometrie.getMinTalus()
                  + " m")
                .activate();
            }
          }
        }
        if (parametresDiapre.talusDiapre.nombrePointsTalus > 0) {
          //sol
          ymin[0]= pnGeometrie.getMinTalus();
          ymin[1]= pnGeometrie.getOrdonnee2();
          pnSols.setMinOrdonnee(ymin);
          pnEau.setMinOrdonnee(ymin);
          //Surcharges
          xmin= pnGeometrie.getMinAbscisse();
          pnSurcharges.setMinAbscisse(xmin);
          xmax= pnGeometrie.getMaxAbscisse();
          pnSurcharges.setMaxAbscisse(xmax);
          //Options
          y1= pnGeometrie.getOrdonnee1();
          pnOptions.setOrdonnee1(y1);
          y2= pnGeometrie.getOrdonnee2();
          pnOptions.setOrdonnee2(y2);
        }
        break;
      case 1 :
        System.err.println("Quitte l'onglet sol");
        //if(!pnSols.blocus) sol=true;
        parametresDiapre.sol= pnSols.getParametresSols();
        //if(!pnSols.blocus)
        parametresDiapre.parametresGeneraux= pnSols.getParametresGeneraux();
        break;
      case 2 :
        System.err.println("Quitte l'onglet eau");
        /*if (!pnEau.blocus)
          eau= true;*/
        parametresDiapre.eau= pnEau.getParametresEau();
        break;
      case 3 :
        System.err.println("Quitte l'onglet surcharges");
        //if(!pnSurcharges.blocus) surchargesProjet = true;
        parametresDiapre.surchargesProjet= pnSurcharges.getParametresProjet();
        //System.out.println("L "+parametresDiapre.surchargesProjet.nombreSurchargesLineiques);
        //lin
        //parametresDiapre.surchargesProjet = pnSurcharges.lin.getParametresSurcharlin();
        //System.out.println("L "+parametresDiapre.surchargesProjet.nombreSurchargesLineiques);
        //System.out.println("SL1 abs :fille: "+ parametresDiapre.surchargesProjet.surchargesLineiques[0].abscisse);
        //unif
        //if(pnSurcharges.unif.bool1 && pnSurcharges.unif.bool2)
        //parametresDiapre.surchargesProjet = pnSurcharges.unif.getParametresSurcharunif();
        //else parametresDiapre.surchargesProjet.nombreSurchargeUniforme = 0;
        //System.out.println("U "+parametresDiapre.surchargesProjet.nombreSurchargeUniforme);
        //trap
        //parametresDiapre.surchargesProjet = pnSurcharges.trap.getParametresSurchartrap();
        //System.out.println("T "+parametresDiapre.surchargesProjet.nombreSurchargesTrapezoidales);
        break;
      case 4 :
        System.err.println("Quitte l'onglet validation");
        break;
      case 5 :
        System.err.println("Quitte l'onglet option");
        parametresDiapre.options= pnOptions.getParametresOptions();
        break;
      case 6 :
        System.err.println("Quitte l'onglet commentaire");
        parametresDiapre.commentairesDiapre=
          pnCommentaires.getParametresCommentaires();
        System.err.println(parametresDiapre.commentairesDiapre.titre1);
        break;
      default :
        System.err.println("Onglet inconnu au bataillon");
    }
    setParametresDiapre(parametresDiapre);
    // Mise � jour de l'onglet courant
    ongletCourant= tpMain.getSelectedIndex();
    // Si on rentre sur les regles de securite : activer que les bassins utilises
    //    if (ongletCourant == 6) pnReglesSecurite.activerChampsUtilises  ();
    //    if (ongletCourant == 2) pnQuais.setQuais                        (implementation.outils_.getQuais());
    //    if (ongletCourant == 4) pnCategories.setCategories              (implementation.outils_.getCategories());
    //    if (ongletCourant == 5) pnPerturbateur.setPerturbateur          (implementation.outils_.getPerturbateur());
  }
  /**
    * Mise � jour des onglets. On leur affecte les donn�es de l'impl�mentation, par exemple lorsque
    * l'on change de simulation courante.
    */
  public void updatePanels() {
    System.out.println("updatePanels");
    //pnGeometrie.getGeometrie();
    //    pnDonneesGenerales.setDonneesGenerales ( implementation.outils_.getDonneesGenerales() );
    //    pnBassins.updatePanel();
    //    pnQuais.setQuais(implementation.outils_.getQuais());
    //    pnMaree.setMaree(implementation.outils_.getMaree());
    //    pnCategories.setCategories(implementation.outils_.getCategories());
    //    pnPerturbateur.setPerturbateur(implementation.outils_.getPerturbateur());
    //    pnReglesSecurite.setReglesSecurite(implementation.outils_.getReglesSecurite());
  }
  /**
    * Enregistre les donn�es de l'onglet courant dans la structure locale. Pour cela on g�n�re un faux
    * changement d'onglet.
    */
  public void enregistreOngletCourant() {
    stateChanged(new ChangeEvent(this));
  }
}
