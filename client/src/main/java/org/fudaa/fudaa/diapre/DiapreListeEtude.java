/*
 * @file         DiapreListeEtude.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuEmptyList;

import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Liste g�rant l'ouverture de plusieurs etudes � la fois.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreListeEtude
  extends BuEmptyList
  implements ListSelectionListener {
  DefaultListModel model_;
  DiapreImplementation diapre;
  public DiapreListeEtude(final BuCommonInterface appli) {
    super();
    diapre= (DiapreImplementation)appli.getImplementation();
    model_= new DefaultListModel();
    setModel(model_);
    setEmptyText("Aucune etude ");
    setCellRenderer(new AfficheurNomsFichiers());
    addListSelectionListener(this);
  }
  // Ajoute une etude dans la liste.
  public void ajouteEtude(final String nomFichier, final FudaaProjet projet) {
    //diapre.projets_.put(nomFichier, projet);
    if (!model_.contains(nomFichier)) {
      model_.addElement(nomFichier);
    }
    revalidate();
    repaint(200);
  }
  // Retire une etude de la liste.
  public void enleveEtude(final String nomFichier) {
    //diapre.projets_.remove ( nomFichier );
    model_.removeElement(nomFichier);
    revalidate();
    repaint(200);
  }
  // Changement du projet en cours dans l'impl�mentation.
  public void valueChanged(final ListSelectionEvent evt) {
    if (evt.getValueIsAdjusting()) {
      return;
    }
    //JList source = (JList)evt.getSource();
    // Effectue le changement de projet en cours.
    if (diapre.fp_ != null) {
      diapre.fp_.enregistreOngletCourant();
      //diapre.fp_ = new DiapreFilleParametres(diapre.getApp());
      //diapre.fp_.setVisible(true);
      //diapre = new DiapreImplementation();
      //diapre.init();
      //diapre.start();
    }
    //diapre.projet_ = (FudaaProjet)diapre.projets_.get( source.getSelectedValue() );
    //diapre.outils_.setProjet ( diapre.projet_ );
    //diapre.setTitle ( diapre.projet_.getFichier() );
    //System.out.println("valueChanged : " + diapre.projet_.getFichier() );
    //diapre.activeActionsExploitation();
    // Mise � jour de fp_ et fermeture de fRappelDonnees.
    if (diapre.fp_ != null) {
      diapre.fp_.updatePanels();
    }
    final BuDesktop dk= diapre.getMainPanel().getDesktop();
    if (diapre.fRappelDonnees_ != null) {
      try {
        diapre.fRappelDonnees_.setClosed(true);
        diapre.fRappelDonnees_.setSelected(false);
      } catch (final PropertyVetoException ex) {}
    }
    if (diapre.fStatistiquesFinales_ != null) {
      try {
        diapre.fStatistiquesFinales_.setClosed(true);
        diapre.fStatistiquesFinales_.setSelected(false);
      } catch (final PropertyVetoException ex) {}
      dk.removeInternalFrame(diapre.fStatistiquesFinales_);
      diapre.fStatistiquesFinales_= null;
    }
    // Ferme fResultats_
    /*if ( diapre.fResultats_ != null)
    {
      try
      {
        diapre.fResultats_.setClosed (true);
        diapre.fResultats_.setSelected (false);
      } catch ( PropertyVetoException ex ) {}
      dk.removeInternalFrame(diapre.fResultats_);
      diapre.fResultats_=null;
    }

    // Ferme fResultatsEtudes_
    if ( diapre.fResultatsEtudes_ != null)
    {
      try
      {
        diapre.fResultatsEtudes_.setClosed (true);
        diapre.fResultatsEtudes_.setSelected (false);
      } catch ( PropertyVetoException ex ) {}
      dk.removeInternalFrame(diapre.fResultatsEtudes_);
      diapre.fResultatsEtudes_=null;
    }*/
  }
  // Pour enregistrer la liste des etudes ouvertes dans un fihcier .pro
  protected void enregistre() {
    final String[] ext= { "pro" };
    String contenu= "";
    final Object[] tempo= model_.toArray();
    final String[] cles= new String[tempo.length];
    for (int i= 0; i < tempo.length; i++) {
      cles[i]= (String)tempo[i];
    }
    // V�rifie que toutes les etudes sont dans le meme repertoire
    String rpt= cles[0];
    // test si fichier du repertoire courant
    if (rpt.lastIndexOf(File.separator) == -1) {
      rpt=
        System.getProperty("user.dir")
          + File.separator
          + "exemples"
          + File.separator
          + "diapre"
          + File.separator;
    } else {
      rpt= rpt.substring(0, rpt.lastIndexOf(File.separator) + 1);
    }
    String cle= "";
    for (int i= 0; i < cles.length; i++) {
      cle= cles[i];
      // si chemin d'acces identique
      if ((cle.startsWith(rpt))
        && ((cle.substring(rpt.length())).lastIndexOf(File.separator) == -1)) {
        contenu += cle.substring(rpt.length()) + "\n";
      } else if (
        rpt.equalsIgnoreCase(
          System.getProperty("user.dir")
            + File.separator
            + "exemples"
            + File.separator
            + "diapre"
            + File.separator)) {
        contenu += cle + "\n";
      } else {
        new BuDialogError(
          diapre.getApp(),
          DiapreImplementation.isDiapre_,
          "Les etudes doivent etre dans le meme r�pertoire")
          .activate();
        return;
      }
    }
    // V�rifie que le fichier d'enregistrement de la liste se trouve dans le meme repertoire
    // que les etudes.
    String nomFichier=
      MethodesUtiles.choisirFichierEnregistrement(
        ext,
        (Component)diapre.getApp(),
        rpt);
    if (nomFichier == null) {
      return;
    }
    if (!nomFichier
      .substring(0, nomFichier.lastIndexOf(File.separator) + 1)
      .equals(rpt)) {
      new BuDialogError(
        diapre.getApp(),
        DiapreImplementation.isDiapre_,
        "La liste doit etre enregistr�e dans\nle meme repertoire que les etudes")
        .activate();
      return;
    }
    // ajoute l"extension si elle est manquante
    if (nomFichier.lastIndexOf(".pro") != (nomFichier.length() - 4)) {
      nomFichier += ".pro";
    }
    MethodesUtiles.enregistrer(nomFichier, contenu, diapre.getApp());
  }
  // Pour ouvrir un ensemble de etudes (d�fini dans un fichier .pro)
  protected void ouvrir() {
    // Ferme les etudes ouvertes.
    final Object[] liste= model_.toArray();
    for (int i= 0; i < liste.length; i++) {
      setSelectedValue(liste[i], true);
      diapre.fermer();
    }
    final String[] ext= { "pro" };
    final String nomFichier=
      MethodesUtiles.choisirFichier(ext, (Component)diapre.getApp(), null, 1);
    if (nomFichier == null) {
      return;
    }
    final String rpt=
      nomFichier.substring(0, nomFichier.lastIndexOf(File.separator) + 1);
    final Vector etude= new Vector();
    try {
      final FileReader fichier= new FileReader(nomFichier);
      final BufferedReader lecteur= new BufferedReader(fichier);
      String line= "";
      while ((line= lecteur.readLine()) != null) {
        etude.add(rpt + line);
      }
      lecteur.close();
      fichier.close();
    } catch (final IOException ex) {
      new BuDialogError(
        diapre.getApp(),
        DiapreImplementation.isDiapre_,
        "Lecture du fichier impossible");
    }
    for (int i= 0; i < etude.size(); i++) {
      final FudaaProjet projet=
        new FudaaProjet(diapre.getApp(), new FudaaFiltreFichier("diapre"));
      projet.setEnrResultats(true);
      projet.ouvrir((String)etude.elementAt(i));
      if (projet.estConfigure()) {
        ajouteEtude((String)etude.elementAt(i), projet);
      }
    }
    //if ( diapre.projets_.size()>0 ) {
    //new BuDialogMessage(diapre.getApp(), diapre.isDiapre_, "Fichiers charg�s").activate();
    //setSelectedIndex(0);
    //diapre.activerCommandesEtude();
    //diapre.activeActionsExploitation();
    //}
  }
}
