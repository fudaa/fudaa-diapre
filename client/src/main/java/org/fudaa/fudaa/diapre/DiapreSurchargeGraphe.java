/*
 * @file         DiapreSurchargeGraphe.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.diapre.SParametresDiapre;

import org.fudaa.ebli.graphe.BGraphe;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreSurchargeGraphe extends JDialog implements ActionListener {
  protected DiapreImplementation diapre;
  SParametresDiapre parametresDiapre;
  private BGraphe graphe_;
  BuButton bok= new BuButton(" Valider ");
  public DiapreSurchargeGraphe(final BuCommonInterface _appli) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Surcharge lin�ique - graphique ",
      true);
    setBackground(Color.white);
    setResizable(false);
    setSize(new Dimension(500, 450));
    setDefaultCloseOperation(1);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    diapre= (DiapreImplementation)_appli.getImplementation();
    bok.addActionListener(new DiapreokListener());
  }
  class DiapreokListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      dispose();
    }
  }
  public BGraphe getGraphe() {
    return graphe_;
  }
  public void setGraphe(final BGraphe _graphe) {
    getContentPane().removeAll();
    graphe_= _graphe;
    graphe_.setPreferredSize(new Dimension(500, 425));
    bok.setPreferredSize(new Dimension(100, 25));
    final BuPanel pane= new BuPanel();
    pane.setLayout(new BuBorderLayout());
    pane.add(graphe_, BuBorderLayout.CENTER);
    pane.add(bok, BuBorderLayout.SOUTH);
    getContentPane().add(pane, BuBorderLayout.CENTER);
  }
  public void actionPerformed(final ActionEvent _evt) {}
}
