/*
 * @file         MyTableModel.java
 * @creation     2000-11-17
 * @modification $Date: 2007-05-04 13:59:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import javax.swing.table.AbstractTableModel;

import org.fudaa.dodico.corba.diapre.SParametresDiapre;
import org.fudaa.dodico.corba.diapre.SResultatsDiapre;
// TABLE MODEL
/**
 * Fenetre d'affichage d'un tableau .
 *
 * @version      $Revision: 1.8 $ $Date: 2007-05-04 13:59:05 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
class MyTableModel extends AbstractTableModel {
  protected SResultatsDiapre SResultats;
  protected SParametresDiapre SParametres;
  int nbCouches;
  //String SouD;
  public void setResultatsProjet(
    final SResultatsDiapre res,
    final SParametresDiapre params) {
    SResultats= res;
    SParametres= params;
    nbCouches= SParametres.sol.nombreCouches + 1;
  }
  //Contructeur
  public MyTableModel(final int nbCouche) {
    nbCouches= nbCouche + 1;
  }
  final String[] columnNames=
    {
      "N� du segment",
      "Couche 1",
      "Couche 2",
      "Couche 3",
      "Couche 4",
      "Couche 5",
      "Couche 6",
      "Couche 7",
      "Couche 8",
      "Couche 9",
      "Couche 10",
      "Couche 11",
      "Couche 12",
      "Couche 13",
      "Couche 14",
      "Couche 15",
      "Couche 16",
      "Couche 17",
      "Couche 18",
      "Couche 19",
      "Couche 20" };
  public int getRowCount() {
    if (SResultats != null) {
      return SResultats.anglesRupture.length;
    }
    return 0;
  }
  public int getColumnCount() {
    return nbCouches;
  }
  public Object getValueAt(final int _row, final int _col) {
    Object r= null;
    Double tmp= new Double(0);
    Integer n= new Integer(0);
    switch (_col) {
      case 0 :
        if (SResultats != null) {
          n= new Integer(SResultats.anglesRupture[_row][0].numeroSegment);
        }
        r= n;
        break;
      case 1 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][1].angleRupture);
        }
        r= tmp;
        break;
      case 2 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][2].angleRupture);
        }
        r= tmp;
        break;
      case 3 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][3].angleRupture);
        }
        r= tmp;
        break;
      case 4 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][4].angleRupture);
        }
        r= tmp;
        break;
      case 5 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][5].angleRupture);
        }
        r= tmp;
        break;
      case 6 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][6].angleRupture);
        }
        r= tmp;
        break;
      case 7 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][7].angleRupture);
        }
        r= tmp;
        break;
      case 8 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][8].angleRupture);
        }
        r= tmp;
        break;
      case 9 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][9].angleRupture);
        }
        r= tmp;
        break;
      case 10 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][10].angleRupture);
        }
        r= tmp;
        break;
      case 11 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][11].angleRupture);
        }
        r= tmp;
        break;
      case 12 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][12].angleRupture);
        }
        r= tmp;
        break;
      case 13 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][13].angleRupture);
        }
        r= tmp;
        break;
      case 14 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][14].angleRupture);
        }
        r= tmp;
        break;
      case 15 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][15].angleRupture);
        }
        r= tmp;
        break;
      case 16 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][16].angleRupture);
        }
        r= tmp;
        break;
      case 17 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][17].angleRupture);
        }
        r= tmp;
        break;
      case 18 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][18].angleRupture);
        }
        r= tmp;
        break;
      case 19 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][19].angleRupture);
        }
        r= tmp;
        break;
      case 20 :
        if (SResultats != null) {
          tmp= new Double(SResultats.anglesRupture[_row][20].angleRupture);
        }
        r= tmp;
        break;
    }
    return r;
  }
  public String getColumnName(final int _col) {
    String r= null;
    switch (_col) {
      case 0 :
        r= "N�";
        break;
      case 1 :
        r= "couche 1";
        break;
      case 2 :
        r= "couche 2";
        break;
      case 3 :
        r= "couche 3";
        break;
      case 4 :
        r= "couche 4";
        break;
      case 5 :
        r= "couche 5";
        break;
      case 6 :
        r= "couche 6";
        break;
      case 7 :
        r= "couche 7";
        break;
      case 8 :
        r= "couche 8";
        break;
      case 9 :
        r= "couche 9";
        break;
      case 10 :
        r= "couche 10";
        break;
      case 11 :
        r= "couche 11";
        break;
      case 12 :
        r= "couche 12";
        break;
      case 13 :
        r= "couche 13";
        break;
      case 14 :
        r= "couche 14";
        break;
      case 15 :
        r= "couche 15";
        break;
      case 16 :
        r= "couche 16";
        break;
      case 17 :
        r= "couche 17";
        break;
      case 18 :
        r= "couche 18";
        break;
      case 19 :
        r= "couche 19";
        break;
      case 20 :
        r= "couche 20";
        break;
    }
    return r;
  }
  public Class getColumnClass(final int _col) {
    Class r= Object.class;
    switch (_col) {
      case 0 :
        r= String.class;
        break;
      case 1 :
        r= String.class;
        break;
    }
    return r;
  }
}
