/*
 * @file         DiapreCalculCourtTerme.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;

import org.fudaa.dodico.corba.diapre.SCoucheSol;
import org.fudaa.dodico.corba.diapre.SSol;
/**
 * Boite de dialogue permettant de saisir les proprietes de la couche de sol.
 * Les proprietes sont automatiquement enregistr�es dans le champ SCoucheSol
 * avant la fermeture de la boite.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreCalculCourtTerme extends JDialog implements ActionListener {
  // Donn�e trait�e dans la boite de dialogue : vaut null par d�faut.
  protected SCoucheSol parametresSol;
  protected SSol solCouche;
  //numero de couche
  static public int i;
  private double[] ymin= new double[2];
  //message d'erreur
  private final String[] messErreur= new String[10];
  //verifie champs de saisi entierement complete
  boolean bool;
  boolean bool1;
  boolean bool2;
  boolean bool3;
  boolean bool4;
  boolean bool5;
  boolean blocus= true;
  private DiapreInterfaceHorizontale dih_;
  // Zones de saisie contenues dans la boite de dialogue
  TextField numeroCouche= new TextField(2);
  TextFieldsDiapre poidsVolumiqueTotal= new TextFieldsDiapre(false);
  TextFieldsDiapre poidsVolumique= new TextFieldsDiapre(false);
  TextFieldsDiapre cohesion= new TextFieldsDiapre(false);
  TextFieldsDiapre angleFrottementInterne= new TextFieldsDiapre(0, 90);
  TextFieldsDiapre limiteInferieureCouche= new TextFieldsDiapre(true);
  BuButton couchePrecedente= new BuButton("Couche pr�c�dente");
  BuButton coucheSuivante= new BuButton("Couche suivante");
  BuButton valider= new BuButton("Valider");
  // Utilise un GridBagLayout pour placer un composant avec des contraintes sp�cifiques
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  // Constructeur
  //Construit une boite de dialogue pour saisir les proprietes des couches de sol.
  public DiapreCalculCourtTerme(final DiapreInterfaceHorizontale _dih, final int i_) {
    super(_dih.diapre_.getFrame(), true);
    i= i_;
    dih_= _dih;
    // Caract�ristiques de la boite de dialogue
    setTitle("Calcul � court terme - Propri�t�s du sol - Interfaces horizontales ");
    setSize(680, 400);
    setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    final Point pos= dih_.diapre_.getFrame().getLocationOnScreen();
    pos.x += (dih_.diapre_.getFrame().getWidth() - getWidth()) / 2;
    pos.y += (dih_.diapre_.getFrame().getHeight() - getHeight()) / 2;
    setLocation(pos);
    //affichage du numero de la couche
    final int j= i + 1;
    final String numCouche= " " + j;
    numeroCouche.setText(numCouche);
    numeroCouche.setEditable(false);
    numeroCouche.setEnabled(false);
    // message d'erreur
    messErreur[0]=
      "La limite inf�rieure de la couche n�1 doit \n�tre inf�rieure ou �gale � ";
    messErreur[1]= "La limite inf�rieure de la couche n�";
    messErreur[2]= "\ndoit �tre inf�rieure ou �gale � ";
    messErreur[3]=
      "L'angle de frottement interne doit �tre \n compris entre 0� et 90�";
    messErreur[4]=
      "Vous devez compl�ter toutes les donn�es \nconcernant cette couche";
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    c.gridy= GridBagConstraints.RELATIVE;
    c.anchor= GridBagConstraints.WEST;
    c.ipadx= 5;
    c.ipady= 5;
    c.gridwidth= 3; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    //c.weightx    = 100; //proportion des colonnes
    c.insets.left= 30;
    c.insets.right= 30;
    //c.insets.bottom=15;
    //premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    c.anchor= GridBagConstraints.CENTER; //c.weightx    = 33;
    placeComposant(
      lm,
      new JLabel("Les couches de sol sont d�crites de haut en bas"),
      c);
    c.gridy= 1;
    c.gridwidth= 1;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, new JLabel("Couche n� : "), c);
    c.gridy= 2;
    placeComposant(lm, new JLabel(" "), c);
    c.gridy= 3;
    placeComposant(lm, new JLabel(" "), c);
    c.gridy= 4;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, new JLabel("Poids volumique humide :"), c);
    c.gridy= 5;
    placeComposant(lm, new JLabel("Poids volumique satur� :"), c);
    c.gridy= 6;
    placeComposant(lm, new JLabel("Coh�sion non drain�e :"), c);
    c.gridy= 7;
    placeComposant(
      lm,
      new JLabel("Angle de frottement interne non drain� :"),
      c);
    c.gridy= 8;
    placeComposant(lm, new JLabel("Limite inf�rieure de la couche :"), c);
    c.gridy= 9;
    placeComposant(lm, new JLabel(""), c);
    c.gridy= 10;
    //placeComposant( lm, new JLabel(""), c);
    c.gridy= 11;
    placeComposant(lm, couchePrecedente, c);
    //deuxieme colonne: ajout des �tiquettes sur la boite de dialogue premiere colonne
    c.gridx= 1; //c.weightx    = 33;
    c.gridy= 1;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, numeroCouche, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, new JLabel(" "), c);
    c.gridy= 3;
    placeComposant(lm, new JLabel(" "), c);
    c.gridy= 4;
    placeComposant(lm, poidsVolumiqueTotal, c);
    c.gridy= 5;
    placeComposant(lm, poidsVolumique, c);
    c.gridy= 6;
    placeComposant(lm, cohesion, c);
    c.gridy= 7;
    placeComposant(lm, angleFrottementInterne, c);
    c.gridy= 8;
    placeComposant(lm, limiteInferieureCouche, c);
    c.gridy= 9;
    placeComposant(lm, new BuLabel(" "), c);
    c.gridy= 10;
    //placeComposant(lm, new BuLabel(" "), c);
    c.gridy= 11;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, coucheSuivante, c);
    //troisieme colonne
    c.gridx= 2; //c.weightx    = 34;
    c.gridy= 1;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, new JLabel(" "), c);
    c.gridy= 2;
    placeComposant(lm, new JLabel(" "), c);
    c.gridy= 4;
    placeComposant(lm, new JLabel("kN/m�"), c);
    c.gridy= 5;
    placeComposant(lm, new JLabel("kN/m�"), c);
    c.gridy= 6;
    placeComposant(lm, new JLabel("kN/m�"), c);
    c.gridy= 7;
    placeComposant(lm, new JLabel("degr�"), c);
    c.gridy= 8;
    placeComposant(lm, new JLabel("m "), c);
    c.gridy= 9;
    placeComposant(lm, new JLabel(""), c);
    c.gridy= 10;
    //placeComposant( lm, new JLabel(""), c);
    c.gridy= 11;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, valider, c);
    //bouton couche suivante inactif tant que tous les champs ne sont pas complets
    coucheSuivante.setEnabled(false);
    valider.setEnabled(false);
    //Bouton CoucheSuivante inactif pour la couche 10 et tant que les 5 champs ne sont pas completes
    if (i == 19) {
      bool= true;
    }
    //action lorsqu'on clique sur le bouton couche suivante 10 couches maximum
    coucheSuivante.addActionListener(this);
    coucheSuivante.setActionCommand("COUCHE_SUIVANTE");
    //Bouton couchePrecedente inactif pour la premiere couche
    if (i == 0) {
      couchePrecedente.setEnabled(false);
    }
    //action lorsq'on clique sur le bouton couche precedente
    couchePrecedente.addActionListener(this);
    coucheSuivante.setActionCommand("COUCHE_PRECEDENTE");
    //action lorsqu'on clique sur valider enregistre et ferme la fenetre
    valider.addActionListener(this);
    coucheSuivante.setActionCommand("VALIDER");
    //lorsque les 5 champs sont complet le bouton suivant est actif
    poidsVolumiqueTotal.addFocusListener(new PoidsVolumiqueTotalAction());
    poidsVolumique.addFocusListener(new PoidsVolumiqueAction());
    cohesion.addFocusListener(new CohesionAction());
    angleFrottementInterne.addFocusListener(new AngleFrottementInterneAction());
    limiteInferieureCouche.addFocusListener(new LimiteInferieureCoucheAction());
    setParametresSol(parametresSol);
    setParametresSols(solCouche);
    //recuperation de ymin
  }
  // Enregistrement des donn�es de la boite de dialogue avant sa fermeture.
  public void actionPerformed(final ActionEvent e) {
    System.out.println(e);
    final String commande= e.getActionCommand();
    if ("VALIDER".equals(commande)) {
      cmd_Valider();
    } else if ("COUCHE_PRECEDENTE".equals(commande)) {
      cmd_CouchePrecedente();
    } else if ("COUCHE_SUIVANTE".equals(commande)) {
      cmd_CoucheSuivante();
    } else {
      parametresSol= getParametresSol();
      solCouche= getParametresSols();
      setVisible(false);
    }
  }
  public void setMinOrdonnee(final double[] min) {
    ymin[0]= min[0];
    ymin[1]= min[1];
  }
  public double[] getMin() {
    return ymin;
  }
  public int getNombreCouches() {
    return i;
  }
  public void setParametresSols(final SSol sol_) {
    solCouche= sol_;
  }
  public SSol getParametresSols() {
    return solCouche;
  }
  // Mutateur des parametres de couche de sol avec affichage des nouvelles donn�es dans la boite de dialogue.
  // Si parametresSol  vaut null, une nouvelle instance est cr�ee
  public void setParametresSol(final SCoucheSol parametresSol_) {
    parametresSol= parametresSol_;
    if (parametresSol != null) {
      poidsVolumiqueTotal.setValue(parametresSol.poidsVolumiqueTotal);
      poidsVolumique.setValue(parametresSol.poidsVolumique);
      cohesion.setValue(parametresSol.cohesion);
      angleFrottementInterne.setValue(parametresSol.angleFrottementInterne);
      limiteInferieureCouche.setValue(parametresSol.limiteInferieureCouche);
      valider.setEnabled(true);
      coucheSuivante.setEnabled(true);
      bool= false;
      bool1= true;
      bool2= true;
      bool3= true;
      bool4= true;
      bool5= true;
    }
  }
  // Accesseur des parametres de couche de sol avec enregistrement pr�alable des donn�es de la boite de dialogue dans ecluse.<BR>
  //Si parametresSol vaut null, une nouvelle instance est cr�ee
  public SCoucheSol getParametresSol() {
    if (parametresSol == null) {
      parametresSol= new SCoucheSol();
    }
    parametresSol.poidsVolumiqueTotal= poidsVolumiqueTotal.getValue();
    System.out.println(
      "poids volumique total : " + parametresSol.poidsVolumiqueTotal);
    parametresSol.poidsVolumique= poidsVolumique.getValue();
    System.out.println("poids volumique : " + parametresSol.poidsVolumique);
    parametresSol.cohesion= cohesion.getValue();
    System.out.println("cohesion : " + parametresSol.cohesion);
    parametresSol.angleFrottementInterne= angleFrottementInterne.getValue();
    System.out.println(
      "angle de frottement interne : " + parametresSol.angleFrottementInterne);
    parametresSol.limiteInferieureCouche= limiteInferieureCouche.getValue();
    System.out.println(
      "limite inferieure couche : " + parametresSol.limiteInferieureCouche);
    return parametresSol;
  }
  //verifie que tous les champs de saisi sont completes
  public boolean testChampsComplet() {
    if (bool1 && bool2 && bool3 && bool4 && bool5 && !bool) {
      valider.setEnabled(true);
      coucheSuivante.setEnabled(true);
      return true;
    }
    coucheSuivante.setEnabled(false);
    if (bool && bool1 && bool2 && bool3 && bool4 && bool5) {
      valider.setEnabled(true);
      return true;
    }
    valider.setEnabled(false);
    return false;
  }
  public void degrisageOnglets() {
    dih_.diapre_.fp_.degrisageOnglet();
  }
  public void messageAssistant(final String _message) {
    final BuDialogMessage message=
      new BuDialogMessage(dih_.diapre_, DiapreImplementation.isDiapre_, _message);
    message.setSize(1000, 600);
    message.setTitle("ERREUR");
    message.setResizable(false);
    message.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    message.activate();
  }
  public void cmd_CoucheSuivante() {
    boolean erreur= false;
    if (bool4) {
      if (testChampsComplet()) {
        ymin= getMin();
        if (i == 0 && limiteInferieureCouche.getValue() > ymin[0]) {
          messageAssistant(messErreur[0] + ymin[0] + " m");
          erreur= true;
        }
        if (!erreur
          && i > 0
          && limiteInferieureCouche.getValue()
            >= dih_.calCourtTerme[i - 1].limiteInferieureCouche.getValue()) {
          messageAssistant(
            messErreur[1]
              + (i + 1)
              + messErreur[2]
              + dih_.calCourtTerme[i
              - 1].limiteInferieureCouche.getValue()
              + " m");
          erreur= true;
        }
        if (!erreur) {
          //System.out.println(ymin[0]+"           "+ymin[1]);
          if (limiteInferieureCouche.getValue() < ymin[1]
            && i > 0
            && dih_.calCourtTerme[i - 1].limiteInferieureCouche.getValue()
              < ymin[1]) {
            messageAssistant(
              "Cette couche de sol ne sera pas prise en compte "
                + "\npar le code de calcul.");
          }
          i++;
          dih_.calCourtTerme[i - 1].setVisible(false);
          if (dih_.calCourtTerme[i] == null) {
            dih_.calCourtTerme[i]= new DiapreCalculCourtTerme(dih_, i);
          }
          dih_.calCourtTerme[i].setMinOrdonnee(ymin);
          dih_.calCourtTerme[i].setParametresSols(solCouche);
          if (solCouche != null && solCouche.nombreCouches >= i + 1) {
            dih_.calCourtTerme[i].setParametresSol(
              solCouche.autresCouches[i - 1]);
          }
          dih_.calCourtTerme[i].setVisible(true);
        }
      } else {
        messageAssistant(messErreur[4]);
      }
    } else {
      messageAssistant(messErreur[3]);
      valider.setEnabled(false);
    }
  }
  public void cmd_CouchePrecedente() {
    DiapreSolsParametres.interHorizontale.getParametresSols();
    if (!testChampsComplet()) {
      valider.setEnabled(false);
      coucheSuivante.setEnabled(false);
    }
    i--;
    dih_.calCourtTerme[i + 1].setVisible(false);
    if (dih_.calCourtTerme[i] != null) {
      dih_.calCourtTerme[i].setVisible(true);
    }
  }
  public void cmd_Valider() {
    int nombreCouche= 0;
    boolean erreur= false;
    while (nombreCouche < 20
      && dih_.calCourtTerme[nombreCouche] != null
      && dih_.calCourtTerme[nombreCouche].valider.isFocusable()) {
      nombreCouche++;
    }
    ymin= getMin();
    if (bool4) {
      if (testChampsComplet()) {
        final int k= verifieCouche(i, nombreCouche);
        if (k > 0) {
          messageAssistant(
            messErreur[1]
              + (k + 1)
              + messErreur[2]
              + dih_.calCourtTerme[k
              - 1].limiteInferieureCouche.getValue()
              + " m");
          erreur= true;
        }
        if (!erreur
          && dih_.calCourtTerme[nombreCouche
            - 1].limiteInferieureCouche.getValue()
            > ymin[1]) {
          messageAssistant(
            messErreur[1] + nombreCouche + messErreur[2] + ymin[1] + " m");
          erreur= true;
        }
        if (!erreur && i == 0 && limiteInferieureCouche.getValue() > ymin[0]) {
          messageAssistant(messErreur[0] + ymin[0] + " m");
          erreur= true;
        }
        if (!erreur
          && i > 0
          && limiteInferieureCouche.getValue()
            >= dih_.calCourtTerme[i - 1].limiteInferieureCouche.getValue()) {
          messageAssistant(
            messErreur[1]
              + (i + 1)
              + messErreur[2]
              + dih_.calCourtTerme[i
              - 1].limiteInferieureCouche.getValue()
              + " m");
          erreur= true;
        }
        if (!erreur) {
          if (limiteInferieureCouche.getValue() < ymin[1]
            && i > 0
            && dih_.calCourtTerme[i - 1].limiteInferieureCouche.getValue()
              < ymin[1]) {
            messageAssistant(
              "Cette couche de sol ne sera pas prise en compte "
                + "\npar le code de cacul.");
          }
          dih_.calCourtTerme[i].setVisible(false);
          i= 0;
          getParametresSol();
          degrisageOnglets();
        }
      } else {
        messageAssistant(messErreur[4]);
      }
    } else {
      messageAssistant(messErreur[3]);
      valider.setEnabled(false);
    }
  }
  // elle verifie que la limite inferieure de la couche j+1 est infereure a la couche j
  // si elle est inferieure, elle verifie si la limite inferieure de la couche j+2 est infeieure j+1
  // et ainsi de suite
  // si lors d'un test , une limite n'est pas inferieure alors return le numero de la couche
  // sinon on retourne 0
  public int verifieCouche(final int j, final int nbre) {
    int couche= 0;
    for (int k= j; k < nbre - 1; k++) {
      if (dih_.calCourtTerme[k + 1].limiteInferieureCouche.getValue()
        >= dih_.calCourtTerme[k].limiteInferieureCouche.getValue()) {
        couche= k + 1;
      }
    }
    return couche;
  }
  class PoidsVolumiqueTotalAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool1= true;
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      if (poidsVolumiqueTotal.getTextDiapre() == null
        || poidsVolumiqueTotal.getTextDiapre().equals("")) {
        bool1= false;
      }
    }
  }
  class PoidsVolumiqueAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool2= true;
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      if (poidsVolumique.getTextDiapre() == null
        || poidsVolumique.getTextDiapre().equals("")) {
        bool2= false;
      }
    }
  }
  class CohesionAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool3= true;
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      if (cohesion.getTextDiapre() == null
        || cohesion.getTextDiapre().equals("")) {
        bool3= false;
      }
    }
  }
  class AngleFrottementInterneAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool4= true;
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      if (angleFrottementInterne.getTextDiapre() == null
        || angleFrottementInterne.getTextDiapre().equals("")) {
        bool4= false;
      }
    }
  }
  class LimiteInferieureCoucheAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool5= true;
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      if (limiteInferieureCouche.getTextDiapre() == null
        || limiteInferieureCouche.getTextDiapre().equals("")) {
        bool5= false;
      }
    }
  }
}
