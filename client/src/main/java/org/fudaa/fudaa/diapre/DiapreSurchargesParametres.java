/*
 * @file         DiapreSurchargesParametres.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;

import org.fudaa.dodico.corba.diapre.SSurchargesProjet;
/**
 * Onglet des surcharges.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreSurchargesParametres extends BuPanel {
  protected boolean blocus1= true;
  protected boolean blocus2= true;
  protected boolean blocus3= true;
  SSurchargesProjet parametresProjet;
  //private DiapreImplementation Diapre;
  private BuCommonInterface appli_;
  static protected DiapreSurcharlin lin;
  static protected DiapreSurcharunif unif;
  static protected DiapreSurchartrap trap;
  double minAbscisse;
  double maxAbscisse;
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }
  public DiapreSurchargesParametres(final BuCommonInterface _appli) {
    super();
    appli_= _appli;
    //Diapre= ((DiapreImplementation)appli_.getImplementation());
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.CENTER;
    c.ipadx= 20;
    c.ipady= 20;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    final BuButton tbLin= new BuButton("      Surcharge lin�ique        ");
    final BuButton tbUnif= new BuButton("Surcharge uniforme  semi infinie");
    final BuButton tbTrap= new BuButton("     Surcharge trap�zoidale     ");
    lin= new DiapreSurcharlin(appli_);
    unif= new DiapreSurcharunif(appli_);
    trap= new DiapreSurchartrap(appli_);
    // Insertion des dessins
    Image img1;
    img1= DiapreResource.DIAPRE.getImage("diapre_2.gif");
    Image img2;
    img2= DiapreResource.DIAPRE.getImage("diapre_4.gif");
    Image img3;
    img3= DiapreResource.DIAPRE.getImage("diapre_3.gif");
    // premi�re colonne
    c.gridx= 0;
    c.gridy= 0;
    placeComposant(lm, tbLin, c);
    tbLin.addActionListener(new DiapreSurcharlinListener());
    c.gridy= 1;
    placeComposant(lm, tbUnif, c);
    tbUnif.addActionListener(new DiapreSurcharunifListener());
    c.gridy= 2;
    placeComposant(lm, tbTrap, c);
    tbTrap.addActionListener(new DiapreSurchartrapListener());
    //deuxieme colonne
    c.gridx= 1;
    c.gridy= 0;
    placeComposant(lm, new BuPicture(img1), c);
    c.gridy= 1;
    placeComposant(lm, new BuPicture(img2), c);
    c.gridy= 2;
    placeComposant(lm, new BuPicture(img3), c);
    setParametresProjet(parametresProjet);
  }
  public void actionPerformed(final ActionEvent e) {
    parametresProjet= getParametresProjet();
    setVisible(true);
  }
  public void setMinAbscisse(final double xmin) {
    minAbscisse= xmin;
    //System.out.println("xmin :: "+xmin);
  }
  public void setMaxAbscisse(final double xmax) {
    maxAbscisse= xmax;
    //System.out.println("xmax :: "+xmax);
  }
  public void setParametresProjet(final SSurchargesProjet parametresProjet_) {
    parametresProjet= parametresProjet_;
    if (parametresProjet != null) {
      if (parametresProjet.nombreSurchargesLineiques > 0) {
        lin= new DiapreSurcharlin(appli_);
        lin.setNbreSurchargesLineiques(
          parametresProjet.nombreSurchargesLineiques);
        lin.setParametresSurcharlin(parametresProjet.surchargesLineiques);
      }
      if (parametresProjet.nombreSurchargeUniforme > 0) {
        unif= new DiapreSurcharunif(appli_);
        unif.setNbreSurchargeUniforme(parametresProjet.nombreSurchargeUniforme);
        unif.setParametresSurcharunif(parametresProjet.surchargeUniforme);
      }
      if (parametresProjet.nombreSurchargesTrapezoidales > 0) {
        trap= new DiapreSurchartrap(appli_);
        trap.setNbreSurchargesTrapezoidales(
          parametresProjet.nombreSurchargesTrapezoidales);
        trap.setParametresSurchartrap(parametresProjet.surchargesTrapezoidales);
      }
    }
  }
  public SSurchargesProjet getParametresProjet() {
    if (parametresProjet == null
      /*&& blocus1
               && blocus2 && blocus3*/
      ) {
      parametresProjet= new SSurchargesProjet();
    }
    //Surcharges lineiques
    parametresProjet.nombreSurchargesLineiques=
      lin.getNbreSurchargesLineiques();
    System.out.println(
      "nbre surcharges lineique : "
        + parametresProjet.nombreSurchargesLineiques);
    parametresProjet.surchargesLineiques= lin.getParametresSurcharlin();
    //Surcharges uniformes
    parametresProjet.surchargeUniforme= unif.getParametresSurcharunif();
    parametresProjet.nombreSurchargeUniforme= unif.getNbreSurchargeUniforme();
    //Surcharges trapezoidales
    parametresProjet.nombreSurchargesTrapezoidales=
      trap.getNbreSurchargesTrapezoidales();
    System.out.println(
      "nbre surcharges trapezoidale : "
        + parametresProjet.nombreSurchargesTrapezoidales);
    parametresProjet.surchargesTrapezoidales= trap.getParametresSurchartrap();
    return parametresProjet;
  }
  class DiapreSurcharlinListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (blocus1) {
        if (parametresProjet != null) {
          lin.setNbreSurchargesLineiques(
            parametresProjet.nombreSurchargesLineiques);
          lin.setParametresSurcharlin(parametresProjet.surchargesLineiques);
        }
        blocus1= false;
      }
      lin.setMinAbscisse(minAbscisse);
      lin.setMaxAbscisse(maxAbscisse);
      lin.setVisible(true);
    }
  }
  class DiapreSurcharunifListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (blocus2) {
        if (parametresProjet != null) {
          unif.setNbreSurchargeUniforme(
            parametresProjet.nombreSurchargeUniforme);
          unif.setParametresSurcharunif(parametresProjet.surchargeUniforme);
        }
        blocus2= false;
      }
      unif.setVisible(true);
    }
  }
  class DiapreSurchartrapListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (blocus3) {
        if (parametresProjet != null) {
          trap.setNbreSurchargesTrapezoidales(
            parametresProjet.nombreSurchargesTrapezoidales);
          trap.setParametresSurchartrap(
            parametresProjet.surchargesTrapezoidales);
        }
        blocus3= false;
      }
      trap.setMinAbscisse(minAbscisse);
      trap.setMaxAbscisse(maxAbscisse);
      trap.setVisible(true);
    }
  }
}
