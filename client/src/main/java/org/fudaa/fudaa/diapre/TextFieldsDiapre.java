/*
 * @file         TextFieldsDiapre.java
 * @creation     2000-11-17
 * @modification $Date: 2007-05-04 13:59:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JComponent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
/**
 * Cette classe permet la saisie des donn�es a deux deux decimal. Il est
 * impossible de saisir plus d'un point. Les valeurs sont automatiquement
 * arrondies � deux d�cimales.
 *
 * @version      $Revision: 1.8 $ $Date: 2007-05-04 13:59:05 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class TextFieldsDiapre extends JComponent implements FocusListener {
  BuTextField zoneTexte= new BuTextField();
  double valmin_;
  double valmax_;
  boolean val_;
  // Compte le nombre de points dans chaine.
  int compteSeparateurs(final String chaine) {
    final int nbChar= chaine.length();
    int nbSepa= 0;
    for (int i= 0; i < nbChar; i++) {
      if (chaine.charAt(i) == '.') {
        nbSepa++;
      }
    }
    return nbSepa;
  }
  // Caract�res valides pour les entiers
  final BuCharValidator CHAR_DIAPRE_ENTIER= new BuCharValidator() {
    public boolean isCharValid(char _char) {
      boolean valid= false;
      if (Character.isDigit(_char) || Character.isISOControl(_char)) {
        valid= true;
      }
      return valid;
    }
  };
  // Caract�res valides pour les d�cimaux
  final BuCharValidator CHAR_DIAPRE_DECIMALE= new BuCharValidator() {
    public boolean isCharValid(char _char) {
      boolean valid= false;
      if ((_char == '.') && (compteSeparateurs(zoneTexte.getText()) == 0)) {
        valid= true;
      } else if (Character.isDigit(_char) || Character.isISOControl(_char)) {
        valid= true;
      }
      return valid;
    }
  };
  // Caract�res valides pour les d�cimaux n�gatifs
  final BuCharValidator CHAR_DIAPRE_NEGATIF= new BuCharValidator() {
    public boolean isCharValid(char _char) {
      boolean valid= false;
      if ((_char == '.') && (compteSeparateurs(zoneTexte.getText()) == 0)) {
        valid= true;
      } else if (Character.isDigit(_char) || Character.isISOControl(_char)) {
        valid= true;
      } else if (_char == '-') {
        valid= true;
      }
      return valid;
    }
  };
  // Valeurs valides pour les entiers
  final BuValueValidator VALUE_DIAPRE_ENTIER= new BuValueValidator() {
    public boolean isValueValid(Object _value) {
      if (_value == null) {
        return false;
      } else if (_value instanceof Integer) {
        if (((Integer)_value).intValue() >= 0) {
          return true;
        }
      }
      return false;
    }
  };
  // Valeurs valides pour les decimaux et decimaux negatif
  final BuValueValidator VALUE_DIAPRE_DECIMALE= new BuValueValidator() {
    public boolean isValueValid(Object _value) {
      if (_value == null || val_) {
        return true;
      } else if (_value instanceof Double) {
        if (((Double)_value).doubleValue() >= 0.0) {
          return true;
        }
      }
      return false;
    }
  };
  // Valeurs valides pour les decimaux compris entre deux valeurs
  final BuValueValidator VALUE_DIAPRE_ENTRE= new BuValueValidator() {
    public boolean isValueValid(Object _value) {
      if (_value == null) {
        return false;
      }
      if (_value.toString().equals("")) {
        return true;
      } else if (_value instanceof Double) {
        if (((Double)_value).doubleValue() >= valmin_
          && ((Double)_value).doubleValue() <= valmax_) {
          return true;
        }
      }
      return false;
    }
  };
  // Chaines valides pour les entiers positif
  final BuStringValidator STRING_DIAPRE_ENTIER= new BuStringValidator() {
    public boolean isStringValid(String _string) {
      try {
        if (!_string.equals("")) {
          new Integer(_string);
        } 
      } catch (Exception e) {
        return false;
      }
      return true;
    }
    public Object stringToValue(String _string) {
      if (_string.equals("")) {
        return "";
      }
      return new Integer(Integer.parseInt(_string));
    }
    public String valueToString(Object value) {
      if (value == null) {
        return "";
      }
      return value.toString();
    }
  };
  // Chaines valides pour les decimaux positfs et negatifs
  final BuStringValidator STRING_DIAPRE_DECIMALE= new BuStringValidator() {
    public boolean isStringValid(String _string) {
      try {
        if (!_string.equals("") ) {
          new Double(_string);
        } 
      } catch (Exception e) {
        return false;
      }
      return true;
    }
    public Object stringToValue(String _string) {
      if (_string.equals("")) {
        return "";
      }
      return new Double(Double.parseDouble(_string));
    }
    public String valueToString(Object value) {
      if (value == null) {
        return new String("");
      }
      if (value.toString().equals("")) {
        return value.toString();
      }
      int arrondi= Math.round(((Double)value).floatValue() * 100);
      return String.valueOf(((double)arrondi) / 100);
    }
  };
  /***************CONSTRUCTEUR***********************/
  /* Constructeur pour entier positif*/
  public TextFieldsDiapre() {
    super();
    zoneTexte.setCharValidator(CHAR_DIAPRE_ENTIER);
    zoneTexte.setValueValidator(VALUE_DIAPRE_ENTIER);
    zoneTexte.setStringValidator(STRING_DIAPRE_ENTIER);
    zoneTexte.setColumns(8);
    zoneTexte.addFocusListener(this);
    // Layout manager pour le composant
    final BuGridLayout lm= new BuGridLayout(2, 5, 5, false, false);
    setLayout(lm);
    // Ajout des composants
    add(zoneTexte);
  }
  /* Constructeur pour decimaux positf et decimaux negatifs
     val = true : negatif  // val = false : positif*/
  public TextFieldsDiapre(final boolean val) {
    super();
    val_= val;
    // Initialisation de la zone de texte
    if (val_) {
      zoneTexte.setCharValidator(CHAR_DIAPRE_NEGATIF);
    } else {
      zoneTexte.setCharValidator(CHAR_DIAPRE_DECIMALE);
    }
    zoneTexte.setValueValidator(VALUE_DIAPRE_DECIMALE);
    zoneTexte.setStringValidator(STRING_DIAPRE_DECIMALE);
    zoneTexte.setColumns(8);
    zoneTexte.addFocusListener(this);
    // Layout manager pour le composant
    final BuGridLayout lm= new BuGridLayout(2, 5, 5, false, false);
    setLayout(lm);
    // Ajout des composants
    add(zoneTexte);
  }
  /* Constructeur pour decimaux negatifs, positifs compris entre deux valeurs*/
  public TextFieldsDiapre(final double valmin, final double valmax) {
    super();
    // initialisation de la valeur maximum et de la valeur minimum
    valmin_= valmin;
    valmax_= valmax;
    // Initialisation de la zone de texte
    if (valmin < 0) {
      zoneTexte.setCharValidator(CHAR_DIAPRE_NEGATIF);
    } else {
      zoneTexte.setCharValidator(CHAR_DIAPRE_DECIMALE);
    }
    zoneTexte.setValueValidator(VALUE_DIAPRE_ENTRE);
    zoneTexte.setStringValidator(STRING_DIAPRE_DECIMALE);
    zoneTexte.setColumns(8);
    zoneTexte.addFocusListener(this);
    // Layout manager pour le composant
    final BuGridLayout lm= new BuGridLayout(2, 5, 5, false, false);
    setLayout(lm);
    // Ajout des composants
    add(zoneTexte);
  }
  // Quand on entre dans la zone de texte c'est que l'on entre dans le composant.
  public void focusGained(final FocusEvent e) {
    zoneTexte.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }
  // Quand on sort de la zone de texte c'est que l'on sort du composant.
  public void focusLost(final FocusEvent e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }
  // Surcharge de setEnabled pour agir sur le label et la zone de texte.
  public void setEnabled(final boolean flag) {
    zoneTexte.setEnabled(flag);
  }
  // Pour lire la valeur de la zone de texte.
  public double toDouble() {
    return ((Double)zoneTexte.getValue()).doubleValue();
  }
  // Pour lire la valeur de la zone de texte.(double)
  public double getValue() {
    return toDouble();
  }
  // Pour lire la valeur de la zone de texte.
  public int toInteger() {
    return ((Integer)zoneTexte.getValue()).intValue();
  }
  //Pour lire la valeur de la zone de texte.(int)
  public int getInt() {
    return toInteger();
  }
  // Lire le valeur de la zone de texte (object)
  public Object getTextDiapre() {
    return zoneTexte.getValue();
  }
  // Ecrire la valeur dans la zone de texte (double)
  public void setValue(final double value) {
    zoneTexte.setValue(new Double(value));
  }
  // Ecrire le valeur dans la zone de texte (int)
  public void setValue(final int value) {
    zoneTexte.setValue(new Integer(value));
  }
  public void setValue(final String value) {
    zoneTexte.setValue(value);
  }
  // Rendre editable la zone de texte
  public void setEditable(final boolean bool) {
    zoneTexte.setEditable(bool);
  }
}
