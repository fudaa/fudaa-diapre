/*
 * @file         DiapreGeometrieParametres.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;

import org.fudaa.dodico.corba.diapre.SPointDiapre;
import org.fudaa.dodico.corba.diapre.STalusDiapre;

/**
 * Classe impl�mentant l'onglet "G�om�trie" pour les param�tres de Diapre Auteur: Jean de Malafosse et Fouzia Elouafi.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author Fouzia Elouafi
 */
public class DiapreGeometrieParametres extends BuPanel implements ActionListener {
  STalusDiapre talus;
  final Object[][] data = { { "point 1 ", "", "" }, { "point 2 ", "", "" }, { "point 3 ", "", "" },
      { "point 4 ", "", "" }, { "point 5 ", "", "" }, { "point 6 ", "", "" }, { "point 7 ", "", "" },
      { "point 8 ", "", "" }, { "point 9 ", "", "" }, { "point 10", "", "" }, { "point 11", "", "" },
      { "point 12", "", "" }, { "point 13", "", "" }, { "point 14", "", "" }, { "point 15", "", "" },
      { "point 16", "", "" }, { "point 17", "", "" }, { "point 18", "", "" }, { "point 19", "", "" },
      { "point 20", "", "" } };
  MyTableModel myModel = new MyTableModel();
  JTable table = new JTable(myModel);
  // BuTextField nombrePoint = new BuTextField().createIntegerField();
  BuCommonInterface appli_;
  DiapreImplementation info;
  static int cpt;
  static char[] chaine = new char[100];
  public DiapreCellEditor anEditor1 = new DiapreCellEditor();
  public DiapreCellEditor anEditor2 = new DiapreCellEditor();

  /**
   * M�thode utile pour placer un composant dans l'onglet.
   * 
   * @param lm layout manager � utiliser.
   * @param composant composant � placer
   * @param c contraintes � utiliser.
   */
  public void placeComposant(final GridBagLayout lm, final Component composant, final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }

  /** Constructeur. appli doit etre instance de DiapreImplementation */
  public DiapreGeometrieParametres(final BuCommonInterface appli) {
    super();
    final DiapreImplementation implementation = (DiapreImplementation) appli.getImplementation();
    appli_ = appli;
    info = implementation;
    // Cr�ation du layout manager
    final GridBagLayout lm = new GridBagLayout();
    final GridBagConstraints c = new GridBagConstraints();
    // Cr�ation de l'onglet lui-meme
    this.setLayout(lm);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    // Contraintes communes � tous les composants
    c.gridy = GridBagConstraints.RELATIVE;
    c.ipadx = 5;
    c.ipady = 5;
    c.gridwidth = 2; // nombre de colonnes pour le composant
    c.gridheight = 1; // nombre de lignes
    // c.weighty = 100; //proportion des lignes
    c.weightx = 100; // proportion des colonnes
    // Premiere colonne
    c.gridx = 0;
    c.gridy = 0;
    c.weighty = 30;
    placeComposant(lm, new BuLabel("  "), c);
    c.gridy = 1;
    c.weighty = 2;
    placeComposant(lm, new BuLabel("D�crivez la g�om�trie du talus et de l'�cran de sout�nement de gauche a droite."),
        c);
    c.gridy = 2;
    c.weighty = 2;
    placeComposant(lm, new BuLabel("    "), c);
    c.gridy = 3;
    c.weighty = 6;
    placeComposant(lm, new BuLabel("L'�cran se trouve � droite, le sol � gauche."), c);
    c.gridy = 4;
    c.gridwidth = 1; // nombre de colonnes pour le composant
    c.weighty = 60;
    table.getColumn(table.getColumnName(1)).setCellEditor(anEditor1);
    table.getColumn(table.getColumnName(2)).setCellEditor(anEditor2);
    table.setRowSelectionInterval(0, 0);
    table.setColumnSelectionInterval(1, 1);
    table.setPreferredScrollableViewportSize(new Dimension(200, 155));
    final JScrollPane scrollPane = new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    // table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    // On regle la taille des colonnes
    // (table.getColumn(table.getColumnName(0))).setPreferredWidth(30);
    /*
     * (table.getColumn(table.getColumnName(1))).setPreferredWidth(15);
     * (table.getColumn(table.getColumnName(2))).setPreferredWidth(15);
     */
    placeComposant(lm, scrollPane, c);
    // deuxieme colonne
    c.gridx = 1;
    c.gridy = 4;
    Image img;
    img = DiapreResource.DIAPRE.getImage("diapre_1.gif");
    placeComposant(lm, new BuPicture(img), c);
    setGeometrie(talus);
  }

  public void actionPerformed(final ActionEvent e) {
    talus = getGeometrie();
    setVisible(true);
  }

  public double getMinTalus() {
    double minTalus;
    int j = 0;
    minTalus = Double.parseDouble(data[0][2].toString());
    while (j < 20 && !data[j][1].toString().equals("") && !data[j][2].toString().equals("")) {
      j++;
    }
    for (int i = 0; i < j - 1; i++) {
      if (minTalus > Double.parseDouble(data[i][2].toString())) {
        minTalus = Double.parseDouble(data[i][2].toString());
      }
    }
    // System.out.println(minTalus);
    return minTalus;
  }

  public double getMaxTalus() {
    double maxTalus;
    int j = 0;
    maxTalus = Double.parseDouble(data[0][2].toString());
    while (j < 20 && !data[j][1].toString().equals("") && !data[j][2].toString().equals("")) {
      j++;
    }
    for (int i = 0; i < j - 1; i++) {
      if (maxTalus < Double.parseDouble(data[i][2].toString())) {
        maxTalus = Double.parseDouble(data[i][2].toString());
      }
    }
    // System.out.println(minTalus);
    return maxTalus;
  }

  // Retour vers les Surcharges
  public double getMinAbscisse() {
    double xmin;
    int i = 0;
    while (i < 20 && !data[i][1].toString().equals("") && !data[i][2].toString().equals("")) {
      i++;
    }
    if (i > 0) {
      xmin = Double.parseDouble(data[0][1].toString());
    } else {
      xmin = 0.0d;
    }
    // System.out.println("geometrie :=> xmin : "+xmin);
    return xmin;
  }

  public double getMaxAbscisse() {
    double xmax;
    int i = 0;
    while (i < 20 && !data[i][1].toString().equals("") && !data[i][2].toString().equals("")) {
      i++;
    }
    if (i > 2) {
      xmax = Double.parseDouble(data[i - 2][1].toString());
    } else {
      xmax = 0.0d;
    }
    // System.out.println("geometrie :=> xmax : "+xmax);
    return xmax;
  }

  // Retour vers les Options
  public double getOrdonnee1() {
    double y1;
    int i = 0;
    while (i < 20 && !data[i][1].toString().equals("") && !data[i][2].toString().equals("")) {
      i++;
    }
    if (i > 2) {
      y1 = Double.parseDouble(data[i - 2][2].toString());
    } else {
      y1 = 0.0d;
    }
    // System.out.println("geometrie :=> y1 : "+y1);
    return y1;
  }

  public double getOrdonnee2() {
    double y2;
    int i = 0;
    while (i < 20 && !data[i][1].toString().equals("") && !data[i][2].toString().equals("")) {
      i++;
    }
    if (i > 2) {
      y2 = Double.parseDouble(data[i - 1][2].toString());
    } else {
      y2 = 0.0d;
    }
    // System.out.println("geometrie :=> y2 : "+y2);
    return y2;
  }

  public STalusDiapre getGeometrie() {
    anEditor1.stopCellEditing();
    anEditor2.stopCellEditing();
    if (talus == null) {
      talus = new STalusDiapre();
    }
    int i = 0;
    while (i < 20 && !data[i][1].toString().equals("") && !data[i][2].toString().equals("")) {
      i++;
    }
    talus.nombrePointsTalus = i;
    System.out.println("nombre point : " + talus.nombrePointsTalus);
    while (i < 20 && data[i][1].toString().equals("") && data[i][2].toString().equals("")) {
      i++;
    }
    if (i > 3 && i < 20) {
      new BuDialogMessage(appli_, DiapreImplementation.isDiapre_, "Le point n� " + (i + 1)
          + " n'est pas pris en compte").activate();
    }
    talus.pointsTalus = new SPointDiapre[talus.nombrePointsTalus];
    for (int j = 0; j < talus.nombrePointsTalus; j++) {
      talus.pointsTalus[j] = new SPointDiapre();
    }
    for (int j = 0; j < talus.nombrePointsTalus; j++) {
      talus.pointsTalus[j].abscissePointDiapre = Double.parseDouble(data[j][1].toString());
      System.out.println("abscisse  : " + (j + 1) + "  " + talus.pointsTalus[j].abscissePointDiapre);
      talus.pointsTalus[j].ordonneePointDiapre = Double.parseDouble(data[j][2].toString());
      System.out.println("ordonnee  : " + (j + 1) + "  " + talus.pointsTalus[j].ordonneePointDiapre);
    }
    return talus;
  }

  public void setGeometrie(final STalusDiapre talus_) {
    talus = talus_;
    if (talus != null) {
      // nombrePoint.setValue(new Integer(talus.nombrePointsTalus));
      for (int j = 0; j < talus.nombrePointsTalus; j++) {
        table.setValueAt(new Double(talus.pointsTalus[j].abscissePointDiapre), j, 1);
        table.setValueAt(new Double(talus.pointsTalus[j].ordonneePointDiapre), j, 2);
      }
    }
  }

  public boolean isNombre(final String _chaine) {
    final int taille = _chaine.length();
    if (taille != 0) {
      int i = 0;
      boolean bool = true;
      while (i < taille
          && (_chaine.charAt(i) >= '0' && _chaine.charAt(i) <= '9' || _chaine.charAt(i) == '-' || _chaine.charAt(i) == '.')) {
        if (i + 1 < taille && _chaine.charAt(i + 1) == '-') {
          return false;
        }
        if (_chaine.charAt(i) == '.') {
          if (!bool) {
            return false;
          }
          bool = false;
        }
        i++;
      }
      return i == taille;
    }
    return false;
  }
  class MyTableModel extends AbstractTableModel {
    final String[] columnNames = { "N�", "x (m)", "y (m)" };

    public int getColumnCount() {
      return columnNames.length;
    }

    public int getRowCount() {
      return data.length;
    }

    public String getColumnName(final int col) {
      return columnNames[col];
    }

    public Object getValueAt(final int row, final int col) {
      return data[row][col];
    }

    public void setValueAt(final Object value, final int row, final int col) {
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          data[row][col] = new String("0" + value.toString());
        } else {
          data[row][col] = value;
        }
      } else {
        data[row][col] = "";
      }
    }

    public boolean isCellEditable(final int row, final int col) {
      return (col > 0);
    }
  }
  /*
   * class TableListener implements KeyListener { String valeur; int longueur; boolean bool; public void
   * keyPressed(KeyEvent e){ KeyStroke key= KeyStroke.getKeyStrokeForEvent(e); int code = key.getKeyCode(); if(code==8 &&
   * chaine[0] != '\u0000' ){ bool=true; cpt--; if(cpt==0) chaine[0]='\u0000'; } else bool=false; if(code==127) {
   * for(int i=0;i<cpt;i++) chaine[i]=chaine[i+1]; cpt--; } if(!(ligne==table.getSelectedRow()) || !(col==
   * table.getSelectedColumn())){ ligne = table.getSelectedRow(); col =table.getSelectedColumn(); valeur =
   * table.getValueAt(ligne,col).toString(); longueur = valeur.length(); //if(bool) cpt=longueur-1; //else cpt=longueur;
   * for(int i=0; i<cpt;i++) chaine[i]=valeur.charAt(i); } } public void keyTyped(KeyEvent e){ KeyStroke key=
   * KeyStroke.getKeyStrokeForEvent(e); char car = key.getKeyChar(); if((car>='0' && car<='9') || car=='-' ||
   * car=='.'){ chaine[cpt] = car; cpt++; } } public void keyReleased(KeyEvent e) { if(table.isCellEditable(ligne, col)) {
   * System.out.println("cpt = "+cpt); if(isNombre(String.valueOf(chaine,0,cpt))) data[ligne][col] =
   * String.valueOf(chaine,0,cpt); else data[ligne][col] = new String(""); } } }
   */
}
