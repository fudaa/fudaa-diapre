/*
 * @file         DiapreSurcharlin.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPicture;

import org.fudaa.dodico.corba.diapre.SSurchargeLineique;
/**
 * Fenetre d'affichage d'un tableau �ditable avec controle des abscisses rentr�es.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreSurcharlin extends JDialog implements ActionListener {
  // fenetre de dessin des surcharges
  //DiapreSurcharlindess  lindess;
  //    protected DiapreImplementation info = new DiapreImplementation();
  // D�claration des variables
  final Object[][] data= { { "SL n�1", "", "", "" }, {
      "SL n�2", "", "", "" }, {
      "SL n�3", "", "", "" }, {
      "SL n�4", "", "", "" }, {
      "SL n�5", "", "", "" }, {
      "SL n�6", "", "", "" }, {
      "SL n�7", "", "", "" }, {
      "SL n�8", "", "", "" }, {
      "SL n�9", "", "", "" }, {
      "SL n�10", "", "", "" }
  };
  //SSurchargesProjet    parametresProjet      = null;
  SSurchargeLineique[] surchargesLineiques;
  private BuDialogMessage message;
  BuCommonInterface appli_;
  //protected BuAssistant          assistant_;
  public DiapreCellEditor anEditor1= new DiapreCellEditor();
  public DiapreCellEditor anEditor2= new DiapreCellEditor();
  public DiapreCellEditor anEditor3= new DiapreCellEditor();
  private double minAbscisse;
  private double maxAbscisse;
  int nombreSurchargesLineiques;
  private boolean erreur;
  boolean sup= true;
  // Zones de saisie
  MyTableModel myModel= new MyTableModel();
  JTable table= new JTable(myModel);
  BuButton graph= new BuButton(" Graphique ");
  BuButton bvalider= new BuButton(" Valider ");
  BuButton bsupprimer= new BuButton(" Supprimer ");
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  // CONSTRUCTEUR /////////////////////////////////////////////////////////
  public DiapreSurcharlin(final BuCommonInterface _appli) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Surcharge lin�ique ",
      true);
    setSize(600, 400);
    //setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    appli_= _appli;
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    //c.anchor      = GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    c.insets.left= 40;
    // premiere colonne de la fenetre
    c.gridx= 0;
    c.gridy= 0;
    c.gridwidth= 2;
    final BuLabel comm= new BuLabel("Rentrer les abscisses dans l'ordre croissant. ");
    placeComposant(lm, comm, c);
    c.gridy= 1;
    c.gridwidth= 1;
    table.setPreferredScrollableViewportSize(new Dimension(250, 160));
    table.getColumn(table.getColumnName(1)).setCellEditor(anEditor1);
    table.getColumn(table.getColumnName(2)).setCellEditor(anEditor2);
    table.getColumn(table.getColumnName(3)).setCellEditor(anEditor3);
    table.setRowSelectionInterval(0, 0);
    table.setColumnSelectionInterval(1, 1);
    final JScrollPane scrollPane= new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //On regle la taille des colonnes
     (table.getColumn(table.getColumnName(0))).setPreferredWidth(55);
    // Couleur du tableau
    //table.setBackground(Color.pink);// couleur de font du tableau
    //table.setForeground(Color.blue);// Couleur du text du tableau
    placeComposant(lm, scrollPane, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, graph, c);
    graph.addActionListener(new DiapreSurcharlindessListener());
    c.gridy= 2;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, bvalider, c);
    bvalider.addActionListener(new DiaprevalidListener());
    // deuxieme colonne
    c.gridx= 1;
    c.gridy= 1;
    c.anchor= GridBagConstraints.CENTER;
    Image img;
    img= DiapreResource.DIAPRE.getImage("diapre_5.gif");
    placeComposant(lm, new BuPicture(img), c);
    c.gridy= 2;
    placeComposant(lm, bsupprimer, c);
    bsupprimer.addActionListener(new DiapreSuppListener());
    //setNbreSurchargesLineiques (nombreSurchargesLineiques);
    //setParametresSurcharlin(surchargesLineiques);
  }
  public void actionPerformed(final ActionEvent e) {
    nombreSurchargesLineiques= getNbreSurchargesLineiques();
    surchargesLineiques= getParametresSurcharlin();
    if (surchargesLineiques != null) {
      setParametresSurcharlin(surchargesLineiques);
    }
    setVisible(true);
  }
  public void setMinAbscisse(final double xmin) {
    //System.out.println("xmin : "+xmin);
    minAbscisse= xmin;
  }
  public void setMaxAbscisse(final double xmax) {
    //System.out.println("xmax : "+xmax);
    maxAbscisse= xmax;
  }
  // Mutateur des parametres de surcharge lineique
  public void setParametresSurcharlin(final SSurchargeLineique[] surchargesLineiques_) {
    surchargesLineiques= surchargesLineiques_;
    if (surchargesLineiques != null) {
      for (int j= 0; j < nombreSurchargesLineiques; j++) {
        table.setValueAt(new Double(surchargesLineiques[j].abscisse), j, 1);
        //System.out.println(surchargesLineiques[j].abscisse);
        table.setValueAt(
          new Double(surchargesLineiques[j].forceVerticale),
          j,
          2);
        table.setValueAt(
          new Double(surchargesLineiques[j].forceHorizontale),
          j,
          3);
      }
    }
  }
  // Accesseur des parametres de surcharge lineique
  public SSurchargeLineique[] getParametresSurcharlin() {
    anEditor1.stopCellEditing();
    anEditor2.stopCellEditing();
    anEditor3.stopCellEditing();
    if (surchargesLineiques == null) {
      surchargesLineiques= new SSurchargeLineique[10];
    }
    int n= 0;
    while (n < 10
      && !data[n][1].toString().equals("")
      && !data[n][2].toString().equals("")
      && !data[n][3].toString().equals("")) {
      n= n + 1;
    }
    nombreSurchargesLineiques= n;
    while (n < 10
      && data[n][1].toString().equals("")
      && data[n][2].toString().equals("")
      && data[n][3].toString().equals("")) {
      n++;
    }
    if (n < 10 && sup) {
      erreur= true;
      if (n == nombreSurchargesLineiques) {
        messageAssistant(
          "Vous devez compl�ter tous les champs"
            + "\nde la surcharge Lin�ique n� "
            + (n + 1));
      } else {
        if (n == nombreSurchargesLineiques + 1) {
          messageAssistant(
            "Vous devez compl�ter la surcharge lin�ique n� "
              + (nombreSurchargesLineiques + 1)
              + "\npour que la surcharge Lin�ique n� "
              + (n + 1)
              + " soit"
              + "\nprise en compte");
        } else {
          messageAssistant(
            "Vous devez compl�ter les surcharges lin�iques n� "
              + (nombreSurchargesLineiques + 1)
              + "\n� "
              + n
              + " pour que la surcharge Lin�ique n� "
              + (n + 1)
              + " soit"
              + "\nprise en compte");
        }
      }
    } else {
      erreur= false;
    }
    for (int i= 0; i < nombreSurchargesLineiques; i++) {
      surchargesLineiques[i]= new SSurchargeLineique();
    }
    // Remplissage des structures:
    for (int j= 0; j < nombreSurchargesLineiques; j++) {
      surchargesLineiques[j].abscisse=
        new Double(data[j][1].toString()).doubleValue();
      surchargesLineiques[j].forceVerticale=
        new Double(data[j][2].toString()).doubleValue();
      surchargesLineiques[j].forceHorizontale=
        new Double(data[j][3].toString()).doubleValue();
      System.out.println(
        "Surcharge "
          + (j + 1)
          + " : abscisse : "
          + surchargesLineiques[j].abscisse);
      System.out.println(
        "Surcharge "
          + (j + 1)
          + " : FV       : "
          + surchargesLineiques[j].forceVerticale);
      System.out.println(
        "Surcharge "
          + (j + 1)
          + " : FH       : "
          + surchargesLineiques[j].forceHorizontale);
    }
    return surchargesLineiques;
  }
  public int getNbreSurchargesLineiques() {
    return nombreSurchargesLineiques;
  }
  public void setNbreSurchargesLineiques(final int nbre) {
    nombreSurchargesLineiques= nbre;
    //if(nbre>0) bsupprimer.setEnabled(true);
  }
  /*--- message de l'assistant ---*/
  private void messageAssistant(final String s) {
    message= new BuDialogMessage(appli_, appli_.getInformationsSoftware(), s);
    message.setSize(500, 150);
    message.setTitle("ERREUR");
    message.setResizable(false);
    message.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    final Point pos= this.getLocationOnScreen();
    pos.x= pos.x + this.getWidth() / 2 - message.getWidth() / 2;
    pos.y= pos.y + this.getHeight() / 2 - message.getHeight() / 2;
    message.setLocation(pos);
    message.setVisible(true);
  }
  boolean testErreur() {
    sup= true;
    getParametresSurcharlin();
    final int[] bool= new int[10];
    final int[] bool1= new int[10];
    for (int i= 0; i < 10; i++) {
      bool[i]= 1;
      bool1[i]= 1;
    }
    // Si tout est bon, on ferme la fenetre !
    for (int l= 0; l < nombreSurchargesLineiques; l++) {
      final int o= l + 1;
      if (((Double.parseDouble(data[l][1].toString()) > minAbscisse)
        || (Double.parseDouble(data[l][1].toString()) < maxAbscisse))) {
        bool[l]= 1; // tout est bon
        //System.out.println(l+"1 : " +bool[l]);
      }
      if (((Double.parseDouble(data[l][1].toString()) < minAbscisse)
        || (Double.parseDouble(data[l][1].toString()) > maxAbscisse))) {
        bool[l]= 0; // il y a une faute
        messageAssistant(
          "Erreur � la surcharge "
            + o
            + " :\n"
            + data[l][1]
            + " : cette abscisse n'appartient pas � ["
            + minAbscisse
            + " , "
            + maxAbscisse
            + "]");
        //System.out.println(l+"2 : " +bool[l]);
      }
      if (l > 0) {
        if (Double.parseDouble(data[l][1].toString())
          <= Double.parseDouble(data[l - 1][1].toString())) {
          bool1[l]= 0; //d�croissance
          messageAssistant(
            "Les abscisses des lignes "
              + l
              + " et "
              + o
              + " \ndoivent �tre strictement croissantes.");
          //System.out.println(l+"3 : "+bool[l]);
        }
        if (Double.parseDouble(data[l][1].toString())
          > Double.parseDouble(data[l - 1][1].toString())) {
          bool1[l]= 1;
          //System.out.println(l+"4 : "+bool[l]);
        }
      }
    }
    if ((bool[0] == 1)
      && (bool[1] == 1)
      && bool[2] == 1
      && bool[3] == 1
      && bool[4] == 1
      && bool[5] == 1
      && bool[6] == 1
      && bool[7] == 1
      && bool[8] == 1
      && bool[9] == 1
      && bool1[0] == 1
      && (bool1[1] == 1)
      && bool1[2] == 1
      && bool1[3] == 1
      && bool1[4] == 1
      && bool1[5] == 1
      && bool1[6] == 1
      && bool1[7] == 1
      && bool1[8] == 1
      && bool1[9] == 1
      && !erreur) {
      getParametresSurcharlin();
      if (!erreur) {
        if (getNbreSurchargesLineiques() == 0) {
          messageAssistant("Aucune surcharge lin�ique" + " n'a �t� d�finie");
          return false;
        }
        return true;
      }
      return true;
    }
    return false;
  }
  // BOUTTONS
  class DiapreSurcharlindessListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (testErreur()) {
        System.out.println("ok");
      //new DiapreConstruitGrapheRappel(appli,1);
      }
    }
  }
  class DiaprevalidListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (testErreur()) {
        dispose();
      }
    }
  }
  class DiapreSuppListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      sup= false;
      getNbreSurchargesLineiques();
      getParametresSurcharlin();
      if (getNbreSurchargesLineiques() > 0) {
        getParametresSurcharlin();
        final BuDialogConfirmation mess=
          new BuDialogConfirmation(
            appli_,
            appli_.getInformationsSoftware(),
            "Voulez vous supprimer toutes les surcharges \nlin�iques?");
        mess.setTitle("ATTENTION");
        if (mess.activate() == JOptionPane.YES_OPTION) {
          for (int row= 0; row < nombreSurchargesLineiques; row++) {
            for (int col= 1; col < 4; col++) {
              table.setValueAt(new String(""), row, col);
            }
          }
          setNbreSurchargesLineiques(0);
          getParametresSurcharlin();
          dispose();
        }
      } else {
        for (int row= 0; row < 10; row++) {
          for (int col= 1; col < 4; col++) {
            table.setValueAt(new String(""), row, col);
          }
        }
        setNbreSurchargesLineiques(0);
        getParametresSurcharlin();
        dispose();
      }
    }
  }
  // TABLE MODEL
  class MyTableModel extends AbstractTableModel {
    final String[] columnNames= { "N�", "x (m)", "V", "H" };
    public int getColumnCount() {
      return columnNames.length;
    }
    public int getRowCount() {
      return data.length;
    }
    public String getColumnName(final int col) {
      return columnNames[col];
    }
    public Object getValueAt(final int row, final int col) {
      return data[row][col];
    }
    /*public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }*/
    public boolean isCellEditable(final int row, final int col) {
      if (col < 1) {
        return false;
      }
      return true;
    }
    public boolean isNombre(final String chaine) {
      final int taille= chaine.length();
      if (taille != 0) {
        int i= 0;
        boolean bool= true;
        while (i < taille
          && (chaine.charAt(i) >= '0'
            && chaine.charAt(i) <= '9'
            || chaine.charAt(i) == '-'
            || chaine.charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool= false;
          }
          i++;
        }
        if (i == taille) {
          return true;
        }
        return false;
      }
      return false;
    }
    // RECUPERATION DES DONNEES
    public void setValueAt(final Object value, final int row, final int col) {
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          data[row][col]= new String("0" + value.toString());
        } else {
          data[row][col]= value;
        }
      } else {
        data[row][col]= new String("");
      }
    }
  }
}
