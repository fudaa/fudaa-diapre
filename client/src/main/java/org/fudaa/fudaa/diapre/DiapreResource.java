/*
 * @file         DiapreResource.java
 * @creation     2000-10-06
 * @modification $Date: 2005-08-16 13:27:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import com.memoire.bu.BuResource;
/**
 * Ressources pour Diapre.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 13:27:38 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class DiapreResource extends BuResource {
  public final static String PARAMETRES= "parametres";
  public final static String RESULTATS= "resultats";
  public final static DiapreResource DIAPRE= new DiapreResource();
  //public DiapreResource(BuResource _parent) { setParent(_parent); }
}
