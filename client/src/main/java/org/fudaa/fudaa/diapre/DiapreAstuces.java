/*
 * @file         DiapreAstuces.java
 * @creation     2001-10-18
 * @modification $Date: 2007-01-19 13:14:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
/**
 * @version      $Id: DiapreAstuces.java,v 1.10 2007-01-19 13:14:34 deniger Exp $
 * @author       Axel von Arnim 
 */
public class DiapreAstuces extends FudaaAstucesAbstract {
  public static DiapreAstuces DIAPRE= new DiapreAstuces();
  protected FudaaAstucesAbstract getParent() {
    return FudaaAstuces.FUDAA;
  }
  protected BuPreferences getPrefs() {
    return DiaprePreferences.DIAPRE;
  }
  /*
  public static void main( String argv[])
  {
    System.out.println("nb ligne de Fudaa "+FudaaAstuces.FUDAA.getNombreLigne());
    System.out.println("nb ligne de Diapre "+DiapreAstuces.DIAPRE.getNombreLigne());
    System.out.println(DiapreAstuces.DIAPRE.getAstuce());
  }*/
}
