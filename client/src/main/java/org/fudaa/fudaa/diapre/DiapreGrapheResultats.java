/*
 * @file         DiapreGrapheResultats.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;
/**
 * put your module comment here
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
class DiapreGrapheResultats extends BGraphe implements PropertyChangeListener {
  public static final int margeGauche= 60;
  public static final int margeDroite= 150;
  private double[][] valeursX_;
  private double[][] valeursY_;
  private String nomaxeX_;
  private String nomaxeY_;
  private String[] nomcol_;
  private Graphe graphe_;
  private Axe axeX_;
  private Axe axeY_;
  private CourbeDefault courbe_;
  boolean surchargeUniforme_;
  double[] surchargeUniformeF_;
  int surchargesLineiques_;
  double[][] surchargeLineique_;
  int surchargesTrapezoidales_;
  double[][] surchargeTrapezoidale_;
  boolean eau_;
  private final Color[] col=
    { Color.black, Color.blue, Color.red, Color.magenta, Color.green.darker()};
  private final Vector axesY= new Vector(); //Vecteur d'axe Y
  private final Vector axesX= new Vector();
  public DiapreGrapheResultats(
    final double[][] _valeursX,
    final double[][] _valeursY,
    final String _nomaxeX,
    final String _nomaxeY,
    final String[] _nomcol,
    final boolean _eau,
    final boolean _surchargeUniforme,
    final double[] _surchargeUniformeF,
    final int _surchargesLineiques,
    final double[][] _surchargeLineique,
    final int _surchargesTrapezoidales,
    final double[][] _surchargeTrapezoidale) {
    graphe_= null;
    axeX_= null;
    axeY_= null;
    valeursX_= _valeursX;
    valeursY_= _valeursY;
    nomaxeX_= _nomaxeX;
    nomaxeY_= _nomaxeY;
    nomcol_= _nomcol;
    surchargeUniforme_= _surchargeUniforme;
    surchargeUniformeF_= _surchargeUniformeF;
    surchargesLineiques_= _surchargesLineiques;
    surchargeLineique_= _surchargeLineique;
    surchargeTrapezoidale_= _surchargeTrapezoidale;
    surchargesTrapezoidales_= _surchargesTrapezoidales;
    eau_= _eau;
    initGraphe();
  }
  //Cette fonction renvoie un axe bien dimensionn�
  private Axe createAxe(final double[] _tab, final String _titre, final boolean bool) {
    final Axe axe= new Axe();
    axe.titre_= _titre;
    axe.vertical_= bool;
    axe.graduations_= true;
    axe.grille_= false;
    //Calcul du min et du max
    double min= _tab[0];
    double max= _tab[0];
    for (int i= 0; i < _tab.length; i++) {
      min= (_tab[i] < min) ? _tab[i] : min;
      max= (_tab[i] > max) ? _tab[i] : max;
    }
    //Calcul du pas � l'aide de min/max
    axe.pas_= (max - min) / 10;
    axe.minimum_= min;
    axe.maximum_= max + 3 * axe.pas_ / 2;
    axe.visible_= true;
    axesY.add(axe);
    axesX.add(axe);
    return axe;
  }
  /**
   * put your documentation comment here
   */
  public void initGraphe() {
    final Marges marges= new Marges();
    graphe_= new Graphe();
    graphe_.animation_= false;
    graphe_.legende_= false;
    graphe_.copyright_= false;
    graphe_.marges_= marges;
    //Definition de la marge en fonction du nombre de courbes
    marges.gauche_= margeGauche;
    marges.droite_= margeDroite;
    marges.haut_= 45;
    marges.bas_= 30;
    axeX_= new Axe();
    axeY_= new Axe();
    axeY_= createAxe(valeursY_[0], nomaxeY_, true);
    axeX_= createAxe(valeursX_[0], nomaxeX_, false);
    graphe_.ajoute(axeX_);
    graphe_.ajoute(axeY_);
    Valeur v= new Valeur();
    //talus et rideau
    courbe_= new CourbeDefault();
    courbe_.marqueurs_= false;
    Aspect asp= new Aspect();
    courbe_.titre_= nomcol_[0];
    asp.contour_= col[0];
    asp.surface_= col[0];
    courbe_.aspect_= asp;
    Vector valcourbe= new Vector();
    for (int i= 0; i < valeursX_[0].length; i++) {
      v= new Valeur();
      v.v_= valeursY_[0][i];
      v.s_= valeursX_[0][i];
      valcourbe.add(v);
    }
    courbe_.valeurs_= valcourbe;
    courbe_.axe_= 0;
    courbe_.visible_= true;
    graphe_.legende_= true;
    graphe_.ajoute(courbe_);
    //couche de sol
    final int nbCoucheSol= valeursX_.length - 2;
    for (int u= 0; u < nbCoucheSol; u++) {
      courbe_= new CourbeDefault();
      courbe_.marqueurs_= false;
      courbe_.aspect_= asp;
      valcourbe= new Vector();
      v= new Valeur();
      v.v_= valeursY_[u + 1][0];
      v.s_= valeursX_[u + 1][0];
      valcourbe.add(v);
      v= new Valeur();
      v.v_= valeursY_[u + 1][1];
      v.s_= valeursX_[u + 1][1];
      valcourbe.add(v);
      courbe_.valeurs_= valcourbe;
      courbe_.axe_= 0;
      courbe_.visible_= true;
      graphe_.legende_= true;
      graphe_.ajoute(courbe_);
      //System.out.println("bonjour");
    }
    //eau
    courbe_= new CourbeDefault();
    courbe_.marqueurs_= false;
    asp= new Aspect();
    courbe_.titre_= nomcol_[1];
    asp.contour_= col[1];
    asp.surface_= col[1];
    courbe_.aspect_= asp;
    valcourbe= new Vector();
    v= new Valeur();
    v.v_= valeursY_[nbCoucheSol + 1][0];
    v.s_= valeursX_[nbCoucheSol + 1][0];
    valcourbe.add(v);
    v= new Valeur();
    v.v_= valeursY_[nbCoucheSol + 1][1];
    v.s_= valeursX_[nbCoucheSol + 1][1];
    valcourbe.add(v);
    courbe_.valeurs_= valcourbe;
    courbe_.axe_= 0;
    courbe_.visible_= true;
    graphe_.legende_= true;
    graphe_.ajoute(courbe_);
    double retour[][];
    if (surchargeUniforme_) {
      retour= dessineSurchargeUniforme();
    } else {
      retour= null;
    }
    if (surchargesTrapezoidales_ > 0) {
      dessineSurchargesTrapezoidales(retour);
    }
    if (surchargesLineiques_ > 0) {
      dessineSurchargesLineiques();
    }
    graphe_.visible_= true;
    setInteractif(true);
    setGraphe(graphe_);
    fullRepaint();
  }
  //--------------------------Surcharge Uniforme-------------------------//
  public double[][] dessineSurchargeUniforme() {
    int u= 0;
    double m, p;
    Valeur v;
    Vector valcourbe;
    final Aspect asp= new Aspect();
    asp.contour_= col[2];
    asp.surface_= col[2];
    double retour[][]= new double[2][41];
    double max= 0, pasX, pasY;
    max= valeursX_[0][valeursX_[0].length - 2] - valeursX_[0][0];
    pasX= max / 40;
    double m1= valeursY_[0][0];
    double m2= valeursY_[0][0];
    for (int i= 0; i < valeursY_[0].length; i++) {
      m1= (valeursY_[0][i] < m1) ? valeursY_[0][i] : m1;
      m2= (valeursY_[0][i] > m2) ? valeursY_[0][i] : m2;
    }
    pasY= (m2 - m1) / 40;
    m=
      (valeursY_[0][u] - valeursY_[0][u + 1])
        / (valeursX_[0][u] - valeursX_[0][u + 1]);
    p= valeursY_[0][u] - m * valeursX_[0][u];
    if (surchargeUniformeF_[0] != 0 || surchargeUniformeF_[1] != 0) {
      //construction des hachures
      final double valF[][]= new double[4][2];
      double valX[]= new double[41];
      final double valY[]= new double[41];
      double angle=
        Math.acos(
          (-1 * surchargeUniformeF_[1])
            / Math.sqrt(
              surchargeUniformeF_[1] * surchargeUniformeF_[1]
                + surchargeUniformeF_[0] * surchargeUniformeF_[0]));
      if (surchargeUniformeF_[0] < 0) {
        angle= 2 * Math.PI - angle;
      }
      for (int i= 0; i * pasX <= max; i++) {
        while (u < valeursX_[0].length - 2
          && valeursX_[0][u + 1] < valeursX_[0][0] + i * pasX) {
          u++;
          m=
            (valeursY_[0][u] - valeursY_[0][u + 1])
              / (valeursX_[0][u] - valeursX_[0][u + 1]);
          p= valeursY_[0][u] - m * valeursX_[0][u];
        }
        //tete de la fleche
        valF[0][0]= valeursX_[0][0] + i * pasX;
        valF[0][1]= m * valF[0][0] + p;
        //queue de la fleche
        valX[i]= valF[1][0]= valF[0][0] + (2 * pasX) * Math.cos(angle);
        valY[i]= valF[1][1]= valF[0][1] + (2 * pasY) * Math.sin(angle);
        //fleche gauche
        valF[2][0]=
          valF[0][0] - (pasX / 2) * Math.cos(Math.PI - angle - Math.PI / 6);
        valF[2][1]=
          valF[0][1] + (pasY / 2) * Math.sin(Math.PI - angle - Math.PI / 6);
        //fleche droite
        valF[3][0]= valF[0][0] + (pasX / 2) * Math.cos(angle - Math.PI / 6);
        valF[3][1]= valF[0][1] + (pasY / 2) * Math.sin(angle - Math.PI / 6);
        for (int f= 1; f < 4; f++) {
          courbe_= new CourbeDefault();
          courbe_.marqueurs_= false;
          courbe_.aspect_= asp;
          valcourbe= new Vector();
          v= new Valeur();
          v.s_= valF[0][0];
          v.v_= valF[0][1];
          valcourbe.add(v);
          v= new Valeur();
          v.s_= valF[f][0];
          v.v_= valF[f][1];
          valcourbe.add(v);
          courbe_.valeurs_= valcourbe;
          courbe_.axe_= 0;
          courbe_.visible_= true;
          graphe_.legende_= true;
          graphe_.ajoute(courbe_);
        }
      }
      //construction de la forme generale de la surcharge
      courbe_= new CourbeDefault();
      courbe_.marqueurs_= false;
      courbe_.titre_= "Surcharge uniforme";
      courbe_.aspect_= asp;
      valcourbe= new Vector();
      for (int i= 0; i < valX.length; i++) {
        v= new Valeur();
        v.v_= valY[i];
        //System.out.println(v.v);
        v.s_= valX[i];
        //System.out.println(v.s);
        valcourbe.add(v);
      }
      courbe_.valeurs_= valcourbe;
      courbe_.axe_= 0;
      courbe_.visible_= true;
      graphe_.legende_= true;
      graphe_.ajoute(courbe_);
      for (int i= 0; i < 2; i++) {
        for (int j= 0; j < 41; j++) {
          retour[i][j]= valX[j];
        }
        valX= valY;
      }
    } else {
      surchargeUniforme_= false;
      retour= null;
    }
    return retour;
  }
  //-------------------------------------Surcharge Lineique------------------//
  public void dessineSurchargesLineiques() {
    final Aspect asp= new Aspect();
    asp.contour_= col[3];
    asp.surface_= col[3];
    boolean blocus1= true;
    int u= 0;
    double m, p;
    m=
      (valeursY_[0][u] - valeursY_[0][u + 1])
        / (valeursX_[0][u] - valeursX_[0][u + 1]);
    p= valeursY_[0][u] - m * valeursX_[0][u];
    for (int i= 0; i < surchargesLineiques_; i++) {
      while (u < valeursX_[0].length - 1
        && valeursX_[0][u + 1] < surchargeLineique_[i][0]) {
        u++;
        m=
          (valeursY_[0][u] - valeursY_[0][u + 1])
            / (valeursX_[0][u] - valeursX_[0][u + 1]);
        p= valeursY_[0][u] - m * valeursX_[0][u];
      }
      final double valF[][]= new double[4][2];
      double angle=
        Math.acos(
          (-1 * surchargeLineique_[i][2])
            / Math.sqrt(
              surchargeLineique_[i][2] * surchargeLineique_[i][2]
                + surchargeLineique_[i][1] * surchargeLineique_[i][1]));
      if (surchargeLineique_[i][1] < 0) {
        angle= 2 * Math.PI - angle;
      }
      //tete de fleche
      valF[0][0]= surchargeLineique_[i][0];
      valF[0][1]= m * valF[0][0] + p;
      //queue de fleche
      valF[1][0]= valF[0][0] + (3 * axeX_.pas_ / 2) * Math.cos(angle);
      valF[1][1]= valF[0][1] + (3 * axeY_.pas_ / 2) * Math.sin(angle);
      //fleche gauche
      valF[2][0]=
        valF[0][0] - (axeX_.pas_ / 4) * Math.cos(Math.PI - angle - Math.PI / 6);
      valF[2][1]=
        valF[0][1] + (axeY_.pas_ / 4) * Math.sin(Math.PI - angle - Math.PI / 6);
      //fleche droite
      valF[3][0]= valF[0][0] + (axeX_.pas_ / 4) * Math.cos(angle - Math.PI / 6);
      valF[3][1]= valF[0][1] + (axeY_.pas_ / 4) * Math.sin(angle - Math.PI / 6);
      for (int f= 1; f < 4; f++) {
        courbe_= new CourbeDefault();
        courbe_.marqueurs_= false;
        courbe_.aspect_= asp;
        if (blocus1) {
          courbe_.titre_= "Surcharge lin�ique";
          blocus1= false;
        }
        final Vector valcourbe= new Vector();
        Valeur v= new Valeur();
        v.s_= valF[0][0];
        v.v_= valF[0][1];
        valcourbe.add(v);
        v= new Valeur();
        v.s_= valF[f][0];
        v.v_= valF[f][1];
        valcourbe.add(v);
        courbe_.valeurs_= valcourbe;
        courbe_.axe_= 0;
        courbe_.visible_= true;
        graphe_.legende_= true;
        graphe_.ajoute(courbe_);
      }
    }
  }
  //-----------------------Surcharge Trapezoidale----------------------//
  public void dessineSurchargesTrapezoidales(final double retour[][]) {
    final Aspect asp= new Aspect();
    asp.contour_= col[4];
    asp.surface_= col[4];
    Vector valcourbe;
    Valeur v;
    //CourbeDefault courbe_;
    boolean blocus2= true;
    double m, p;
    double pasX, pasY;
    int u= 0;
    if (retour == null) {
      m=
        (valeursY_[0][u] - valeursY_[0][u + 1])
          / (valeursX_[0][u] - valeursX_[0][u + 1]);
      p= valeursY_[0][u] - m * valeursX_[0][u];
    } else {
      m= (retour[1][1] - retour[1][0]) / (retour[0][1] - retour[0][0]);
      p= retour[1][0] - m * retour[0][0];
      //System.out.println("bonjour");
    }
    for (int i= 0; i < surchargesTrapezoidales_; i++) {
      pasX=
        (surchargeTrapezoidale_[i][1] - surchargeTrapezoidale_[i][0])
          / (4
            * (surchargeTrapezoidale_[i][1] - surchargeTrapezoidale_[i][0])
            / axeX_.pas_);
      pasY= axeY_.pas_ / 8;
      if ((surchargeTrapezoidale_[i][5] != 0
        || surchargeTrapezoidale_[i][3] != 0) //H2,V2
        && (surchargeTrapezoidale_[i][4] != 0
          || surchargeTrapezoidale_[i][2] != 0)) { //H1,V1
        double a1, a2, b1, b2;
        //equation de FH
        a1=
          (surchargeTrapezoidale_[i][5] - surchargeTrapezoidale_[i][4])
            / (surchargeTrapezoidale_[i][1] - surchargeTrapezoidale_[i][0]);
        b1= surchargeTrapezoidale_[i][4] - a1 * surchargeTrapezoidale_[i][0];
        //equation de FV
        a2=
          (surchargeTrapezoidale_[i][3] - surchargeTrapezoidale_[i][2])
            / (surchargeTrapezoidale_[i][1] - surchargeTrapezoidale_[i][0]);
        b2= surchargeTrapezoidale_[i][2] - a2 * surchargeTrapezoidale_[i][0];
        if (retour == null) {
          while (u < valeursX_[0].length - 1
            && valeursX_[0][u + 1] < surchargeTrapezoidale_[i][0]) {
            u++;
          }
          m=
            (valeursY_[0][u] - valeursY_[0][u + 1])
              / (valeursX_[0][u] - valeursX_[0][u + 1]);
          p= valeursY_[0][u] - m * valeursX_[0][u];
        } else {
          while (u < retour[0].length - 2
            && retour[0][u + 1] <= surchargeTrapezoidale_[i][0]) {
            u++;
          }
          m=
            (retour[1][u] - retour[1][u + 1])
              / (retour[0][u] - retour[0][u + 1]);
          p= retour[1][u] - m * retour[0][u];
          //System.out.println("bonjour");
        }
        int cpt= 0, h= 0, nbFleche= 0;
        while (cpt * pasX < surchargeTrapezoidale_[i][0]) {
          cpt++;
        }
        h= cpt;
        while (h * pasX <= surchargeTrapezoidale_[i][1]) {
          h++;
          nbFleche++;
        }
        h= 0;
        final double valX[]= new double[nbFleche];
        final double valY[]= new double[nbFleche];
        for (int y= cpt; y * pasX <= surchargeTrapezoidale_[i][1]; y++) {
          final double fv= a2 * y * pasX + b2;
          final double fh= a1 * y * pasX + b1;
          double angle= Math.acos((-1 * fh) / Math.sqrt(fh * fh + fv * fv));
          if (fv < 0) {
            angle= 2 * Math.PI - angle;
          }
          //System.out.println(Math.toDegrees(angle));
          final double valF[][]= new double[4][2];
          if (retour == null) {
            while (u < valeursX_[0].length - 1
              && valeursX_[0][u + 1] < y * pasX) {
              u++;
              m=
                (valeursY_[0][u] - valeursY_[0][u + 1])
                  / (valeursX_[0][u] - valeursX_[0][u + 1]);
              p= valeursY_[0][u] - m * valeursX_[0][u];
              //System.out.println("bonjour");
            }
          } else {
            while (u < retour[0].length - 2 && retour[0][u + 1] <= y * pasX) {
              u++;
              m=
                (retour[1][u] - retour[1][u + 1])
                  / (retour[0][u] - retour[0][u + 1]);
              p= retour[1][u] - m * retour[0][u];
              //System.out.println("bonjour");
            }
          }
          //tete de la fleche
          valF[0][0]= y * pasX;
          valF[0][1]= m * valF[0][0] + p;
          //queue de la fleche
          valX[h]= valF[1][0]= valF[0][0] + (4 * pasX) * Math.cos(angle);
          valY[h]= valF[1][1]= valF[0][1] + (4 * pasY) * Math.sin(angle);
          //fleche gauche
          valF[2][0]=
            valF[0][0] - (pasX) * Math.cos(Math.PI - angle - Math.PI / 6);
          valF[2][1]=
            valF[0][1] + (pasY) * Math.sin(Math.PI - angle - Math.PI / 6);
          //fleche droite
          valF[3][0]= valF[0][0] + (pasX) * Math.cos(angle - Math.PI / 6);
          valF[3][1]= valF[0][1] + (pasY) * Math.sin(angle - Math.PI / 6);
          h++;
          for (int f= 1; f < 4; f++) {
            courbe_= new CourbeDefault();
            courbe_.marqueurs_= false;
            courbe_.aspect_= asp;
            valcourbe= new Vector();
            v= new Valeur();
            v.s_= valF[0][0];
            v.v_= valF[0][1];
            valcourbe.add(v);
            v= new Valeur();
            v.s_= valF[f][0];
            v.v_= valF[f][1];
            valcourbe.add(v);
            courbe_.valeurs_= valcourbe;
            courbe_.axe_= 0;
            courbe_.visible_= true;
            graphe_.legende_= true;
            graphe_.ajoute(courbe_);
          }
        }
        //construction de la forme generale de la surcharge
        courbe_= new CourbeDefault();
        courbe_.marqueurs_= false;
        if (blocus2) {
          courbe_.titre_= "Surcharge trap�zoidale";
          blocus2= false;
        }
        courbe_.aspect_= asp;
        valcourbe= new Vector();
        for (int j= 0; j < nbFleche; j++) {
          v= new Valeur();
          v.v_= valY[j];
          v.s_= valX[j];
          valcourbe.add(v);
        }
        courbe_.valeurs_= valcourbe;
        courbe_.axe_= 0;
        courbe_.visible_= true;
        graphe_.legende_= true;
        graphe_.ajoute(courbe_);
      } else {
        surchargesTrapezoidales_= 0;
      }
    }
  }
  public void propertyChange(final PropertyChangeEvent _evt) {
    fullRepaint();
  }
  public Axe getAxeX() {
    return axeX_;
  }
  public void axeXEstVisible(final boolean _b) {
    axeX_.visible_= _b;
  }
  public void graduationsXEstVisible(final boolean _b) {
    axeX_.graduations_= _b;
  }
  public Axe getAxeY(final int numeroAxe) {
    return (Axe)axesY.elementAt(numeroAxe);
  }
}
