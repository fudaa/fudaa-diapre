/*
 * @file         DiapreDiagrammePression.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;
/**
 * Fenetre d'affichage d'un diagramme des pressions.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreDiagrammePression
  extends BGraphe
  implements PropertyChangeListener {
  public static final int margeGauche= 60;
  public static final int margeDroite= 150;
  private double[][] valeursX_;
  private double[][] valeursY_;
  private String nomaxeX_;
  private String nomaxeY_;
  //private String[] nomcol_;
  private Graphe graphe_;
  private Axe axeX_;
  //private Axe axeY_;
  private CourbeDefault courbe_; //  pression totale
  private CourbeDefault courbeE_; // pression effective
  private CourbeDefault courbeI_; // pression interstitielle
  private final Color[] col=
    { Color.black, Color.blue, Color.green, Color.orange, Color.pink };
  private final Vector axesY= new Vector(); //Vecteur d'axe Y
  private final Vector axesX= new Vector();
  public DiapreDiagrammePression(
    final double[][] _valeursX,
    final double[][] _valeursY,
    final String _nomaxeX,
    final String _nomaxeY,
    final String[] _nomcol) {
    graphe_= null;
    axeX_= null;
    //axeY_= null;
    valeursX_= _valeursX;
    valeursY_= _valeursY;
    nomaxeX_= _nomaxeX;
    nomaxeY_= _nomaxeY;
    //nomcol_= _nomcol;
    initGraphe();
  }
  //Cette fonction renvoie un axe bien dimensionn�
  private Axe createAxe(final double[] _tab, final String _titre, final boolean bool) {
    final Axe axe= new Axe();
    axe.titre_= _titre;
    axe.vertical_= bool;
    axe.graduations_= true;
    axe.grille_= false;
    //Calcul du min et du max
    double min= _tab[0];
    double max= _tab[0];
    for (int i= 0; i < _tab.length; i++) {
      min= (_tab[i] < min) ? _tab[i] : min;
      max= (_tab[i] > max) ? _tab[i] : max;
    }
    //Calcul du pas � l'aide de min/max
    int i= 0;
    while (i < (max - min) / 10) {
      i++;
    }
    axe.pas_= i;
    axe.minimum_= min;
    axe.maximum_= max + 5;
    axe.visible_= true;
    axesY.add(axe);
    axesX.add(axe);
    return axe;
  }
  public void initGraphe() {
    final Marges marges= new Marges();
    graphe_= new Graphe();
    graphe_.animation_= false;
    graphe_.legende_= false;
    graphe_.copyright_= false;
    graphe_.marges_= marges;
    //Definition de la marge
    marges.gauche_= margeGauche;
    marges.droite_= margeDroite;
    marges.haut_= 45;
    marges.bas_= 30;
    Axe axeX= new Axe();
    Axe axeY= new Axe();
    axeY= createAxe(valeursY_[0], nomaxeY_, true);
    axeX= createAxe(valeursX_[0], nomaxeX_, false);
    graphe_.ajoute(axeX);
    graphe_.ajoute(axeY);
    courbe_= new CourbeDefault();
    courbeE_= new CourbeDefault();
    courbeI_= new CourbeDefault();
    courbe_.titre_= "Pression totale";
    if (valeursX_[3][1] == 1) {
      courbeE_.titre_= "Pression Effective";
      courbeI_.titre_= "Pression Interstitielle";
    }
    //courbe_.trace = "lineaire";
    courbe_.marqueurs_= false;
    courbeE_.marqueurs_= false;
    courbeI_.marqueurs_= false;
    // Couleurs des courbes
    final Aspect asp= new Aspect();
    asp.contour_= col[0];
    asp.surface_= col[0];
    courbe_.aspect_= asp;
    final Aspect asp1= new Aspect();
    asp1.contour_= col[1];
    asp1.surface_= col[1];
    courbeE_.aspect_= asp1;
    final Aspect asp2= new Aspect();
    asp2.contour_= col[2];
    asp2.surface_= col[2];
    courbeI_.aspect_= asp2;
    // courbe pression totale
    final Vector valcourbe= new Vector();
    for (int i= 0; i < valeursY_[0].length; i++) {
      final Valeur v= new Valeur();
      v.v_= valeursY_[0][i];
      v.s_= valeursX_[0][i];
      valcourbe.add(v);
    }
    // courbe pression effective
    final Vector valcourbeE= new Vector();
    if (valeursX_[3][1] == 1) {
      for (int j= 0; j < valeursY_[0].length; j++) {
        final Valeur v1= new Valeur();
        v1.v_= valeursY_[0][j];
        v1.s_= valeursX_[1][j];
        valcourbeE.add(v1);
      }
    }
    // courbe pression interstitielle
    final Vector valcourbeI= new Vector();
    if (valeursX_[3][1] == 1) {
      for (int k= 0; k < valeursY_[0].length; k++) {
        final Valeur v2= new Valeur();
        v2.v_= valeursY_[0][k];
        v2.s_= valeursX_[2][k];
        valcourbeI.add(v2);
      }
    }
    courbe_.valeurs_= valcourbe;
    courbeE_.valeurs_= valcourbeE;
    courbeI_.valeurs_= valcourbeI;
    courbe_.axe_= 0;
    courbeE_.axe_= 0;
    courbeI_.axe_= 0;
    courbe_.visible_= true;
    courbeE_.visible_= true;
    courbeI_.visible_= true;
    graphe_.ajoute(courbe_);
    graphe_.ajoute(courbeE_);
    graphe_.ajoute(courbeI_);
    graphe_.legende_= true;
    graphe_.visible_= true;
    setInteractif(true);
    setGraphe(graphe_);
    fullRepaint();
  }
  public void propertyChange(final PropertyChangeEvent _evt) {
    fullRepaint();
  }
  public Axe getAxeX() {
    return axeX_;
  }
  public void axeXEstVisible(final boolean _b) {
    axeX_.visible_= _b;
  }
  public void graduationsXEstVisible(final boolean _b) {
    axeX_.graduations_= _b;
  }
  public Axe getAxeY(final int numeroAxe) {
    return (Axe)axesY.elementAt(numeroAxe);
  }
  /*public Axe getAxeX(int numeroAxe)
  {
      return (Axe)axesY.elementAt(numeroAxe);
  }*/
}
