/*
 * @file         DiapreTable.java
 * @creation     2000-11-17
 * @modification $Date: 2005-08-16 13:27:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import javax.swing.JTable;
/**
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 13:27:38 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreTable extends JTable {
  /*public DiapreTable () {
    MyTableModel myModel = new MyTableModel();
    JTable table = new JTable(myModel);
    table.setPreferredScrollableViewportSize(new Dimension(300, 150));
    JScrollPane scrollPane = table.createScrollPaneForTable(table);
    setVisible(true);  
  }
  
  class MyTableModel extends AbstractTableModel {
    final String[] columnNames = {"N�", 
                                  "Abscisse",
                                  "Ordonn�e"};
                                  
    
    final Object[][] data = {
        {"point 1 ", new Double(0) , new Double(0)},
        {"point 2 ", new Double(0) , new Double(0)},
        {"point 3 ", new Double(0) , new Double(0)},
        {"point 4 ", new Double(0) , new Double(0)},
        {"point 5 ", new Double(0) , new Double(0)},
        {"point 6 ", new Double(0) , new Double(0)},
        {"point 7 ", new Double(0) , new Double(0)},
        {"point 8 ", new Double(0) , new Double(0)},
        {"point 9 ", new Double(0) , new Double(0)},
        {"point 10", new Double(0) , new Double(0)}
    };
    //double data[][]= new double[10][3];
    
    public int getColumnCount() {
      return columnNames.length;
    }
    
    public int getRowCount() {
      return data.length;
    }
  
    public String getColumnName(int col) {
      return columnNames[col];
    }
  
    public Object getValueAt(int row, int col) {
      return data[row][col];
    }
  
    
    public boolean isCellEditable(int row, int col) {
      return true;
    }
   
    public void setValueAt(Object value, int row, int col) {
      if (data[0][col] instanceof Double &&  !(value instanceof Double)) {
        try {
          data[row][col] = new Double(value.toString().equals("") ? 0 :  Double.parseDouble(value.toString()));
          //fireTableCellUpdated(row, col);
        } 
        catch (NumberFormatException e) {
        }
      } 
      else {
        data[row][col] = value;
        //fireTableCellUpdated(row, col);
      }
    }
  }*/
}
