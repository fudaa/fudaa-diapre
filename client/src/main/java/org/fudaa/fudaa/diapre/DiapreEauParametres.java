/*
 * @file         DiapreEauParametres.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.diapre.SEau;
/**
 * Fenetre de saisie des parametres de la nappe d'eau.
 * Ces parametres sont enregistrés dans la structure SEau.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreEauParametres extends BuPanel implements ActionListener {
  boolean blocus= true;
  double[] minOrdonnee;
  DiapreImplementation Diapre;
  BuCommonInterface appli_;
  protected DiapreEauStat stat; //fenetre pour l'action statique
  SEau parametresEau;
  // zone de saisie
  BuButton tbStat= new BuButton("Calcul hydrostatique");
  BuButton tbDyn= new BuButton("Calcul hydrodynamique");
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }
  public DiapreEauParametres(final BuCommonInterface _appli) {
    super();
    appli_= _appli;
    Diapre= ((DiapreImplementation)appli_.getImplementation());
    stat= new DiapreEauStat(_appli);
    // grille d'affchage
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    setLayout(lm);
    c.anchor= GridBagConstraints.SOUTH;
    c.ipadx= 50;
    c.ipady= 50;
    tbStat.addActionListener(new DiapreStatListener());
    tbDyn.setEnabled(false);
    c.gridx= 0;
    c.gridy= 0;
    c.gridwidth= 1;
    c.gridheight= 1;
    c.weightx= 100;
    c.weighty= 100;
    c.fill= GridBagConstraints.CENTER;
    placeComposant(lm, tbStat, c);
    c.anchor= GridBagConstraints.CENTER;
    c.ipadx= 10;
    c.ipady= 10;
    c.gridx= 0;
    c.gridy= 1;
    c.gridwidth= 1;
    c.gridheight= 1;
    c.weightx= 100;
    c.weighty= 100;
    c.fill= GridBagConstraints.CENTER;
    placeComposant(lm, new JLabel(""), c);
    c.anchor= GridBagConstraints.NORTH;
    c.ipadx= 50;
    c.ipady= 50;
    c.gridx= 0;
    c.gridy= 2;
    c.gridwidth= 1;
    c.gridheight= 1;
    c.weightx= 100;
    c.weighty= 100;
    c.fill= GridBagConstraints.CENTER;
    placeComposant(lm, tbDyn, c);
    setParametresEau(parametresEau);
  }
  public void actionPerformed(final ActionEvent e) {
    parametresEau= getParametresEau();
    setVisible(true);
  }
  public void setMinOrdonnee(final double[] min) {
    //System.out.println("min eau : "+min);
    minOrdonnee= min;
  }
  // Mutateur des parametres de l'eau
  public void setParametresEau(final SEau parametresEau_) {
    parametresEau= parametresEau_;
    if (parametresEau != null) {
      stat.setParametresEau(parametresEau);
    }
  }
  // Accesseur des parametres de l'eau
  public SEau getParametresEau() {
    if (parametresEau == null) {
      parametresEau= new SEau();
    }
    if (stat != null) {
      parametresEau= stat.getParametresEau();
    }
    return parametresEau;
  }
  class DiapreStatListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (blocus) {
        setParametresEau(parametresEau);
        blocus= false;
      }
      stat.setMinOrdonnee(minOrdonnee);
      stat.setVisible(true);
    }
  }
}
