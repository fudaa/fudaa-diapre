/*
 * @file         DiapreOutilsDonnees.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import org.fudaa.dodico.corba.diapre.*;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Un ensemble de m�thodes facilitant l'acc�s aux donn�es.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreOutilsDonnees {
  // Projet dans lequel il faut chercher les donn�es.
  FudaaProjet projet_;
  //SParametresDiapre params;
  // Constructeur
  public DiapreOutilsDonnees(final FudaaProjet _projet) {
    projet_= _projet;
  }
  // Mutateur de projet_
  public void setProjet(final FudaaProjet _projet) {
    projet_= _projet;
  }
  // Cr�� une structure de param�tres par d�faut et l'ajoute dans projet_ 
  public void creationDonnees() {
    // Structure regroupant toutes les donn�es
    final SParametresDiapre params= new SParametresDiapre();
    //commentaires
    params.commentairesDiapre= new SCommentairesDiapre();
    //talus
    params.talusDiapre= new STalusDiapre(0, new SPointDiapre[20]);
    //sol
    params.sol= new SSol("", 0, new SCoucheSol(), new SCoucheSol[10]);
    //eau
    params.eau= new SEau();
    //surcharges
    params.surchargesProjet=
      new SSurchargesProjet(
        0,
        0,
        0,
        new SSurchargeLineique[10],
        new SSurchargeUniforme(),
        new SSurchargeTrapezoidale[10]);
    //options
    params.options= new SOptions();
    //parametres generaux
    params.parametresGeneraux= new SParametresGeneraux();
    //commentaires
    params.commentairesDiapre= new SCommentairesDiapre();
    projet_.addParam(DiapreResource.PARAMETRES, params);
  }
  // Pour l'acc�s � la totalit� des param�tres.
  public SParametresDiapre getParametres() {
    return (SParametresDiapre)projet_.getParam(DiapreResource.PARAMETRES);
  }
  /*  public void setParametres(SParametresDiapre params_) {
      params=params_;
    }*/
  // Pour l'acc�s aux r�sultats.
  public SResultatsDiapre getResultats() {
    return (SResultatsDiapre)projet_.getResult(DiapreResource.RESULTATS);
  }
}
