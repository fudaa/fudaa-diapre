/*
 * @file         DiapreSurchartrap.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPicture;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.diapre.SSurchargeTrapezoidale;
/**
 * Fenetre d'affichage d'un tableau �ditable avec controles des abscisses saisies.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:13 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreSurchartrap extends JDialog implements ActionListener {
  // fenetre de dessin des surcharges
  //DiapreSurchartrapdess trapdess;
  //    protected DiapreImplementation info = new DiapreImplementation();
  // D�claration des variables
  final Object[][] data= { { "ST n�1", "", "", "", "", "", "" }, {
      "ST n�2", "", "", "", "", "", "" }, {
      "ST n�3", "", "", "", "", "", "" }, {
      "ST n�4", "", "", "", "", "", "" }, {
      "ST n�5", "", "", "", "", "", "" }, {
      "ST n�6", "", "", "", "", "", "" }, {
      "ST n�7", "", "", "", "", "", "" }, {
      "ST n�8", "", "", "", "", "", "" }, {
      "ST n�9", "", "", "", "", "", "" }, {
      "ST n�10", "", "", "", "", "", "" }
  };
  //SSurchargesProjet parametresProjet = null;
  SSurchargeTrapezoidale[] surchargesTrapezoidales;
  private BuDialogMessage message;
  BuCommonInterface appli_;
  public DiapreCellEditor anEditor1= new DiapreCellEditor();
  public DiapreCellEditor anEditor2= new DiapreCellEditor();
  public DiapreCellEditor anEditor3= new DiapreCellEditor();
  public DiapreCellEditor anEditor4= new DiapreCellEditor();
  public DiapreCellEditor anEditor5= new DiapreCellEditor();
  public DiapreCellEditor anEditor6= new DiapreCellEditor();
  private double minAbscisse;
  private double maxAbscisse;
  int nombreSurchargesTrapezoidales;
  private boolean erreur;
  boolean sup= true;
  // zones de saisie
  MyTableModel myModel= new MyTableModel();
  JTable table= new JTable(myModel);
  BuButton graph= new BuButton(" Graphique ");
  BuButton bvalider= new BuButton(" Valider ");
  BuButton bsupprimer= new BuButton(" Supprimer ");
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  // CONSTRUCTEUR /////////////////////////////////////////////////////////////
  public DiapreSurchartrap(final BuCommonInterface _appli) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Surcharge trap�zo�dale ",
      true);
    setSize(800, 400);
    setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    appli_= _appli;
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    //c.anchor      = GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    c.insets.left= 40;
    // premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    c.gridwidth= 2;
    final BuLabel comm= new BuLabel("Rentrer les abscisses dans l'ordre croissant. ");
    placeComposant(lm, comm, c);
    c.gridy= 1;
    c.gridwidth= 1;
    table.setPreferredScrollableViewportSize(new Dimension(500, 160));
    table.getColumn(table.getColumnName(1)).setCellEditor(anEditor1);
    table.getColumn(table.getColumnName(2)).setCellEditor(anEditor2);
    table.getColumn(table.getColumnName(3)).setCellEditor(anEditor3);
    table.getColumn(table.getColumnName(4)).setCellEditor(anEditor4);
    table.getColumn(table.getColumnName(5)).setCellEditor(anEditor5);
    table.getColumn(table.getColumnName(6)).setCellEditor(anEditor6);
    table.setRowSelectionInterval(0, 0);
    table.setColumnSelectionInterval(1, 1);
    final JScrollPane scrollPane= new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //On regle la taille des colonnes
     (table.getColumn(table.getColumnName(0))).setPreferredWidth(50);
    placeComposant(lm, scrollPane, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.WEST;
    graph.addActionListener(new DiapreSurchartrapdessListener());
    placeComposant(lm, graph, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.CENTER;
    //trapdess = new DiapreSurchartrapdess(_appli);
    bvalider.addActionListener(new DiapreValidListener());
    placeComposant(lm, bvalider, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, bsupprimer, c);
    bsupprimer.addActionListener(new DiapreSuppListener());
    // deuxieme colonne
    c.gridx= 1;
    c.gridy= 1;
    c.anchor= GridBagConstraints.CENTER;
    c.gridheight= 2;
    Image img;
    img= DiapreResource.DIAPRE.getImage("diapre_7.gif");
    placeComposant(lm, new BuPicture(img), c);
  }
  public void actionPerformed(final ActionEvent e) {
    surchargesTrapezoidales= getParametresSurchartrap();
    setVisible(true);
  }
  public void setMinAbscisse(final double xmin) {
    //System.out.println("xmin : "+xmin);
    minAbscisse= xmin;
  }
  public void setMaxAbscisse(final double xmax) {
    //System.out.println("xmax : "+xmax);
    maxAbscisse= xmax;
  }
  // Mutateur des parametres de surcharge lineique
  public void setParametresSurchartrap(final SSurchargeTrapezoidale[] surchargesTrapezoidales_) {
    surchargesTrapezoidales= surchargesTrapezoidales_;
    if (surchargesTrapezoidales != null) {
      for (int j= 0; j < nombreSurchargesTrapezoidales; j++) {
        table.setValueAt(
          new Double(surchargesTrapezoidales[j].abscisseDebut),
          j,
          1);
        table.setValueAt(
          new Double(surchargesTrapezoidales[j].abscisseFin),
          j,
          2);
        table.setValueAt(
          new Double(surchargesTrapezoidales[j].forceVerticaleDebut),
          j,
          3);
        table.setValueAt(
          new Double(surchargesTrapezoidales[j].forceVerticaleFin),
          j,
          4);
        table.setValueAt(
          new Double(surchargesTrapezoidales[j].forceHorizontaleDebut),
          j,
          5);
        table.setValueAt(
          new Double(surchargesTrapezoidales[j].forceHorizontaleFin),
          j,
          6);
      }
    }
  }
  // Accesseur des parametres de surcharge lineique
  public SSurchargeTrapezoidale[] getParametresSurchartrap() {
    anEditor1.stopCellEditing();
    anEditor2.stopCellEditing();
    anEditor3.stopCellEditing();
    anEditor4.stopCellEditing();
    anEditor5.stopCellEditing();
    anEditor6.stopCellEditing();
    if (surchargesTrapezoidales == null) {
      surchargesTrapezoidales= new SSurchargeTrapezoidale[10];
    }
    int n= 0;
    while (n < 10
      && !data[n][1].toString().equals("")
      && !data[n][2].toString().equals("")
      && !data[n][3].toString().equals("")
      && !data[n][4].toString().equals("")
      && !data[n][5].toString().equals("")
      && !data[n][6].toString().equals("")) {
      n= n + 1;
    }
    nombreSurchargesTrapezoidales= n;
    //System.out.println("nbre : "+n);
    while (n < 10
      && data[n][1].toString().equals("")
      && data[n][2].toString().equals("")
      && data[n][3].toString().equals("")
      && data[n][4].toString().equals("")
      && data[n][5].toString().equals("")
      && data[n][6].toString().equals("")) {
      n++;
    }
    if (n < 10 && sup) {
      erreur= true;
      if (n == nombreSurchargesTrapezoidales) {
        messageAssistant(
          "Vous devez compl�ter tous les champs"
            + "\nde la surcharge Trap�zoidale n� "
            + (n + 1));
      } else {
        if (n == nombreSurchargesTrapezoidales + 1) {
          messageAssistant(
            "Vous devez compl�ter la surcharge Trap�zo�dale n� "
              + (nombreSurchargesTrapezoidales + 1)
              + "\npour que la surcharge Trap�zo�dale n� "
              + (n + 1)
              + " soit"
              + "\nprise en compte");
        } else {
          messageAssistant(
            "Vous devez compl�ter les surcharges Trap�zo�dales n� "
              + (nombreSurchargesTrapezoidales + 1)
              + "\n� "
              + n
              + " pour que la surcharge Trap�zo�dale n� "
              + (n + 1)
              + " soit"
              + "\nprise en compte");
        }
      }
    } else {
      erreur= false;
    }
    if (!erreur) {
      for (int i= 0; i < nombreSurchargesTrapezoidales; i++) {
        surchargesTrapezoidales[i]= new SSurchargeTrapezoidale();
      }
      // Remplissage des structures:
      for (int j= 0; j < nombreSurchargesTrapezoidales; j++) {
        surchargesTrapezoidales[j].abscisseDebut=
          new Double(data[j][1].toString()).doubleValue();
        surchargesTrapezoidales[j].abscisseFin=
          new Double(data[j][2].toString()).doubleValue();
        surchargesTrapezoidales[j].forceVerticaleDebut=
          new Double(data[j][3].toString()).doubleValue();
        surchargesTrapezoidales[j].forceVerticaleFin=
          new Double(data[j][4].toString()).doubleValue();
        surchargesTrapezoidales[j].forceHorizontaleDebut=
          new Double(data[j][5].toString()).doubleValue();
        surchargesTrapezoidales[j].forceHorizontaleFin=
          new Double(data[j][6].toString()).doubleValue();
        System.out.println(
          "Surcharge "
            + (j + 1)
            + " : abscisse d�but: "
            + surchargesTrapezoidales[j].abscisseDebut);
        System.out.println(
          "Surcharge "
            + (j + 1)
            + " : abscisse fin  : "
            + surchargesTrapezoidales[j].abscisseFin);
        System.out.println(
          "Surcharge "
            + (j + 1)
            + " : FV       d�but: "
            + surchargesTrapezoidales[j].forceVerticaleDebut);
        System.out.println(
          "Surcharge "
            + (j + 1)
            + " : FV       fin  : "
            + surchargesTrapezoidales[j].forceVerticaleFin);
        System.out.println(
          "Surcharge "
            + (j + 1)
            + " : FH       d�but: "
            + surchargesTrapezoidales[j].forceHorizontaleDebut);
        System.out.println(
          "Surcharge "
            + (j + 1)
            + " : FH       fin  : "
            + surchargesTrapezoidales[j].forceHorizontaleFin);
      }
    }
    return surchargesTrapezoidales;
  }
  public int getNbreSurchargesTrapezoidales() {
    return nombreSurchargesTrapezoidales;
  }
  public void setNbreSurchargesTrapezoidales(final int nbre) {
    nombreSurchargesTrapezoidales= nbre;
    //if(nbre==0) bsupprimer.setEnabled(false);
  }
  /*--- message de l'assistant ---*/
  private void messageAssistant(final String s) {
    message= new BuDialogMessage(appli_, appli_.getInformationsSoftware(), s);
    message.setSize(500, 150);
    message.setTitle(" ERREUR ");
    message.setResizable(false);
    message.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    final Point pos= this.getLocationOnScreen();
    pos.x= pos.x + this.getWidth() / 2 - message.getWidth() / 2;
    pos.y= pos.y + this.getHeight() / 2 - message.getHeight() / 2;
    message.setLocation(pos);
    message.setVisible(true);
  }
  boolean testErreur() {
    sup= true;
    getParametresSurchartrap();
    final int[] bool= new int[10];
    final int[] bool0= new int[10];
    final int[] boo= new int[10];
    final int[] boo1= new int[10];
    final int[] bool1= new int[10];
    final int[] bool2= new int[10];
    for (int i= 0; i < 10; i++) {
      bool[i]= 1; // intervalle abscisse d�but
      bool0[i]= 1; // intervalle abscisse fin
      boo[i]= 1; // croissance des abscisses
      boo1[i]= 1; // croissance des abscisses d'une ligne sur l'autre
      bool1[i]= 1; // superposition
      bool2[i]= 1; // �galit�
    }
    // Intervalle [xmin , xmax]
    for (int l= 0; l < nombreSurchargesTrapezoidales; l++) {
      final int o= l + 1;
      if (((Double.parseDouble(data[l][1].toString()) >= minAbscisse)
        || (Double.parseDouble(data[l][1].toString()) <= maxAbscisse))) {
        bool[l]= 1;
      }
      if (((Double.parseDouble(data[l][2].toString()) >= minAbscisse)
        || (Double.parseDouble(data[l][2].toString()) <= maxAbscisse))) {
        bool0[l]= 1;
      }
      if (((Double.parseDouble(data[l][1].toString()) < minAbscisse)
        || (Double.parseDouble(data[l][1].toString()) > maxAbscisse))) {
        bool[l]= 0;
        messageAssistant(
          "Erreur � la surcharge "
            + o
            + " :\n"
            + data[l][1]
            + " : Cette abscisse n'appartient pas � ["
            + minAbscisse
            + " , "
            + maxAbscisse
            + "]");
      }
      if (((Double.parseDouble(data[l][2].toString()) < minAbscisse)
        || (Double.parseDouble(data[l][2].toString()) > maxAbscisse))) {
        bool0[l]= 0;
        messageAssistant(
          "Erreur � la surcharge "
            + o
            + " :\n"
            + data[l][2]
            + " : Cette abscisse n'appartient pas � ["
            + minAbscisse
            + " , "
            + maxAbscisse
            + "]");
      }
      // Croissance des abscisses sur une ligne
      if (Double.parseDouble(data[l][1].toString())
        > Double.parseDouble(data[l][2].toString())) {
        messageAssistant(
          "Surcharge " + o + " : Les abscisses doivent �tre croissantes!");
        boo[l]= 0;
      }
      if (Double.parseDouble(data[l][1].toString())
        < Double.parseDouble(data[l][2].toString())) {
        boo[l]= 1;
      }
      // d'une ligne sur l'autre
      if ((nombreSurchargesTrapezoidales > 1) && (l > 0)) {
        if (Double.parseDouble(data[l - 1][1].toString())
          > Double.parseDouble(data[l][1].toString())
          || //Double.parseDouble(data[ l-1 ][2].toString())> Double.parseDouble(data[l][2].toString())||
        //Double.parseDouble(data[ l-1 ][1].toString())> Double.parseDouble(data[l][2].toString())||
        Double
          .parseDouble(
          data[l - 1][2].toString())
            > Double.parseDouble(data[l][1].toString())) {
          messageAssistant(
            "Surcharges "
              + l
              + " et "
              + o
              + " : Les abscisses doivent �tre croissantes\n d'une surcharge sur l'autre. ");
          boo1[l]= 0;
          //System.out.println(l+" : croiss 1/autre "+boo1[l]);
        }
        if (Double.parseDouble(data[l - 1][1].toString())
          < Double.parseDouble(data[l][1].toString())
          || //Double.parseDouble(data[ l-1 ][2].toString())< Double.parseDouble(data[l][2].toString())||
        //Double.parseDouble(data[ l-1 ][1].toString())< Double.parseDouble(data[l][2].toString())||
        Double
          .parseDouble(
          data[l - 1][2].toString())
            < Double.parseDouble(data[l][1].toString())) {
          //boo1[l] = 1;
          //System.out.println(l+" : croiss 1/autre "+boo1[l]);
        }
        // Superposition des surcharges
        if ((Double.parseDouble(data[l][1].toString())
          > Double.parseDouble(data[l - 1][1].toString())
          && Double.parseDouble(data[l][1].toString())
            < Double.parseDouble(data[l - 1][2].toString()))
          || (Double.parseDouble(data[l][2].toString())
            > Double.parseDouble(data[l - 1][1].toString())
            && Double.parseDouble(data[l][2].toString())
              < Double.parseDouble(data[l - 1][2].toString()))) {
          messageAssistant(
            "Superposition des surcharges " + l + " et " + o + CtuluLibString.DOT);
          bool1[l]= 0;
        }
        if ((Double.parseDouble(data[l][1].toString())
          < Double.parseDouble(data[l - 1][1].toString())
          && Double.parseDouble(data[l][1].toString())
            > Double.parseDouble(data[l - 1][2].toString()))
          || (Double.parseDouble(data[l][2].toString())
            < Double.parseDouble(data[l - 1][1].toString())
            && Double.parseDouble(data[l][2].toString())
              > Double.parseDouble(data[l - 1][2].toString()))) {
          bool1[l]= 1;
        }
      }
      // Egalit� de deux abscisses ( = surcharge lin�ique)
      if (Double.parseDouble(data[l][1].toString())
        == Double.parseDouble(data[l][2].toString())) {
        bool2[l]= 0;
        messageAssistant(
          "Les abscisses de la surcharge " + o + " sont �gales.");
      }
      if (Double.parseDouble(data[l][1].toString())
        != Double.parseDouble(data[l][2].toString())) {
        bool2[l]= 1;
      }
    } //fin du for...
    if ((bool[0] == 1)
      && (bool[1] == 1)
      && bool[2] == 1
      && bool[3] == 1
      && bool[4] == 1
      && bool[5] == 1
      && bool[6] == 1
      && bool[7] == 1
      && bool[8] == 1
      && bool[9] == 1
      && (bool0[0] == 1)
      && (bool0[1] == 1)
      && bool0[2] == 1
      && bool0[3] == 1
      && bool0[4] == 1
      && bool0[5] == 1
      && bool0[6] == 1
      && bool0[7] == 1
      && bool0[8] == 1
      && bool0[9] == 1
      && (bool1[0] == 1)
      && (bool1[1] == 1)
      && bool1[2] == 1
      && bool1[3] == 1
      && bool1[4] == 1
      && bool1[5] == 1
      && bool1[6] == 1
      && bool1[7] == 1
      && bool1[8] == 1
      && bool1[9] == 1
      && (bool2[0] == 1)
      && (bool2[1] == 1)
      && bool2[2] == 1
      && bool2[3] == 1
      && bool2[4] == 1
      && bool2[5] == 1
      && bool2[6] == 1
      && bool2[7] == 1
      && bool2[8] == 1
      && bool2[9] == 1
      && (boo[0] == 1)
      && (boo[1] == 1)
      && boo[2] == 1
      && boo[3] == 1
      && boo[4] == 1
      && boo[5] == 1
      && boo[6] == 1
      && boo[7] == 1
      && boo[8] == 1
      && boo[9] == 1
      && (boo1[0] == 1)
      && (boo1[1] == 1)
      && boo1[2] == 1
      && boo1[3] == 1
      && boo1[4] == 1
      && boo1[5] == 1
      && boo1[6] == 1
      && boo1[7] == 1
      && boo1[8] == 1
      && boo1[9] == 1
      && !erreur) {
      getParametresSurchartrap();
      if (!erreur) {
        if (getNbreSurchargesTrapezoidales() == 0) {
          messageAssistant(
            "Aucune surcharge trap�zoidale" + " n'a �t� d�finie");
          return false;
        }
        return true;
      }
      return false;
    }
    return false;
  }
  class DiapreSurchartrapdessListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      /*getParametresSurchartrap();
       if(!erreur){
          if(getNbreSurchargesTrapezoidales() == 0)
             messageAssistant("Aucune surcharge trap�zoidale"+
                        " n'a �t� d�finie");*/
      //if(testErreur())
      //new DiapreConstruitGrapheRappel(appli_,3);
    }
  }
  // R�alisation des controles sur les abscisses au moment de valider
  class DiapreValidListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (testErreur()) {
        dispose();
      }
    }
  }
  class DiapreSuppListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      sup= false;
      getNbreSurchargesTrapezoidales();
      getParametresSurchartrap();
      if (getNbreSurchargesTrapezoidales() > 0) {
        getParametresSurchartrap();
        final BuDialogConfirmation mess=
          new BuDialogConfirmation(
            appli_,
            appli_.getInformationsSoftware(),
            "Voulez vous supprimer toutes les surcharges \ntrap�zo�dales?");
        mess.setTitle("ATTENTION");
        if (mess.activate() == JOptionPane.YES_OPTION) {
          for (int row= 0; row < nombreSurchargesTrapezoidales; row++) {
            for (int col= 1; col < 7; col++) {
              table.setValueAt("", row, col);
            }
          }
          nombreSurchargesTrapezoidales= 0;
          getParametresSurchartrap();
          dispose();
        }
      } else {
        for (int row= 0; row < 10; row++) {
          for (int col= 1; col < 7; col++) {
            table.setValueAt("", row, col);
          }
        }
        nombreSurchargesTrapezoidales= 0;
        getParametresSurchartrap();
        dispose();
      }
    }
  }
  class MyTableModel extends AbstractTableModel {
    final String[] columnNames=
      { "N� ", "x1 (m)", "x2 (m)", "V 1", "V 2", "H 1", "H 2", };
    public int getColumnCount() {
      return columnNames.length;
    }
    public int getRowCount() {
      return data.length;
    }
    public String getColumnName(final int col) {
      return columnNames[col];
    }
    public Object getValueAt(final int row, final int col) {
      return data[row][col];
    }
    /*public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }*/
    public boolean isCellEditable(final int row, final int col) {
      if (col < 1) {
        return false;
      }
      return true;
    }
    public boolean isNombre(final String chaine) {
      final int taille= chaine.length();
      if (taille != 0) {
        int i= 0;
        boolean bool= true;
        while (i < taille
          && (chaine.charAt(i) >= '0'
            && chaine.charAt(i) <= '9'
            || chaine.charAt(i) == '-'
            || chaine.charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool= false;
          }
          i++;
        }
        if (i == taille) {
          return true;
        }
        return false;
      }
      return false;
    }
    public void setValueAt(final Object value, final int row, final int col) {
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          data[row][col]= "0" + value.toString();
        } else {
          data[row][col]= value;
        //System.out.println("Vous avez rentr� "+value+" � la case ("+row+","+col+").");
        }
      } else {
        data[row][col]= "";
      }
    }
  }
}
