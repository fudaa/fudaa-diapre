/*
 * @file         DiapreInterfaceHorizontale.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPicture;

import org.fudaa.dodico.corba.diapre.SCoucheSol;
import org.fudaa.dodico.corba.diapre.SParametresGeneraux;
import org.fudaa.dodico.corba.diapre.SSol;
/**
 * Boite de dialogue permettant de saisir les param�tres generaux sur le mode de calcul.
 * Les donn�es sont automatiquement enregistr�es dans le champ parametres Generaux avant la fermeture de la boite.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreInterfaceHorizontale
  extends JDialog
  implements ActionListener {
  // Donn�e trait�e dans la boite de dialogue : vaut null par d�faut.
  SParametresGeneraux parametresGeneraux;
  SSol sol;
  protected DiapreCalculCourtTerme[] calCourtTerme;
  protected DiapreCalculLongTerme[] calLongTerme;
  //protected DiapreImplementation info = new DiapreImplementation();
  protected boolean longTerme= true;
  protected boolean courtTerme= true;
  protected DiapreImplementation diapre_;
  protected DiapreSolsParametres interHori;
  private final double[] ymin= new double[2];
  private final String messLongTerme=
    new String(
      "Attention vous �tes en calcul court terme."
        + " \nVoulez vous vraiment changer de mode de calcul");
  private final String messCourtTerme=
    new String(
      "Attention vous �tes en calcul long terme."
        + " \nVoulez vous vraiment changer de mode de calcul");
  // Zones de saisie contenues dans la boite de dialogue
  CheckboxGroup choixPousseeButee= new CheckboxGroup();
  Checkbox poussee=
    new Checkbox("Calcul en pouss�e  ", choixPousseeButee, true);
  Checkbox butee= new Checkbox("Calcul en but�e    ", choixPousseeButee, false);
  TextFieldsDiapre deltaSurPhi1= new TextFieldsDiapre(-1, 1);
  TextFieldsDiapre deltaSurPhi2= new TextFieldsDiapre(-1, 1);
  BuButton calculLongTerme= new BuButton("Calcul � long terme ");
  BuButton calculCourtTerme= new BuButton("Calcul � court terme ");
  // Utilise un GridBagLayout pour placer un composant avec des contraintes sp�cifiques
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  // Constructeur
  //contructeur par defaut
  /*public DiapreInterfaceHorizontale() {
  }*/
  //Construit une boite de dialogue pour saisir les parametres generaux pour le mode de calcul.
  public DiapreInterfaceHorizontale(final BuCommonInterface _appli) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Interface horizontale ",
      true);
    diapre_= ((DiapreImplementation)_appli.getImplementation());
    interHori= new DiapreSolsParametres(diapre_);
    // Caract�ristiques de la boite de dialogue
    setSize(660, 350);
    setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    //initialisation des tableau de fenetre
    calLongTerme= new DiapreCalculLongTerme[20];
    calCourtTerme= new DiapreCalculCourtTerme[20];
    for (int i= 0; i < 20; i++) {
      calLongTerme[i]= null;
      calCourtTerme[i]= null;
    }
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    c.gridy= GridBagConstraints.RELATIVE;
    c.ipadx= 5;
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //20;  //proportion des colonnes
    //c.insets.left=30;
    //premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, poussee, c);
    c.gridy= 1;
    placeComposant(lm, butee, c);
    c.gridy= 2;
    c.gridwidth= 3;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(
      lm,
      new BuLabel("____________________________________________________________________________________________"),
      c);
    c.gridy= 3;
    c.gridwidth= 1;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, calculLongTerme, c);
    //deuxieme colonne
    c.gridx= 1;
    c.gridy= 0; //c.anchor = GridBagConstraints.EAST; c.weightx = 40;
    Image deltaPhi;
    deltaPhi= DiapreResource.DIAPRE.getImage("diapre_8.gif");
    placeComposant(lm, new BuPicture(deltaPhi), c);
    c.gridy= 1;
    placeComposant(lm, new BuPicture(deltaPhi), c);
    c.gridy= 3;
    c.gridwidth= 2;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(lm, calculCourtTerme, c);
    //troisieme colonne
    c.gridx= 2; //c.weightx    = 40;
    c.gridy= 0;
    c.gridwidth= 1;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, deltaSurPhi1, c);
    c.gridy= 1;
    placeComposant(lm, deltaSurPhi2, c);
    deltaSurPhi2.setValue(0);
    deltaSurPhi2.setEnabled(false);
    deltaSurPhi2.setEditable(false);
    deltaSurPhi1.addFocusListener(new DeltaSurPhiAction());
    deltaSurPhi2.addFocusListener(new DeltaSurPhi2Action());
    calculLongTerme.setEnabled(false);
    calculCourtTerme.setEnabled(false);
    poussee.addItemListener(new PousseeAction());
    butee.addItemListener(new ButeeAction());
    calculCourtTerme.addActionListener(this);
    calculCourtTerme.setActionCommand("COURT_TERME");
    calculLongTerme.addActionListener(this);
    calculLongTerme.setActionCommand("LONG_TERME");
    setParametresGeneraux(parametresGeneraux);
    setParametresSols(sol);
  }
  /** Enregistrement des donn�es de la boite de dialogue avant sa fermeture. */
  public void actionPerformed(final ActionEvent e) {
    System.out.println(e);
    final String commande= e.getActionCommand();
    if ("COURT_TERME".equals(commande)) {
      cmd_CalculCourtTerme();
    } else if ("LONG_TERME".equals(commande)) {
      cmd_CalculLongTerme();
    } else {
      parametresGeneraux= getParametresGeneraux();
      sol= getParametresSols();
      setVisible(false);
    }
  }
  public void setMinOrdonnee(final double[] min) {
    ymin[0]= min[0];
    ymin[1]= min[1];
  }
  public double[] getMinOrdonnee() {
    return ymin;
  }
  /*public DiapreCalculLongTerme[] getCalculLongTerme(){
    return calLongTerme;
  }

  public DiapreCalculCourtTerme[] getCalculCourtTerme(){
    return calCourtTerme;
  }*/
  // permet d'initialiser les parametres
  public void setParametresSols(final SSol sol_) {
    sol= sol_;
    if (sol != null) {
      if (longTerme) {
        for (int i= 0; i < sol.nombreCouches; i++) {
          calLongTerme[i]= new DiapreCalculLongTerme(this, i);
          calLongTerme[i].setMinOrdonnee(ymin);
          calLongTerme[i].setParametresSols(sol);
          if (i == 0) {
            calLongTerme[i].setParametresSol(sol.premiereCouche);
          } else {
            calLongTerme[i].setParametresSol(sol.autresCouches[i - 1]);
          }
        }
      } else {
        for (int i= 0; i < sol.nombreCouches; i++) {
          calCourtTerme[i]= new DiapreCalculCourtTerme(this, i);
          calCourtTerme[i].setMinOrdonnee(ymin);
          calCourtTerme[i].setParametresSols(sol);
          if (i == 0) {
            calCourtTerme[i].setParametresSol(sol.premiereCouche);
          } else {
            calCourtTerme[i].setParametresSol(sol.autresCouches[i - 1]);
          }
        }
      }
    }
  }
  public SSol getParametresSols() {
    int i= 0;
    if (sol == null) {
      sol= new SSol();
    }
    sol.interfaceCoucheSol= "H";
    if (longTerme) {
      while (i < 20
        && calLongTerme != null
        && calLongTerme[i] != null
        && calLongTerme[i].valider.isFocusable()) {
        i++;
      }
      sol.nombreCouches= i;
      System.out.println("nombreCouche long terme : " + sol.nombreCouches);
      if (sol.nombreCouches > 0) {
        sol.premiereCouche= calLongTerme[0].getParametresSol();
        sol.autresCouches= new SCoucheSol[sol.nombreCouches - 1];
        for (int j= 0; j < sol.nombreCouches - 1; j++) {
          sol.autresCouches[j]= new SCoucheSol();
          sol.autresCouches[j]= calLongTerme[j + 1].getParametresSol();
        }
      }
    } else {
      while (i < 20
        && calCourtTerme != null
        && calCourtTerme[i] != null
        && calCourtTerme[i].valider.isFocusable()) {
        i++;
      }
      sol.nombreCouches= i;
      System.out.println("nombreCouche court terme : " + sol.nombreCouches);
      if (sol.nombreCouches > 0) {
        sol.premiereCouche= calCourtTerme[0].getParametresSol();
        sol.autresCouches= new SCoucheSol[sol.nombreCouches - 1];
        for (int j= 0; j < sol.nombreCouches - 1; j++) {
          sol.autresCouches[j]= new SCoucheSol();
          sol.autresCouches[j]= calCourtTerme[j + 1].getParametresSol();
        }
      }
    }
    return sol;
  }
  public void setParametresGeneraux(final SParametresGeneraux parametres) {
    parametresGeneraux= parametres;
    if (parametresGeneraux != null) {
      if (parametres.choixButeePoussee.equals("B")) {
        butee.setState(true);
        deltaSurPhi2.setValue(parametres.deltaSurPhi);
      }
      if (parametres.choixButeePoussee.equals("P")) {
        poussee.setState(true);
        deltaSurPhi1.setValue(parametres.deltaSurPhi);
      }
      if (parametres.choixCalcul.equals("L")) {
        courtTerme= false;
      }
      if (parametres.choixCalcul.equals("C")) {
        longTerme= false;
      }
      calculCourtTerme.setEnabled(true);
      calculLongTerme.setEnabled(true);
    }
  }
  public SParametresGeneraux getParametresGeneraux() {
    if (parametresGeneraux == null) {
      parametresGeneraux= new SParametresGeneraux();
    }
    if (butee.getState()) {
      parametresGeneraux.choixButeePoussee= "B";
    }
    if (poussee.getState()) {
      parametresGeneraux.choixButeePoussee= "P";
    }
    System.out.println(
      "Choix Butee Poussee : " + parametresGeneraux.choixButeePoussee);
    if (butee.getState()) {
      parametresGeneraux.deltaSurPhi= deltaSurPhi2.getValue();
      System.out.println("delta sur phi : " + parametresGeneraux.deltaSurPhi);
    }
    if (poussee.getState() && !deltaSurPhi1.getTextDiapre().equals("")) {
      parametresGeneraux.deltaSurPhi= deltaSurPhi1.getValue();
      System.out.println("delta sur phi : " + parametresGeneraux.deltaSurPhi);
    }
    if (courtTerme) {
      parametresGeneraux.choixCalcul= "C";
    } else {
      parametresGeneraux.choixCalcul= "L";
    }
    System.out.println(
      "choix long court terme : " + parametresGeneraux.choixCalcul);
    return parametresGeneraux;
  }
  //affiche une boite de dialogue pour le choix du calcul
  public void cmd_CalculLongTerme() {
    if (butee.getState()
      || deltaSurPhi1.getTextDiapre() != null
      && !deltaSurPhi1.getTextDiapre().equals("")) {
      if (deltaSurPhi2.getTextDiapre() != null
        && !deltaSurPhi2.getTextDiapre().equals("")) {
        DiapreSolsParametres.interHorizontale.setVisible(false);
        if (!longTerme) {
          final BuDialogConfirmation mess=
            new BuDialogConfirmation(
              diapre_,
              diapre_.getInformationsSoftware(),
              messLongTerme);
          mess.setTitle("ATTENTION");
          if (mess.activate() == JOptionPane.YES_OPTION) {
            longTerme= true;
          }
        }
        if (longTerme) {
          courtTerme= false;
          setParametresSols(sol);
          calLongTerme[0]= new DiapreCalculLongTerme(this, 0);
          calLongTerme[0].setMinOrdonnee(ymin);
          calLongTerme[0].setParametresSols(sol);
          if (sol != null && sol.premiereCouche != null) {
            calLongTerme[0].setParametresSol(sol.premiereCouche);
          }
          calLongTerme[0].setVisible(true);
          getParametresGeneraux();
          getParametresSols();
        }
      } else {
        new BuDialogMessage(
          diapre_,
          diapre_.getInformationsSoftware(),
          "Delta/Phi doit �tre compris entre" + "\n -1 et +1")
          .activate();
        calculLongTerme.setEnabled(false);
        calculCourtTerme.setEnabled(false);
      }
    } else {
      new BuDialogMessage(
        diapre_,
        diapre_.getInformationsSoftware(),
        "Delta/Phi doit �tre compris entre" + "\n -1 et +1")
        .activate();
      calculLongTerme.setEnabled(false);
      calculCourtTerme.setEnabled(false);
    }
  }
  public void cmd_CalculCourtTerme() {
    if (butee.getState()
      || deltaSurPhi1.getTextDiapre() != null
      && !deltaSurPhi1.getTextDiapre().equals("")) {
      if (deltaSurPhi2.getTextDiapre() != null
        && !deltaSurPhi2.getTextDiapre().equals("")) {
        DiapreSolsParametres.interHorizontale.setVisible(false);
        if (!courtTerme) {
          final BuDialogConfirmation mess=
            new BuDialogConfirmation(
              diapre_,
              diapre_.getInformationsSoftware(),
              messCourtTerme);
          if (mess.activate() == JOptionPane.YES_OPTION) {
            courtTerme= true;
          }
        }
        if (courtTerme) {
          longTerme= false;
          setParametresSols(sol);
          calCourtTerme[0]= new DiapreCalculCourtTerme(this, 0);
          calCourtTerme[0].setMinOrdonnee(ymin);
          calCourtTerme[0].setParametresSols(sol);
          if (sol != null && sol.nombreCouches >= 1) {
            calCourtTerme[0].setParametresSol(sol.premiereCouche);
          }
          calCourtTerme[0].setVisible(true);
          getParametresGeneraux();
          getParametresSols();
        }
      } else {
        new BuDialogMessage(
          diapre_,
          diapre_.getInformationsSoftware(),
          "Delta/Phi doit �tre compris entre" + "\n +1 et -1")
          .activate();
        calculLongTerme.setEnabled(false);
        calculCourtTerme.setEnabled(false);
      }
    } else {
      new BuDialogMessage(
        diapre_,
        diapre_.getInformationsSoftware(),
        "Delta/Phi doit �tre compris entre" + "\n +1 et -1")
        .activate();
      calculLongTerme.setEnabled(false);
      calculCourtTerme.setEnabled(false);
    }
  }
  class PousseeAction implements ItemListener {
    public void itemStateChanged(final ItemEvent e) {
      deltaSurPhi1.setEnabled(true);
      if (deltaSurPhi1.getTextDiapre() != null
        && !deltaSurPhi1.getTextDiapre().equals("")) {
        calculLongTerme.setEnabled(true);
        calculCourtTerme.setEnabled(true);
      } else {
        calculLongTerme.setEnabled(false);
        calculCourtTerme.setEnabled(false);
      }
    }
  }
  class ButeeAction implements ItemListener {
    public void itemStateChanged(final ItemEvent e) {
      deltaSurPhi1.setEnabled(false);
      calculLongTerme.setEnabled(true);
      calculCourtTerme.setEnabled(true);
    }
  }
  class DeltaSurPhiAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      calculLongTerme.setEnabled(true);
      calculCourtTerme.setEnabled(true);
    }
    public void focusLost(final FocusEvent e) {}
  }
  class DeltaSurPhi2Action implements FocusListener {
    public void focusGained(final FocusEvent e) {}
    public void focusLost(final FocusEvent e) {}
  }
}
