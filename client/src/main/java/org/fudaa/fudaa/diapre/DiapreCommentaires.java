/*
 * @file         DiapreCommentaires.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.diapre.SCommentairesDiapre;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreCommentaires extends BuPanel implements ActionListener {
  SCommentairesDiapre parametresCommentaires;
  // zones de saisie
  BuTextField titre1= new BuTextField();
  BuTextField titre2= new BuTextField();
  BuTextField titre3= new BuTextField();
  BuTextField titre4= new BuTextField();
  BuTextField titre5= new BuTextField();
  //BuButton enregistrer = new BuButton("Enregistrer");
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }
  public DiapreCommentaires() {
    super();
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    c.ipadx= 5;
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    //c.weighty    = 100; //proportion des lignes
    //c.weightx    = 100; //proportion des colonnes
    titre1.setColumns(30);
    titre2.setColumns(30);
    titre3.setColumns(30);
    titre4.setColumns(30);
    titre5.setColumns(30);
    c.gridy= 0;
    placeComposant(lm, new BuLabel("Entrez votre texte : "), c);
    c.gridy= 1;
    placeComposant(lm, titre1, c);
    c.gridy= 2;
    placeComposant(lm, titre2, c);
    c.gridy= 3;
    placeComposant(lm, titre3, c);
    c.gridy= 4;
    placeComposant(lm, titre4, c);
    c.gridy= 5;
    placeComposant(lm, titre5, c);
    //c.gridy = 6;
    //placeComposant(lm, enregistrer, c);
    //enregistrer.addActionListener(new DiapreEnregistrerListener());
  }
  // Mutateur des parametres de Options
  public void actionPerformed(final ActionEvent e) {
    parametresCommentaires= getParametresCommentaires();
    setVisible(true);
  }
  public void setParametresCommentaires(final SCommentairesDiapre parametresCommentaires_) {
    parametresCommentaires= parametresCommentaires_;
    if (parametresCommentaires != null) {
      //parametresCommentaires = new SCommentairesDiapre();
      titre1.setText(parametresCommentaires.titre1);
      titre2.setText(parametresCommentaires.titre2);
      titre3.setText(parametresCommentaires.titre3);
      titre4.setText(parametresCommentaires.titre4);
      titre5.setText(parametresCommentaires.titre5);
    }
  }
  // Accesseur des parametres de Options
  public SCommentairesDiapre getParametresCommentaires() {
    if (parametresCommentaires == null) {
      parametresCommentaires= new SCommentairesDiapre();
    }
    parametresCommentaires.titre1= titre1.getText();
    System.out.println("Titre 1 :" + parametresCommentaires.titre1);
    parametresCommentaires.titre2= titre2.getText();
    System.out.println("Titre 2 :" + parametresCommentaires.titre2);
    parametresCommentaires.titre3= titre3.getText();
    System.out.println("Titre 3 :" + parametresCommentaires.titre3);
    parametresCommentaires.titre4= titre4.getText();
    System.out.println("Titre 4 :" + parametresCommentaires.titre4);
    parametresCommentaires.titre5= titre5.getText();
    System.out.println("Titre 5 :" + parametresCommentaires.titre5);
    return parametresCommentaires;
  }
  // Enregistrement
  //class DiapreEnregistrerListener implements ActionListener {
  //public void actionPerformed( ActionEvent e)
  //{
  //System.out.println("Enregistrement");
  //getParametresCommentaires();
  //}
  //}
}
