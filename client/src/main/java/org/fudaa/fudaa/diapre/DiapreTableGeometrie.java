/*
 * @file         DiapreTableGeometrie.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreTableGeometrie implements TableModel {
  double[][] tab= new double[10][10];
  public DiapreTableGeometrie() {}
  public int getColumnCount() {
    return 3;
  }
  public int getRowCount() {
    return 10;
  }
  public boolean isCellEditable(final int row, final int col) {
    return true;
  }
  public Object getValueAt(final int row, final int col) {
    double ret= 0.;
    switch (col) {
      case 0 :
        switch (row) {
          case 0 :
            ret= 1;
            break;
          case 1 :
            ret= 2;
            break;
          case 2 :
            ret= 3;
            break;
          case 3 :
            ret= 4;
            break;
          case 4 :
            ret= 5;
            break;
          case 5 :
            ret= 6;
            break;
          case 6 :
            ret= 7;
            break;
          case 7 :
            ret= 8;
            break;
          case 8 :
            ret= 9;
            break;
          case 9 :
            ret= 10;
            break;
        }
        break;
      case 1 :
        ret= tab[row][0];
        break;
      case 2 :
        ret= tab[row][1];
        break;
    }
    return new Double(ret);
  }
  public String getColumnName(final int col) {
    String nomCol= null;
    System.out.println(col);
    switch (col) {
      case 0 :
        nomCol= "Num�ro";
        break;
      case 1 :
        nomCol= "abscisse";
        break;
      case 2 :
        nomCol= "ordonn�e";
        break;
    }
    return nomCol;
  }
  public void setValueAt(final Object obj, final int row, final int col) {
    System.out.println("setValueAt1");
    try {
      final double temp= Double.parseDouble((String)obj);
      //2//Integer temp = new Integer(Integer.parseInt((String) obj));
      //int i = temp.intValue();
      //3//int temp = Integer.parseInt((String) obj);
      System.out.println("setValueAt2"); //jamais imprim� ! !! !!!
      switch (col) {
        case 1 :
          tab[row][0]= temp;
          System.out.println("setValueAt3");
          break;
        case 2 :
          tab[row][1]= temp;
          break;
      }
      //fireTableCellUpdated(0,10);
    } catch (final Exception e) {}
  }
  public Class getColumnClass(final int col) {
    return Double.class;
  }
  public void addTableModelListener(final TableModelListener _l) {}
  public void removeTableModelListener(final TableModelListener _l) {}
}
