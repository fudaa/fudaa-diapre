/*
 * @file         DiapreValidationParametres.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.diapre.SParametresDiapre;
/**
 * Description de l'onglet des parametres des categories.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreValidationParametres extends BuPanel//implements ActionListener,FocusListener
{
  //protected   DiapreImplementation        Diapre;  
  BuCommonInterface appli_;
  DiapreFilleParametres fp_;
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }
  public DiapreValidationParametres(
    final BuCommonInterface _appli,
    final DiapreFilleParametres _fp) {
    super();
    appli_= _appli;
    fp_= _fp;
    //Diapre   = ((DiapreImplementation)appli_.getImplementation());
    // grille d'affchage
    /*GridBagLayout lm = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    this.setLayout  (lm);
    this.setBorder  (new EmptyBorder(5, 5, 5, 5));
    
    c.gridy       = GridBagConstraints.RELATIVE;
    c.anchor      = GridBagConstraints.WEST;
    */
    String message= "   Si vous avez entr� toutes les donn�es du probl�me";
    message += "vous pouvez cliquer sur\n";
    message += "VALIDER pour quitter l'entr�e des donn�es.";
    message
      += "   Puis, si vous souhaitez lancer les calculs, allez dans le menu Calculs";
    message += "puis Lancer les calculs.";
    final BuLabelMultiLine text= new BuLabelMultiLine(message);
    final BuButton tbvalider= new BuButton("VALIDER");
    /*c.insets.bottom = 0; c.insets.top = 0; c.insets.left = 30;
    c.gridx = 0; 
    placeComposant( lm, text1      , c);
    c.insets.top = 0;
    placeComposant( lm, text2      , c);
    c.insets.top = 20;
    placeComposant( lm, text3      , c);
    c.insets.top = 0;
    placeComposant( lm, text4      , c);
    
    c.ipadx       = 50;
    c.ipady       = 50;    
    c.insets.top = 40;
    c.gridx = 0;  c.fill = c.CENTER; c.anchor = c.CENTER;
    placeComposant( lm, tbvalider  , c);*/
    add(text, BuBorderLayout.CENTER);
    add(tbvalider, BuBorderLayout.SOUTH);
    tbvalider.addActionListener(new TbValiderListener());
  }
  public void MessageAssistant(final BuCommonInterface _appli, final String mess) {
    final BuDialogMessage message=
      new BuDialogMessage(_appli, _appli.getInformationsSoftware(), mess);
    message.setSize(700, 200);
    message.setTitle(" ERREUR ");
    message.setResizable(false);
    final Point pos= this.getLocationOnScreen();
    pos.x= pos.x + this.getWidth() / 2 - message.getWidth() / 2;
    pos.y= pos.y + this.getHeight() / 2 - message.getHeight() / 2;
    message.setLocation(pos);
    message.setModal(false);
    //int j=0;
    //String messErreur[] = new String[30];
    //for(int i=0;i<mess.length();i++){
    //if(mess.charAt(i)!='\n')
    //messErreur[j] = new String(mess);
    //else j++;
    //}
    //for(int i=0;i<j;i++)    
    //message.getContentPane().add(new JLabel(messErreur[i]));
    message.setVisible(true);
  }
  class TbValiderListener implements ActionListener {
    //DiapreImplementation diapre;
    //BuCommonInterface appli;
    public TbValiderListener() {
      //diapre = diapre_;
      //appli= appli_; 
    }
    public void actionPerformed(final ActionEvent e) {
      String messErreur= new String("");
      final SParametresDiapre parametresDiapre= fp_.getParametresDiapre();
      boolean erreur= false;
      final int nombrePointTalus= parametresDiapre.talusDiapre.nombrePointsTalus;
      final int nombreCouche= parametresDiapre.sol.nombreCouches;
      //double dernierPointTalus = parametresDiapre.talusDiapre.pointsTalus[nombrePointTalus-2].ordonneePointDiapre;
      final double dernierPointEcran=
        parametresDiapre.talusDiapre.pointsTalus[nombrePointTalus
          - 1].ordonneePointDiapre;
      final double minTalus= fp_.pnGeometrie.getMinTalus();
      final int nombreSurchargesLineiques=
        parametresDiapre.surchargesProjet.nombreSurchargesLineiques;
      final int nombreSurchargesTrapezoidales=
        parametresDiapre.surchargesProjet.nombreSurchargesTrapezoidales;
      final double xmin=
        parametresDiapre.talusDiapre.pointsTalus[0].abscissePointDiapre;
      final double xmax=
        parametresDiapre.talusDiapre.pointsTalus[nombrePointTalus
          - 2].abscissePointDiapre;
      // Sols
      //premiere couche
      if (nombreCouche == 1
        && parametresDiapre.sol.premiereCouche.limiteInferieureCouche
          > dernierPointEcran) {
        messErreur=
          "Erreur sol : la limite inf�rieure de la couche n�1 doit �tre inf�rieure � "
            + dernierPointEcran
            + " m";
        erreur= true;
      }
      if (nombreCouche > 1
        && parametresDiapre.sol.premiereCouche.limiteInferieureCouche
          > minTalus) {
        messErreur=
          messErreur
            + "\nErreur sol : la limite inf�rieure de la couche n�1 doit �tre inf�rieure � "
            + minTalus
            + " m";
        erreur= true;
      }
      //derniere couche
      if (nombreCouche > 1
        && parametresDiapre.sol.autresCouches[nombreCouche
          - 2].limiteInferieureCouche
          > dernierPointEcran) {
        messErreur=
          messErreur
            + "\nErreur sol : la limite inf�rieure de la couche n�"
            + nombreCouche
            + " doit �tre inf�rieure � "
            + dernierPointEcran
            + " m";
        erreur= true;
      }
      // Eau
      if (parametresDiapre.eau.presenceNappeEau == 1
        && parametresDiapre.eau.coteNappeEau > minTalus) {
        messErreur=
          messErreur
            + "\nErreur eau : l'ordonn�e de la nappe d'eau doit �tre inf�rieure � "
            + minTalus
            + " m";
        erreur= true;
      }
      // Surcharges
      if (nombreSurchargesLineiques > 0
        && (parametresDiapre.surchargesProjet.surchargesLineiques[0].abscisse
          < xmin
          || parametresDiapre
            .surchargesProjet
            .surchargesLineiques[nombreSurchargesLineiques
            - 1].abscisse
            > xmax)) {
        messErreur=
          messErreur
            + "\nErreur surcharges lin�iques: les abscisses doivent appartenir � ["
            + xmin
            + CtuluLibString.VIR
            + xmax
            + "]";
        erreur= true;
      }
      if (nombreSurchargesTrapezoidales > 0
        && (parametresDiapre
          .surchargesProjet
          .surchargesTrapezoidales[0]
          .abscisseDebut
          < xmin
          || parametresDiapre
            .surchargesProjet
            .surchargesTrapezoidales[nombreSurchargesTrapezoidales
            - 1].abscisseFin
            > xmax)) {
        messErreur=
          messErreur
            + "\nErreur surcharges trap�zo�dales: les abscisses doivent appartenir � ["
            + xmin
            + CtuluLibString.VIR
            + xmax
            + "]";
        erreur= true;
      }
      //System.out.println("nombre points : "+parametresDiapre.talusDiapre.nombrePointsTalus);
      if (erreur) {
        MessageAssistant(appli_, messErreur);
      } else {
        fp_.enregistreParametres();
        MessageAssistant(
          appli_,
          "Les param�tres sont valides\nVous pouvez lancer le calcul");
        appli_.setEnabledForAction("CALCULER", true);
      }
      /*{
        try {
          String nomFichier=diapre.projet_.getFichier();
          String nomCourt;
          String repertoire = System.getProperty("user.dir")+File.separator;
          if ( nomFichier.lastIndexOf(File.separator) == -1 )
            nomCourt = nomFichier;
          else
            nomCourt = nomFichier.substring(nomFichier.lastIndexOf(File.separator));
          
          int i=0;
          while(nomCourt.charAt(i)!='.')
            i++;  
          nomCourt=nomCourt.substring(1,i);
          
          System.out.println( nomCourt);
          parametresDiapre.nomFichierEntree = nomCourt;
          DParametresDiapre.ecritParametresDiapre(nomCourt,repertoire,parametresDiapre); //lecture
        }
        catch(IOException evt){
          //new BuDialogError(getApp(), isDiapre_, u.toString()).activate();
          diapre.setEnabledForAction("CALCULER"       , false);
          System.out.println(evt);
          return;
        }
        diapre.fp_.setVisible(false);
        diapre.projet_.addParam("params",diapre.fp_.getParametresDiapre());
        diapre.projet_.enregistre();
        diapre.setEnabledForAction("CALCULER", true);
      }*/
    }
  }
}
