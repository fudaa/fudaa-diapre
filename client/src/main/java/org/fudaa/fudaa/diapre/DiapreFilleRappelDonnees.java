/*
 * @file         DiapreFilleRappelDonnees.java
 * @creation     2000-12-10
 * @modification $Date: 2006-09-19 15:02:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;

import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuCommonInterface;

import org.fudaa.dodico.corba.diapre.SParametresDiapre;
import org.fudaa.dodico.corba.diapre.SParametresGeneraux;
import org.fudaa.dodico.corba.diapre.SSol;
import org.fudaa.dodico.corba.diapre.SSurchargesProjet;

/**
 * G�n�ration du rappel des donn�es au format HTML et stackage dans des strings.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:02:15 $ by $Author: deniger $
 * @author Fouzia Elouafi , Jean de Malafosse
 */
public class DiapreFilleRappelDonnees extends BuBrowserFrame {
  public DiapreImplementation implementation;
  // Pour l'enregistrement du rapport en html:
  String html1; // parametresgeneraux
  String html2; // sols
  String html3; // eau
  String html4; // surcharges
  boolean nappeEau;
  double coteNappeEau ;
  double yrideau;
  String typeCalcul = "";

  public DiapreFilleRappelDonnees(final BuCommonInterface _appli) {
    super();
    implementation = (DiapreImplementation) _appli.getImplementation();
  }

  /**
   * M�thode de g�n�ration du rappel des donn�es au format HTML dans une chaine de caract�res.
   */
  public String construitRappelDonnees(final SParametresDiapre donnees) {
    final StringBuffer html = new StringBuffer("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n");
    final double[] ytalus = new double[2];
    // boolean nappeEau ;
    // double coteNappeEau=0;
    // double yrideau;
    // String typeCalcul=new String("");
    ytalus[0] = implementation.fp_.pnGeometrie.getMinTalus();
    ytalus[1] = implementation.fp_.pnGeometrie.getMaxTalus();
    yrideau = donnees.talusDiapre.pointsTalus[donnees.talusDiapre.nombrePointsTalus - 1].ordonneePointDiapre;
    if (donnees.eau != null && donnees.eau.presenceNappeEau == 1 && donnees.eau.coteNappeEau > yrideau) {
      nappeEau = true;
      if (donnees.eau.typeCalculEau.equals("S")) {
        typeCalcul = "statique";
      } else {
        typeCalcul = "dynamique";
      }
      coteNappeEau = donnees.eau.coteNappeEau;
    } else {
      nappeEau = false;
    }
    html.append("<html><head>\n");
    html.append("<title>Rappel des donn&eacute;es</title></head>");
    html.append("<center><h1>Rapport</h1></center>\n");
    html.append("<br><br><br>\n");
    html.append("<body bgcolor=white>\n"); // #\"FFFFFF\">");
    html.append("<h1>I. Rappel des donn�es.</h1>\n");
    // html.append((implementation.getInformationsDocument()).toString()+"<BR><BR>\n");
    html.append(construitParametresGeneraux(donnees.parametresGeneraux));
    html.append(construitSol(donnees.sol, ytalus, nappeEau, coteNappeEau));
    html.append(construitEau(typeCalcul, nappeEau, coteNappeEau));
    html.append(construitSurcharges(donnees.surchargesProjet));
    html.append("<h2>Graphique</h2>\n");
    html.append("<img src=" + "diapre-logo.gif" + ">\n");
    html.append("<img src=" + "graphRapp.gif" + ">\n");
    html.append("<br><br><br>");
    html.append("<h1>II. R�sultats.</h1>\n");
    html.append("<img src=" + "graphResult.gif" + ">\n");
    html.append("<br><br><br>\n");
    html.append("</body></html>");
    return html.toString();
  }

  /** Construction de la partie pour les donn�es g�n�rales. */
  public StringBuffer construitParametresGeneraux(final SParametresGeneraux dg) {
    final StringBuffer html = new StringBuffer("");
    String choixButeePoussee;
    String choixCalcul;
    if (dg.choixButeePoussee.equals("B")) {
      choixButeePoussee = "but&eacute;e";
    } else {
      choixButeePoussee = "pouss&eacute;e";
    }
    if (dg.choixCalcul.equals("L")) {
      choixCalcul = "long";
    } else {
      choixCalcul = "court";
    }
    html.append("<br><h3>Description du calcul.</h3>\n");
    html1 = "<br><h3>Description du calcul.</h3>\n";
    html.append("Le calcul est effectu&eacute; en " + "<font color=green>" + choixButeePoussee + "</font>"
        + " avec une inclinaison sur l'&eacute;cran &eacute;gale � delta = " // <img src=\"diapre_5.gif\"> = "
        + "<font color=green>" + dg.deltaSurPhi + "</font>" + "* phi . "
        // <img src=\"phi.gif\"> . "
        + "\nIl s'agit d'un calcul � " + "<font color=green>" + choixCalcul + "</font>" + " terme.");
    html1 = html1
        + "Le calcul est effectu&eacute; en "
        + "<font color=green>"
        + choixButeePoussee
        + "</font>"
        + " avec une inclinaison sur l'&eacute;cran &eacute;gale � <img src=\"delta.gif\" width=10 height=12 align=abscenter> = "
        + "<font color=green>" + dg.deltaSurPhi + "</font>"
        + " * <img src=\"phi.gif\" width=12 height=12 align=abscenter> . " + "\nIl s'agit d'un calcul � "
        + "<font color=green>" + choixCalcul + "</font>" + " terme.";
    return html;
  }

  /** Construction de la partie pour les donn�es g�n�rales. */
  public StringBuffer construitSol(final SSol sol, final double[] ytalus, boolean _nappeEau, final double _coteNappeEau) {
    final StringBuffer html = new StringBuffer("");
    boolean np = false;
    // Premier tableau : donn�es des sols.
    html.append("<br><br><h3>Description des sols.</h3>\n");
    html2 = "<br><br><h3>Description des sols.</h3>\n";
    html.append("<table BORDER cellspacing=0 COLS=6 WIDTH=\"100%\">\n");
    html2 = html2 + "<table BORDER cellspacing=0 COLS=6 WIDTH=\"100%\">\n";
    // Ligne 1 : intitul�s des colonnes.
    html.append("<tr><td>Num&eacute;ro de la couche</td>\n");
    html2 = html2 + "<tr><td>Num&eacute;ro de la couche</td>\n";
    html.append("<td>Cote sup&eacute;rieure (m)</td>\n");
    html2 = html2 + "<td>Cote sup&eacute;rieure (m)</td>\n";
    html.append("<td>Cote inf&eacute;rieure (m)</td>\n");
    html2 = html2 + "<td>Cote inf&eacute;rieure (m)</td>\n";
    html.append("<td>Poids volumique de calcul (kN/m�)</td>\n");
    html2 = html2 + "<td>Poids volumique de calcul (kN/m�)</td>\n";
    html.append("<td>Coh&eacute;sion (kN/m�)</td>\n");
    html2 = html2 + "<td>Coh&eacute;sion (kN/m�)</td>\n";
    html.append("<td>Angle de frottement interne (degr&eacute;)</td></tr>\n");
    html2 = html2 + "<td>Angle de frottement interne (degr&eacute;)</td></tr>\n";
    html.append("<tr><td align=CENTER>" + 1 + "</td>\n");
    html2 = html2 + "<tr><td align=CENTER>" + 1 + "</td>\n";
    // limite superieure
    if (ytalus[0] == ytalus[1]) {
      html.append("<td align=CENTER>" + ytalus[0] + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + ytalus[0] + "</td>\n";
    } else {
      html.append("<td align=CENTER>" + "entre " + ytalus[0] + " et " + ytalus[1] + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + "entre " + ytalus[0] + " et " + ytalus[1] + "</td>\n";
    }
    // limite inferieure
    if (_nappeEau && _coteNappeEau > sol.premiereCouche.limiteInferieureCouche) {
      // premiere partie
      html.append("<td align=CENTER>" + _coteNappeEau + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + _coteNappeEau + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.poidsVolumiqueTotal + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.poidsVolumiqueTotal + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.cohesion + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.cohesion + "</td>\n";
      html.append("<td align=center>" + sol.premiereCouche.angleFrottementInterne + "</td>\n");
      html2 = html2 + "<td align=center>" + sol.premiereCouche.angleFrottementInterne + "</td>\n";
      html.append("</tr>\n");
      html2 = html2 + "</tr>\n";
      // deuxieme partie
      html.append("<tr><td align=CENTER>" + 1 + "</td>\n");
      html2 = html2 + "<tr><td align=CENTER>" + 1 + "</td>\n";
      html.append("<td align=CENTER>" + _coteNappeEau + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + _coteNappeEau + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.limiteInferieureCouche + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.limiteInferieureCouche + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.poidsVolumique + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.poidsVolumique + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.cohesion + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.cohesion + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.angleFrottementInterne + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.angleFrottementInterne + "</td>\n";
      html.append("</tr>\n");
      html2 = html2 + "</tr>\n";
      _nappeEau = false;
      np = true;
    } else {
      html.append("<td align=CENTER>" + sol.premiereCouche.limiteInferieureCouche + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.limiteInferieureCouche + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.poidsVolumiqueTotal + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.poidsVolumiqueTotal + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.cohesion + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.cohesion + "</td>\n";
      html.append("<td align=CENTER>" + sol.premiereCouche.angleFrottementInterne + "</td>\n");
      html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.angleFrottementInterne + "</td>\n";
      html.append("</tr>\n");
      html2 = html2 + "</tr>\n";
    }
    for (int i = 0; i < sol.nombreCouches - 1; i++) {
      html.append("<tr><td align=CENTER>" + (i + 2) + "</td>\n");
      html2 = html2 + "<tr><td align=CENTER>" + (i + 2) + "</td>\n";
      // limite superieure
      if (i == 0) {
        // limitesup=sol.premiereCouche.limiteInferieureCouche;
        html.append("<td align=CENTER>" + sol.premiereCouche.limiteInferieureCouche + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.premiereCouche.limiteInferieureCouche + "</td>\n";
      } else {
        // limitesup=sol.autresCouches[i-1].limiteInferieureCouche;
        html.append("<td align=CENTER>" + sol.autresCouches[i - 1].limiteInferieureCouche + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i - 1].limiteInferieureCouche + "</td>\n";
      }
      if (_nappeEau && _coteNappeEau > sol.autresCouches[i].limiteInferieureCouche) {
        // premiere partie
        html.append("<td align=CENTER>" + _coteNappeEau + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + _coteNappeEau + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].poidsVolumiqueTotal + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].poidsVolumiqueTotal + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].cohesion + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].cohesion + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].angleFrottementInterne + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].angleFrottementInterne + "</td>\n";
        html.append("</tr>\n");
        html2 = html2 + "</tr>\n";
        // deuxieme partie
        html.append("<tr><td align=CENTER>" + (i + 2) + "</td>\n");
        html2 = html2 + "<tr><td align=CENTER>" + (i + 2) + "</td>\n";
        html.append("<td align=CENTER>" + _coteNappeEau + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + _coteNappeEau + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].limiteInferieureCouche + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].limiteInferieureCouche + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].poidsVolumique + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].poidsVolumique + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].cohesion + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].cohesion + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].angleFrottementInterne + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].angleFrottementInterne + "</td>\n";
        html.append("</tr>\n");
        html2 = html2 + "</tr>\n";
        _nappeEau = false;
        np = true;
      } else {
        html.append("<td align=CENTER>" + sol.autresCouches[i].limiteInferieureCouche + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].limiteInferieureCouche + "</td>\n";
        if (np) {
          html.append("<td align=CENTER>" + sol.autresCouches[i].poidsVolumique + "</td>\n");
          html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].poidsVolumique + "</td>\n";
        } else {
          html.append("<td align=CENTER>" + sol.autresCouches[i].poidsVolumiqueTotal + "</td>\n");
          html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].poidsVolumiqueTotal + "</td>\n";
        }
        html.append("<td align=CENTER>" + sol.autresCouches[i].cohesion + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].cohesion + "</td>\n";
        html.append("<td align=CENTER>" + sol.autresCouches[i].angleFrottementInterne + "</td>\n");
        html2 = html2 + "<td align=CENTER>" + sol.autresCouches[i].angleFrottementInterne + "</td>\n";
        html.append("</tr>\n");
        html2 = html2 + "</tr>\n";
      }
    }
    html.append("</table>");
    html2 = html2 + "</table>";
    return html;
  } // Fin du rapport pour les sols.

  // construction de la partie html de l'eau
  public StringBuffer construitEau(final String _typeCalcul, final boolean _nappeEau, final double _coteNappeEau) {
    final StringBuffer html = new StringBuffer(500);
    html.append("<br><br>\n");
    html3 = "<br><br>\n";
    html.append("<h3>Description de l'eau :</h3>\n");
    html3 = html3 + "<h3>Description de l'eau :</h3>\n";
    if (_nappeEau) {
      html.append("<tr><td>Le calcul tient compte de l'action " + "<font color=green>" + _typeCalcul + "</font>"
          + " de l'eau.");
      html3 = html3 + "<tr><td>Le calcul tient compte de l'action " + "<font color=green>" + _typeCalcul + "</font>"
          + " de l'eau.";
      html.append("<tr><td>La nappe d'eau est � la cote  " + "<font color=green>" + _coteNappeEau + "</font>" + " m.");
      html3 = html3 + "<tr><td>La nappe d'eau est � la cote  " + "<font color=green>" + _coteNappeEau + "</font>"
          + " m.";
    } else {
      html.append("</td></tr>\nL'eau n'a pas &eacute;t&eacute; prise en compte");
      html3 = html3 + "</td></tr>\nL'eau n'a pas &eacute;t&eacute; prise en compte";
    }
    return html;
  }

  /** Construction de la partie html des surcharges. */
  public StringBuffer construitSurcharges(final SSurchargesProjet surcharge) {
    final StringBuffer html = new StringBuffer("");
    html.append("<br><br>\n");
    html4 = "<br><br>\n";
    html.append("<h3>Description des surcharges.</h3>\n");
    html4 = html4 + "<h3>Description des surcharges.</h3>\n";
    // html.append("<hr WIDTH=100% SIZE=3 ALIGN=center>");//trace une ligne
    html.append("<h4>Surcharges lin&eacute;iques.</h4>\n");
    html4 = html4 + "<h4>Surcharges lin&eacute;iques.</h4>\n";
    // surcharge lineiques
    if (surcharge != null && surcharge.nombreSurchargesLineiques > 0) {
      html.append("<table BORDER cellspacing=0 COLS=4 WIDTH=\"100%\">\n");
      html4 = html4 + "<table BORDER cellspacing=0 COLS=4 WIDTH=\"100%\">\n";
      // Ligne 1 : intitul�s des colonnes.
      html.append("<tr><td>Num&eacute;ro</td>\n");
      html4 = html4 + "<tr><td>Num&eacute;ro</td>\n";
      html.append("<td>X (m)</td>\n");
      html4 = html4 + "<td>X (m)</td>\n";
      html.append("<td>V (kN/ml)</td>\n");
      html4 = html4 + "<td>V (kN/ml)</td>\n";
      html.append("<td>H (kN/ml)</td>\n");
      html4 = html4 + "<td>H (kN/ml)</td>\n";
      for (int i = 0; i < surcharge.nombreSurchargesLineiques; i++) {
        html.append("<tr><td align=center>" + (i + 1) + "</td>\n");
        html4 = html4 + "<tr><td align=center>" + (i + 1) + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesLineiques[i].abscisse + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesLineiques[i].abscisse + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesLineiques[i].forceVerticale + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesLineiques[i].forceVerticale + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesLineiques[i].forceHorizontale + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesLineiques[i].forceHorizontale + "</td>\n";
      }
      html.append("</table>\n");
      html4 = html4 + "</table>\n";
    } else {
      html.append("<tr><td>Aucune surcharge lin&eacute;ique n'a &eacute;t&eacute; entr&eacute;e");
      html4 = html4 + "<tr><td>Aucune surcharge lin&eacute;ique n'a &eacute;t&eacute; entr&eacute;e";
    }
    // surchage unforme
    html.append("<br><h4>Surcharge uniforme semi-infinie.</h4>\n");
    html4 = html4 + "<br><h4>Surcharge uniforme semi-infinie.</h4>\n";
    if (surcharge != null && surcharge.nombreSurchargeUniforme > 0) {
      html.append("V = " + surcharge.surchargeUniforme.forceVerticale + " kN/m�<br>\n");
      html4 = html4 + "V = " + surcharge.surchargeUniforme.forceVerticale + " kN/m�<br>\n";
      html.append("H = " + surcharge.surchargeUniforme.forceHorizontale + " kN/m�<br>\n");
      html4 = html4 + "H = " + surcharge.surchargeUniforme.forceHorizontale + " kN/m�<br>\n";
    } else {
      html.append("Aucune surcharge uniforme n'a &eacute;t&eacute; entr&eacute;e");
      html4 = html4 + "Aucune surcharge uniforme n'a &eacute;t&eacute; entr&eacute;e";
    }
    // surcharges trapezoidale
    html.append("<br><h4>Surcharges trap&eacute;zoidales.</h4>\n");
    html4 = html4 + "<br><h4>Surcharges trap&eacute;zoidales.</h4>\n";
    if (surcharge != null && surcharge.nombreSurchargesTrapezoidales > 0) {
      html.append("<table BORDER cellspacing=0 COLS=7 WIDTH=\"100%\">\n");
      html4 = html4 + "<table BORDER cellspacing=0 COLS=7 WIDTH=\"100%\">\n";
      // Ligne 1 : intitul�s des colonnes.
      html.append("<tr><td>Num&eacute;ro</td>\n");
      html4 = html4 + "<tr><td>Num&eacute;ro</td>\n";
      html.append("<td>X1 (m)</td>\n");
      html4 = html4 + "<td>X1 (m)</td>\n";
      html.append("<td>X2 (m)</td>\n");
      html4 = html4 + "<td>X2 (m)</td>\n";
      html.append("<td>V1 (kN/ml)</td>\n");
      html4 = html4 + "<td>V1 (kN/ml)</td>\n";
      html.append("<td>V2 (kN/ml)</td>\n");
      html4 = html4 + "<td>V2 (kN/ml)</td>\n";
      html.append("<td>H1 (kN/ml)</td>\n");
      html4 = html4 + "<td>H1 (kN/ml)</td>\n";
      html.append("<td>H2 (kN/ml)</td>\n");
      html4 = html4 + "<td>H2 (kN/ml)</td>\n";
      for (int i = 0; i < surcharge.nombreSurchargesTrapezoidales; i++) {
        html.append("<tr><td align=CENTER>" + (i + 1) + "</td>\n");
        html4 = html4 + "<tr><td align=CENTER>" + (i + 1) + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].abscisseDebut + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].abscisseDebut + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].abscisseFin + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].abscisseFin + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceVerticaleDebut + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceVerticaleDebut + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceVerticaleFin + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceVerticaleFin + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceHorizontaleDebut + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceHorizontaleDebut + "</td>\n";
        html.append("<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceHorizontaleFin + "</td>\n");
        html4 = html4 + "<td align=CENTER>" + surcharge.surchargesTrapezoidales[i].forceHorizontaleFin + "</td>\n";
      }
      html.append("</table>");
      html4 = html4 + "</table>";
    } else {
      html.append("<tr><td>Aucune surcharge trap&eacute;zoidale n'a &eacute;t&eacute; entr&eacute;e");
      html4 = html4 + "<tr><td>Aucune surcharge trap&eacute;zoidale n'a &eacute;t&eacute; entr&eacute;e";
    }
    return html;
  }
}
