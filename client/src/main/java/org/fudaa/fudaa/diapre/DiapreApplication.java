/*
 * @file         DiapreApplication.java
 * @creation     1999-10-01
 * @modification $Date: 2003-11-25 10:13:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import com.memoire.bu.BuApplication;
/**
 * L'application cliente Diapre.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:13:38 $ by $Author: deniger $
 * @author       Fouzia Elouafi , Jean de Malafosse
 */
public class DiapreApplication extends BuApplication {
  /**
   * Constructeur de l objet DiapreApplication
   */
  public DiapreApplication() {
    super();
    setImplementation(new DiapreImplementation());
  }
}
