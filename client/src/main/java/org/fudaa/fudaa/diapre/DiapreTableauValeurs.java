/*
 * @file         DiapreTableauValeurs.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;

import org.fudaa.dodico.corba.diapre.SParametresDiapre;
import org.fudaa.dodico.corba.diapre.SResultatsDiapre;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Fenetre d'affichage d'un tableau .
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreTableauValeurs extends JDialog implements ActionListener {
  DiapreDiagrammePression diag;
  //protected SResultatsDiapre SResultats;
  //protected SParametresDiapre SParametres;
  private FudaaProjet projet_;
  int precis;
  int nb1;
  double[] tab1= new double[5000];
  double[] tab2= new double[5000];
  double[] tab3= new double[5000];
  double[] tab4= new double[5000];
  //private BuDialogMessage message= null;
  //private BuCommonInterface appli_;
  public String titre= "Diagramme des pressions: tableau ";
  // Zones de saisie
  MyTableModel myModel;
  JTable table;
  // CONSTRUCTEUR /////////////////////////////////////////////////////////
  public DiapreTableauValeurs(final BuCommonInterface _appli, final FudaaProjet _p) {
    super(_appli.getFrame(), "tableau des valeurs", true);
    System.out.println("ok!!!!!!!!!!!!!!");
    System.out.println("DiapreTableauValeurs debut");
    setTitle(titre);
    setSize(600, 350);
    setLocation(230, 100);
    setModal(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    projet_= _p;
    myModel= new MyTableModel();
    table= new JTable(myModel);
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    //c.anchor      = GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 2; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    // premiere colonne de la fenetre
    c.gridx= 0;
    c.gridy= 0;
    table.setPreferredScrollableViewportSize(new Dimension(500, 170));
    final JScrollPane scrollPane= new JScrollPane(table);
    placeComposant(lm, scrollPane, c);
    c.gridy= 1;
    final BuButton ok= new BuButton(" Valider ");
    placeComposant(lm, ok, c);
    ok.addActionListener(new DiapreokListener());
    // deuxieme colonne
    c.gridx= 1;
    c.gridy= 1;
    //BuButton     graph    = new BuButton(" Graph ");
    //placeComposant(lm, graph, c);
    //graph.addActionListener(new DiapreGraphListener());
    //diag = new DiapreDiagrammePression(_appli);
  }
  SParametresDiapre getParametres() {
    return (SParametresDiapre)projet_.getParam(DiapreResource.PARAMETRES);
  }
  SResultatsDiapre getResultats() {
    if (projet_ == null) {
      System.out.println("projet nul");
    } else {
      System.out.println("projet non null");
    }
    if (projet_.getResult(DiapreResource.RESULTATS) == null) {
      System.out.println("resu nul");
    } else {
      System.out.println("resu non null");
    }
    return (SResultatsDiapre) (projet_.getResult(DiapreResource.RESULTATS));
  }
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  //Permet de n'afficher qu'une partie des informations
  public void setPrecision(final int precision) {
    precis= precision;
    final SParametresDiapre SParams= getParametres();
    final SResultatsDiapre SResultats= getResultats();
    System.out.println("set " + precis);
    final double nb= SParams.options.nombreSegmentsEcran / precision;
    int j= 0;
    while (j < nb) {
      j++;
    }
    nb1= j;
    System.out.println(nb1);
    for (int i= 0; i < nb1; i++) {
      tab1[i]=
        SResultats.pointsDiagrammePression[precis * i].ordonneeMilieuSegment;
      tab2[i]= SResultats.pointsDiagrammePression[precis * i].pressionEffective;
      tab3[i]=
        SResultats.pointsDiagrammePression[precis * i].pressionInterstitielle;
      tab4[i]= SResultats.pointsDiagrammePression[precis * i].pressionTotale;
    }
  }
  public void setProjet(final FudaaProjet _projet) {
    projet_= _projet;
  }
  public void actionPerformed(final ActionEvent e) {
    //setVisible(true);
  }
  // BOUTTONS
  /*class DiapreGraphListener implements ActionListener {
        public void actionPerformed( ActionEvent e)
        {
          //diag.setVisible(true);
          dispose();
        }
    }*/
  class DiapreokListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      dispose();
    }
  }
  // TABLE MODEL
  class MyTableModel extends AbstractTableModel {
    public int getColumnCount() {
      return 4;
    }
    public int getRowCount() {
      if (getResultats() != null) {
        return nb1;
      }
      return 0;
    }
    public boolean isCellEditable(final int _row, final int _col) {
      return false;
    }
    public Object getValueAt(final int _row, final int _col) {
      Object r= null;
      Double tmp= new Double(0);
      String vide= "-";
      final SResultatsDiapre SResultats= getResultats();
      final SParametresDiapre SParametres= getParametres();
      if (SParametres.parametresGeneraux.choixCalcul.equals("L")) {
        switch (_col) {
          case 0 :
            if (SResultats != null) {
              tmp= new Double(tab1[_row]);
            }
            r= tmp;
            break;
          case 1 :
            if (SResultats != null) {
              tmp= new Double(tab2[_row]);
            }
            r= tmp;
            break;
          case 2 :
            if (SResultats != null) {
              tmp= new Double(tab3[_row]);
            }
            r= tmp;
            break;
          case 3 :
            if (SResultats != null) {
              tmp= new Double(tab4[_row]);
            }
            r= tmp;
            break;
        }
      }
      if (SParametres.parametresGeneraux.choixCalcul.equals("C")) {
        switch (_col) {
          case 0 :
            if (SResultats != null) {
              tmp= new Double(tab1[_row]);
            }
            r= tmp;
            break;
          case 1 :
            if (SResultats != null) {
              vide= "   -";
            }
            r= vide;
            break;
          case 2 :
            if (SResultats != null) {
              vide= "   -";
            }
            r= vide;
            break;
          case 3 :
            if (SResultats != null) {
              tmp= new Double(tab2[_row]);
            }
            r= tmp;
            break;
        }
      }
      return r;
    }
    public String getColumnName(final int _col) {
      String r= null;
      switch (_col) {
        case 0 :
          r= "Ordonnee milieu";
          break;
        case 1 :
          r= "P. effective";
          break;
        case 2 :
          r= "P. interstitielle";
          break;
        case 3 :
          r= "P. totale";
          break;
      }
      return r;
    }
    public void setValueAt(final Object _obj, final int _row, final int _col) {}
    public Class getColumnClass(final int _col) {
      Class r= Object.class;
      switch (_col) {
        case 0 :
          r= String.class;
          break;
        case 1 :
          r= String.class;
          break;
        case 2 :
          r= String.class;
          break;
      }
      return r;
    }
    //public void addTableModelListener (TableModelListener _l)
    //{}
    //public void removeTableModelListener (TableModelListener _l)
    //{}
  }
}
