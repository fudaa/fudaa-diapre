/*
 * @file         DiapreSolsParametres.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.diapre.SParametresGeneraux;
import org.fudaa.dodico.corba.diapre.SSol;
/**
 * Classe impl�mentant l'onglet "Sols" pour les param�tres de diapre.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author       Fouzia Elouafi
 */
public class DiapreSolsParametres extends BuPanel implements ActionListener {
  // Raccourci vers les donn�es g�n�rales locales de l'application.
  SSol sol;
  SParametresGeneraux parametresGeneraux;
  BuCommonInterface appli_;
  // Zones de saisie contenues dans l'onglet
  BuButton interfaceHorizontale=
    new BuButton("L'interface entre les couches de sol est horizontale  ");
  BuButton interfaceQuelconque=
    new BuButton("L'interface entre les couches de sol est quelconque ");
  // creation de boite de dialogue
  boolean blocus= true;
  static DiapreInterfaceHorizontale interHorizontale;
  int nombreCouches;
  double[] ymin= new double[2];
  // M�thode utile pour placer un composant dans l'onglet.
  // @param lm layout manager � utiliser.
  // @param composant composant � placer
  // @param c contraintes � utiliser.
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }
  // Constructeur. appli doit etre instance de DiapreImplementation
  public DiapreSolsParametres(final BuCommonInterface appli) {
    super();
    //DiapreImplementation implementation = (DiapreImplementation)appli.getImplementation();
    appli_= appli;
    // bouton inaccessible dans cette version
    interfaceQuelconque.setEnabled(false);
    //interfaceHorizontale.setToolTipText("indisponible pour cette version");
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    // Cr�ation de l'onglet lui-meme
    this.setLayout(lm);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    // Contraintes communes � tous les composants
    c.anchor= GridBagConstraints.SOUTH;
    c.ipadx= 50;
    c.ipady= 50;
    c.gridx= 0;
    c.gridy= 0;
    c.gridwidth= 1;
    c.gridheight= 1;
    c.weightx= 100;
    c.weighty= 100;
    c.fill= GridBagConstraints.CENTER;
    placeComposant(lm, interfaceHorizontale, c);
    /*c.gridx = 0; c.gridy = 1; c.gridwidth = 1; c.gridheight = 1; c.weightx = 100; c.weighty = 100; c.fill = c.CENTER;
    placeComposant ( lm, new JLabel("")  , c );
    c.gridx = 0; c.gridy = 2; c.gridwidth = 1; c.gridheight = 1; c.weightx = 100; c.weighty = 100; c.fill = c.CENTER;
    placeComposant ( lm, new JLabel("")  , c );
    c.gridx = 0; c.gridy = 3; c.gridwidth = 1; c.gridheight = 1; c.weightx = 100; c.weighty = 100; c.fill = c.CENTER;
    placeComposant ( lm, new JLabel("")  , c );
    c.gridx = 0; c.gridy = 4; c.gridwidth = 1; c.gridheight = 1; c.weightx = 100; c.weighty = 100; c.fill = c.CENTER;
    placeComposant ( lm, new JLabel("")  , c );*/
    c.ipadx= 10;
    c.ipady= 10;
    c.gridx= 0;
    c.gridy= 1;
    c.gridwidth= 1;
    c.gridheight= 1;
    c.weightx= 100;
    c.weighty= 100;
    c.fill= GridBagConstraints.CENTER;
    placeComposant(lm, new JLabel(""), c);
    c.anchor= GridBagConstraints.NORTH;
    c.ipadx= 50;
    c.ipady= 50;
    c.gridx= 0;
    c.gridy= 2;
    c.gridwidth= 1;
    c.gridheight= 1;
    c.weightx= 100;
    c.weighty= 100;
    c.fill= GridBagConstraints.CENTER;
    placeComposant(lm, interfaceQuelconque, c);
    interfaceHorizontale.addActionListener(new InterfaceHorizontaleListener());
    setParametresSols(sol);
    setParametresGeneraux(parametresGeneraux);
  }
  public void actionPerformed(final ActionEvent e) {
    sol= getParametresSols();
    parametresGeneraux= getParametresGeneraux();
    setVisible(true);
  }
  // Accesseur de donneesGenerales avec enregistrement pr�alable des donn�es contenues dans l'onglet.
  //Permet de lire les donn�es pr�sentes dans l'onglet.
  public void setMinOrdonnee(final double[] min) {
    ymin[0]= min[0];
    ymin[1]= min[1];
  }
  public double[] getMinOrdonnee() {
    return ymin;
  }
  public void setParametresGeneraux(final SParametresGeneraux parametresGeneraux_) {
    parametresGeneraux= parametresGeneraux_;
  }
  public SParametresGeneraux getParametresGeneraux() {
    if (!blocus) {
      parametresGeneraux= interHorizontale.getParametresGeneraux();
    }
    return parametresGeneraux;
  }
  public SSol getParametresSols() {
    if (!blocus) {
      sol= interHorizontale.getParametresSols();
    }
    return sol;
  }
  // Mutateur de donneesGenerales avec affichage des nouvelles valeurs dans l'onglet. Permet de d�finir les
  // donn�es trait�es dans l'onget. Elles sont imm�diatement affich�es.
  public void setParametresSols(final SSol sol_) {
    sol= sol_;
  }
  //affiche une boite de dialogue pour le choix du calcul
  class InterfaceHorizontaleListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (blocus) {
        interHorizontale= new DiapreInterfaceHorizontale(appli_);
        interHorizontale.setParametresGeneraux(parametresGeneraux);
        interHorizontale.setParametresSols(sol);
        blocus= false;
      }
      interHorizontale.setMinOrdonnee(ymin);
      interHorizontale.setVisible(true);
    }
  }
}
