/*
 * @file         DiapreOptionsParametres.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.diapre.SOptions;
/**
 * Description de l'onglet des parametres des options.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:13 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreOptionsParametres
  extends BuPanel
  implements ActionListener {
  // fenetre de visualisation des options de discr�tisation par d�faut
  DiapreOptionsDiscret optionsDiscret;
  //private DiapreImplementation Diapre;
  boolean blocus= true;
  //Structure � remplir
  SOptions parametresOptions;
  final String[] ligne= { "D", "S" };
  // Pour calculer la hauteur du rideau on a besoin des deux derni�res ordonn�es
  double ordonnee1; //Avant Derni�re Ordonn�e
  double ordonnee2; //Derni�re ordonn�e
  //zones de saisie
  JComboBox boxligneGlissement= new JComboBox(ligne);
  BuButton discret= new BuButton("Discr�tisation calcul�e par d�faut   ");
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }
  public DiapreOptionsParametres(final BuCommonInterface _appli) {
    super();
    //Diapre= ((DiapreImplementation)_appli.getImplementation());
    // grille d'affichage
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    this.setLayout(lm);
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    //c.weighty    = 100;  //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    //c.insets.top = 50;
    //c.insets.bottom = 50;
    //premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    c.anchor= GridBagConstraints.EAST;
    c.weighty= 10;
    final BuLabel gli= new BuLabel("La ligne de glissement est   :       ");
    placeComposant(lm, gli, c);
    c.gridy= 3;
    c.gridwidth= 2;
    c.anchor= GridBagConstraints.CENTER;
    c.weighty= 30;
    final BuLabel blab=
      new BuLabel("________________________________________________________________________________________________");
    placeComposant(lm, blab, c);
    c.ipadx= 50; // espace autour des composants
    c.ipady= 50;
    c.gridy= 4;
    c.gridwidth= 2;
    c.anchor= GridBagConstraints.CENTER;
    c.weighty= 50;
    placeComposant(lm, discret, c);
    discret.addActionListener(new DiapreOptionsDiscretListener(_appli));
    //deuxieme colonne
    c.gridx= 1;
    c.gridy= 0;
    c.gridwidth= 1;
    c.anchor= GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    boxligneGlissement.setEditable(false);
    placeComposant(lm, boxligneGlissement, c);
    c.gridy= 1;
    c.weighty= 5;
    final BuLabel lab2= new BuLabel("   S = Succession de segments  ");
    placeComposant(lm, lab2, c);
    c.gridy= 2;
    c.weighty= 5;
    final BuLabel lab3= new BuLabel("   D = Une seule droite  ");
    placeComposant(lm, lab3, c);
    //c.ipadx = 50; // espace autour des composants
    //c.ipady = 50;
    //c.gridy = 4;
    //placeComposant( lm, modif, c);
    //modif.setEnabled(false);
  }
  public void actionPerformed(final ActionEvent e) {
    if (!blocus) {
      getParametresOptions();
    }
    setVisible(true);
  }
  public void setOrdonnee1(final double y1) {
    ordonnee1= y1;
  }
  public void setOrdonnee2(final double y2) {
    ordonnee2= y2;
  }
  public int getHauteur() {
    final double h= 100 * (ordonnee1 - ordonnee2);
    int i= 0;
    while (i < h) {
      i++;
    }
    return (i);
  }
  // Mutateur des parametres de Options
  public void setParametresOptions(final SOptions parametresOptions_) {
    parametresOptions= parametresOptions_;
    if (parametresOptions != null) {
      boxligneGlissement.setSelectedItem(parametresOptions.ligneGlissement);
      //optionsDiscret.setParametresOptions(parametresOptions);
    }
  }
  // Accesseur des parametres de Options
  public SOptions getParametresOptions() {
    if (parametresOptions == null) {
      parametresOptions= new SOptions();
    }
    if (!blocus) {
      parametresOptions= optionsDiscret.getParametresOptions();
      parametresOptions.ligneGlissement=
        (String)boxligneGlissement.getSelectedItem();
      System.out.println(
        "Ligne de glissement :" + parametresOptions.ligneGlissement);
    }
    return parametresOptions;
  }
  // Fenetre: Discretisation calculee par defaut
  class DiapreOptionsDiscretListener implements ActionListener {
    private BuCommonInterface appli;
    public DiapreOptionsDiscretListener(final BuCommonInterface appli_) {
      appli= appli_;
    }
    public void actionPerformed(final ActionEvent e) {
      if (blocus) {
        optionsDiscret= new DiapreOptionsDiscret(appli, getHauteur());
        blocus= false;
      }
      optionsDiscret.setParametresOptions(parametresOptions);
      optionsDiscret.setOrdonnee1(ordonnee1);
      optionsDiscret.setOrdonnee2(ordonnee2);
      optionsDiscret.setParametresOptions(parametresOptions);
      //System.out.println(parametresOptions.nombreSegments);
      //optionsDiscret.getHauteur();
      optionsDiscret.affiche();
      getParametresOptions();
      optionsDiscret.setVisible(true);
    }
  }
}
