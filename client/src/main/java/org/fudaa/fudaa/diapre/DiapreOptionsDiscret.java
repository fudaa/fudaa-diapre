/*
 * @file         DiapreOptionsDiscret.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;

import org.fudaa.dodico.corba.diapre.SOptions;
/**
 * Fenetre de saisie des parametres des options.
 * Ces parametres sont enregistr�s dans la structure SOptions.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:13 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreOptionsDiscret extends JDialog implements ActionListener {
  // fenetre de saisie des options de discr�tisation
  DiapreOptionsModif optionsModif;
  SOptions parametresOptions;
  double ordonnee1;
  double ordonnee2;
  static public int i;
  boolean blocus= true;
  static private boolean bool= true;
  // zones de saisie
  TextFieldsDiapre nombreSegmentsEcran= new TextFieldsDiapre();
  String nb;
  TextFieldsDiapre largeurSegmentDebut= new TextFieldsDiapre(false);
  String segdeb;
  TextFieldsDiapre nombreSegmentsTalus= new TextFieldsDiapre();
  String segfin;
  BuButton valider= new BuButton("Valider");
  BuButton modif= new BuButton("Modifier");
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  public DiapreOptionsDiscret(final BuCommonInterface _appli, final int i_) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Options: discr�tisation du talus et de l'�cran calcul�e par d�faut ",
      true);
    setSize(550, 300);
    setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    i= i_;
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    c.anchor= GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    // premi�re colonne
    c.gridx= 0;
    c.gridy= 0;
    final BuLabel nbSeg= new BuLabel("  Nombre de segments discr�tisant l'�cran :");
    placeComposant(lm, nbSeg, c);
    c.gridy= 1;
    final BuLabel taillPremier=
      new BuLabel("  Taille du segment le plus proche de l'�cran discr�tisant le talus :");
    placeComposant(lm, taillPremier, c);
    c.gridy= 2;
    final BuLabel taillDernier=
      new BuLabel("  Nombre de segments discr�tisant le talus :");
    placeComposant(lm, taillDernier, c);
    c.gridy= 3;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(lm, valider, c);
    valider.addActionListener(new DiapreValiderListener());
    // deuxi�me colonne
    c.gridx= 1;
    c.gridy= 0;
    //System.out.println("hauteur");
    nombreSegmentsEcran.setEditable(false);
    placeComposant(lm, nombreSegmentsEcran, c);
    c.gridy= 1;
    segdeb= new String("0.01");
    largeurSegmentDebut.setValue(segdeb);
    largeurSegmentDebut.setEditable(false);
    placeComposant(lm, largeurSegmentDebut, c);
    c.gridy= 2;
    segfin= new String("1000");
    nombreSegmentsTalus.setValue(segfin);
    nombreSegmentsTalus.setEditable(false);
    placeComposant(lm, nombreSegmentsTalus, c);
    c.gridy= 3;
    placeComposant(lm, modif, c);
    modif.addActionListener(new DiapreModifListener(_appli));
    // troisieme colonne
    c.gridx= 2;
    c.gridy= 1;
    c.anchor= GridBagConstraints.WEST;
    final BuLabel unit= new BuLabel("m");
    placeComposant(lm, unit, c);
  }
  public void setOrdonnee1(final double y1) {
    ordonnee1= y1;
  }
  public void setOrdonnee2(final double y2) {
    ordonnee2= y2;
  }
  public int getHauteur() {
    return (int)Math.floor(100 * (ordonnee1 - ordonnee2));
  }
  public void affiche() {
    nb= new String(getHauteur() + "");
    nombreSegmentsEcran.setValue(nb);
  }
  // Mutateur des parametres des options
  public void setParametresOptions(final SOptions parametresOptions_) {
    parametresOptions= parametresOptions_;
    /*if (parametresOptions != null) {
      nombreSegmentsEcran.setValue            (parametresOptions.nombreSegmentsEcran);
      largeurSegmentDebut.setValue       (parametresOptions.largeurSegmentDebut);
      nombreSegmentsTalus.setValue         (parametresOptions.nombreSegmentsTalus);
    }*/
  }
  // Accesseur des parametres des options
  public SOptions getParametresOptions() {
    if (parametresOptions == null) {
      parametresOptions= new SOptions();
    }
    if (!blocus && !bool) {
      parametresOptions= optionsModif.getParametresOptions();
    } else {
      parametresOptions.nombreSegmentsEcran= getHauteur();
      parametresOptions.largeurSegmentDebut= 0.01;
      parametresOptions.nombreSegmentsTalus= 1000;
    }
    return parametresOptions;
  }
  public void actionPerformed(final ActionEvent e) {
    if (!blocus) {
      parametresOptions= getParametresOptions();
    }
    setVisible(true);
  }
  class DiapreValiderListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      getParametresOptions();
      setVisible(false);
    }
  }
  class DiapreModifListener implements ActionListener {
    BuCommonInterface appli;
    public DiapreModifListener(final BuCommonInterface appli_) {
      appli= appli_;
    }
    public void actionPerformed(final ActionEvent e) {
      if (blocus) {
        optionsModif= new DiapreOptionsModif(appli);
        blocus= false;
      }
      optionsModif.setParametresOptions(parametresOptions);
      optionsModif.setOrdonnee1(ordonnee1);
      optionsModif.setOrdonnee2(ordonnee2);
      optionsModif.setParametresOptions(parametresOptions);
      //optionsModif.getHauteur();
      setVisible(false);
      optionsModif.affiche();
      optionsModif.setVisible(true);
    }
  }
}
