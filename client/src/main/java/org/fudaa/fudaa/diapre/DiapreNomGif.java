/*
 * @file         DiapreNomGif.java
 * @creation     2000-12-21
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JDialog;
import javax.swing.WindowConstants;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
/**
 * Fenetre d'affichage de la saisie du nom du fichier .gif.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreNomGif extends JDialog implements ActionListener {
  BuCommonInterface appli_;
  private BuDialogMessage message= null;
  DiapreImplementation diapre= null;
  //zone de saisie
  BuButton ok= new BuButton("Valider");
  BuTextField saisie= new BuTextField();
  public String nomFichier= ""; // nom du fichier
  boolean bool= true; //si pas de saisie
  int choix= 0; //si fichier image, false si fichier html
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  public DiapreNomGif(final BuCommonInterface _appli, final int choix_) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Saisie du nom du fichier ",
      true);
    appli_= _appli;
    setSize(400, 200);
    setModal(true);
    //setVisible(true);
    //setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    diapre= (DiapreImplementation)_appli.getImplementation();
    choix= choix_;
    saisie.setColumns(20);
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    c.anchor= GridBagConstraints.WEST;
    c.ipadx= 5;
    c.ipady= 5;
    c.gridwidth= 2; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    c.insets.left= 15;
    c.insets.right= 15;
    //premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    c.anchor= GridBagConstraints.CENTER;
    final BuLabel lab0= new BuLabel("Entrer le nom du fichier sans l'extension :");
    placeComposant(lm, lab0, c);
    c.gridy= 1;
    c.gridwidth= 1;
    c.anchor= GridBagConstraints.EAST;
    if (choix == 1) {
      saisie.setText("graphRapp");
    }
    if (choix == 2) {
      saisie.setText("graphResult");
    }
    if (choix == 3) {
      saisie.setText("rapport");
    }
    saisie.setColumns(5);
    placeComposant(lm, saisie, c);
    saisie.addFocusListener(new SaisieAction());
    c.gridy= 2;
    c.gridwidth= 2;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(lm, ok, c);
    ok.addActionListener(new okListener());
    //deuxieme colonne
    BuLabel lab;
    c.gridx= 1;
    c.gridy= 1;
    c.gridwidth= 1;
    c.anchor= GridBagConstraints.WEST;
    if (choix == 1) {
      lab= new BuLabel(".gif");
    }
    if (choix == 2) {
      lab= new BuLabel(".gif");
    } else {
      lab= new BuLabel(".html");
    }
    placeComposant(lm, lab, c);
  }
  public void actionPerformed(final ActionEvent e) {
    setVisible(true);
  }
  class okListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (bool && saisie.getText() != null && !saisie.getText().equals("")) {
        nomFichier= saisie.getText();
        if (choix == 1) {
          diapre.setNomGifRapp(nomFichier); // Fichier gif
        }
        if (choix == 2) {
          diapre.setNomGifResult(nomFichier);
        }
        if (choix == 3) {
          diapre.setNomHtml(nomFichier);
        }
        dispose();
      } else {
        messageAssistant("Entrer le nom sans l'extension:");
      }
    }
  }
  /*--- message de l'assistant ---*/
  void messageAssistant(final String s) {
    message= new BuDialogMessage(appli_, DiapreImplementation.isDiapre_, s);
    message.setSize(400, 150);
    message.setTitle(" ERREUR ");
    message.setResizable(false);
    message.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    final Point pos= this.getLocationOnScreen();
    pos.x= pos.x + this.getWidth() / 2 - message.getWidth() / 2;
    pos.y= pos.y + this.getHeight() / 2 - message.getHeight() / 2;
    message.setLocation(pos);
    message.setVisible(true);
  }
  class SaisieAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool= true;
      ok.setEnabled(true);
    }
    public void focusLost(final FocusEvent e) {
      if (saisie.getText() == null || (saisie.getText()).equals("")) {
        bool= false;
      }
    }
  }
}
