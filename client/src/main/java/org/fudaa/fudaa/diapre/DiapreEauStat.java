/*
 * @file         DiapreEauStat.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JDialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;

import org.fudaa.dodico.corba.diapre.SEau;
/**
 * Fenetre de saisie des parametres statiques de la nappe d'eau.
 * Ces parametres sont enregistr�s dans la structure SEau.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:12 $ by $Author: deniger $
 * @author       Jean de Malafosse , Fouzia Elouafi
 */
public class DiapreEauStat extends JDialog implements ActionListener {
  SEau parametresEau;
  // zones de saisie
  TextFieldsDiapre coteNappeEau= new TextFieldsDiapre(true);
  BuButton valid= new BuButton("Valider");
  BuButton annul= new BuButton("Supprimer la nappe d'eau");
  // Bouleen pour une saisie totale
  boolean bool;
  // Bouleen pour savoir si l'utilisateur a saisi valider ou annuler
  boolean bool1= true; //true: valider, false: annuler
  double minOrdonnee;
  double ymin;
  BuCommonInterface appli;
  //private DiapreImplementation implementation;
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  public DiapreEauStat(final BuCommonInterface _appli) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Action statique de la nappe d'eau ",
      true);
    appli= _appli;
    //implementation= (DiapreImplementation)_appli.getImplementation();
    setSize(600, 250);
    setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    // grille d'affchage
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    c.anchor= GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    //Premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    c.gridwidth= 3;
    c.anchor= GridBagConstraints.CENTER;
    final BuLabel txt= new BuLabel("La nappe d'eau est suppos�e horizontale");
    placeComposant(lm, txt, c);
    c.gridy= 1;
    c.gridwidth= 1;
    c.anchor= GridBagConstraints.WEST;
    final BuLabel txt1= new BuLabel("   Ordonn�e de la nappe d'eau :");
    placeComposant(lm, txt1, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.CENTER;
    placeComposant(lm, valid, c);
    //valid.setEnabled(false);
    //valid.setEnabled(false);
    valid.addActionListener(new DiapreValidListener());
    //deuxieme colonne
    c.gridx= 1;
    c.gridy= 1;
    placeComposant(lm, coteNappeEau, c);
    //remove(coteNappeEau);
    //troisieme colonne
    c.gridx= 2;
    c.gridy= 1;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, new BuLabel("m"), c);
    c.gridy= 2;
    placeComposant(lm, annul, c);
    annul.addActionListener(new DiapreAnnulListener());
    // Pour voir si une case est remplie
    coteNappeEau.addFocusListener(new CoteNappeEauAction());
    //coteNappeEau.setRequestFocusEnabled(true);
    //coteNappeEau.requestFocus();
    coteNappeEau.setEditable(true);
    setParametresEau(parametresEau);
  }
  public void actionPerformed(final ActionEvent e) {
    parametresEau= getParametresEau();
    setVisible(true);
  }
  public void setMinOrdonnee(final double[] min) {
    minOrdonnee= min[0];
    ymin= min[1];
  }
  // Mutateur des parametres de la nappe d'eau
  public void setParametresEau(final SEau parametresEau_) {
    parametresEau= parametresEau_;
    if (parametresEau != null && parametresEau.presenceNappeEau == 1) {
      coteNappeEau.setValue(parametresEau.coteNappeEau);
      bool= true;
    }
  }
  // Accesseur des parametres de la nappe d'eau
  public SEau getParametresEau() {
    if (parametresEau == null) {
      parametresEau= new SEau();
    }
    if (bool1
      && coteNappeEau.getTextDiapre() != null
      && !coteNappeEau.getTextDiapre().equals("")) {
      parametresEau.presenceNappeEau= 1;
      System.out.println(
        "Pr�sence d'une nappe d'eau: " + parametresEau.presenceNappeEau);
      parametresEau.typeCalculEau= "S";
      System.out.println("Type de calcul: " + parametresEau.typeCalculEau);
      parametresEau.coteNappeEau= coteNappeEau.getValue();
      System.out.println(
        "Cote de la nappe d'eau : " + parametresEau.coteNappeEau);
    } else {
      parametresEau.presenceNappeEau= 0;
      System.out.println(
        "Pr�sence d'une nappe d'eau: " + parametresEau.presenceNappeEau);
    }
    return parametresEau;
  }
  class DiapreValidListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (bool
        && coteNappeEau.getTextDiapre() != null
        && !coteNappeEau.getTextDiapre().equals("")) {
        //bool=true;
        getParametresEau();
        if (coteNappeEau.getValue() > minOrdonnee) {
          new BuDialogMessage(
            appli,
            DiapreImplementation.isDiapre_,
            "La nappe d'eau doit �tre inf�rieure\n"
              + "� la cote inf�rieure du talus,\n"
              + "soit "
              + minOrdonnee
              + " m")
            .activate();
        } else {
          //System.out.println(ymin+"      "+coteNappeEau.getValue());
          if (coteNappeEau.getValue() < ymin) {
            new BuDialogMessage(
              appli,
              DiapreImplementation.isDiapre_,
              "La nappe d'eau ne sera pas prise en compte"
                + "\npar le code de calcul.")
              .activate();
          }
          bool1= true;
          getParametresEau();
          dispose();
        }
      } else {
        new BuDialogMessage(
          appli,
          DiapreImplementation.isDiapre_,
          "Vous devez compl�ter l'ordonn�e\n" + "de la nappe d'eau")
          .activate();
      }
    }
  }
  class DiapreAnnulListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      bool1= false;
      getParametresEau();
      coteNappeEau.setValue("");
      valid.setEnabled(false);
      dispose();
    }
  }
  class CoteNappeEauAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      bool= true;
      valid.setEnabled(true);
    }
    public void focusLost(final FocusEvent e) {
      if (coteNappeEau.getTextDiapre() == null
        || coteNappeEau.getTextDiapre().equals("")) {
        bool= false;
      }
    }
  }
}
