/*
 * @file         DiapreOptionsModif.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JDialog;
import javax.swing.WindowConstants;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;

import org.fudaa.dodico.corba.diapre.SOptions;
/**
 * Fenetre de saisie des parametres des options.
 * Ces parametres sont enregistr�s dans la structure SOptions.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:09 $ by $Author: deniger $
 * @author       Jean de Malafosse
 */
public class DiapreOptionsModif extends JDialog implements ActionListener {
  SOptions parametresOptions;
  private BuDialogMessage message;
  private DiapreImplementation diapre;
  private BuCommonInterface appli_;
  static private double ordonnee1;
  static private double ordonnee2;
  // zones de saisie
  TextFieldsDiapre nombreSegmentsEcran= new TextFieldsDiapre();
  int nbe;
  TextFieldsDiapre largeurSegmentDebut= new TextFieldsDiapre(false);
  String segdeb;
  TextFieldsDiapre nombreSegmentsTalus= new TextFieldsDiapre();
  BuButton valider= new BuButton("Valider");
  // Bouleen pour une saisie totale
  int bool1;
  int bool2;
  int bool3;
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  public DiapreOptionsModif(final BuCommonInterface _appli) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Options: discr�tisation du talus et de l'�cran  ",
      true);
    setSize(550, 300);
    setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (_appli instanceof Frame) {
      final Point pos= ((Frame)_appli).getLocationOnScreen();
      pos.x += (((Frame)_appli).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)_appli).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    appli_= _appli;
    diapre= ((DiapreImplementation)appli_.getImplementation());
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    c.anchor= GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    // premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    placeComposant(
      lm,
      new BuLabel("  Nombre de segments discr�tisant l'�cran :"),
      c);
    c.gridy= 1;
    placeComposant(
      lm,
      new BuLabel("  Taille du segment le plus proche de l'�cran discr�tisant le talus :"),
      c);
    c.gridy= 2;
    placeComposant(
      lm,
      new BuLabel("  Nombre de segments discr�tisant le talus :"),
      c);
    // deuxieme colonne
    c.gridx= 1;
    c.gridy= 0;
    c.anchor= GridBagConstraints.EAST;
    //DecimalFormat fmt=new DecimalFormat();
    //nombreSegments.setDisplayFormat(Format 2);
    //fmt.setMaximumFractionDigits(2);
    placeComposant(lm, nombreSegmentsEcran, c);
    c.gridy= 1;
    //largeurSegmentDebut.setValue(0.01);
    placeComposant(lm, largeurSegmentDebut, c);
    c.gridy= 2;
    //largeurSegmentFin.setValue(0.1);
    placeComposant(lm, nombreSegmentsTalus, c);
    c.gridy= 3;
    valider.setEnabled(true);
    placeComposant(lm, valider, c);
    valider.addActionListener(new DiapreValiderListener());
    // troisieme colonne
    c.gridx= 2;
    c.gridy= 1;
    c.anchor= GridBagConstraints.WEST;
    final BuLabel unit= new BuLabel("m");
    placeComposant(lm, unit, c);
    c.gridy= 3;
    placeComposant(lm, new BuLabel("      "), c);
    // Pour voir si une case est remplie
    nombreSegmentsEcran.addFocusListener(new NombreSegmentsEcranAction());
    largeurSegmentDebut.addFocusListener(new LargeurSegmentDebutAction());
    nombreSegmentsTalus.addFocusListener(new NombreSegmentsTalusAction());
  }
  public void actionPerformed(final ActionEvent e) {
    parametresOptions= getParametresOptions();
    setVisible(true);
  }
  public void setOrdonnee1(final double y1) {
    ordonnee1= y1;
  }
  public void setOrdonnee2(final double y2) {
    ordonnee2= y2;
  }
  public int getHauteur() {
    final double h= 100 * (ordonnee1 - ordonnee2);
    int i= 0;
    while (i < h) {
      i++;
    }
    //System.out.println(i);
    return (i);
  }
  public void affiche() {
    getParametresOptions();
    if (parametresOptions != null
      && parametresOptions.nombreSegmentsEcran != 0) {
      nombreSegmentsEcran.setValue(parametresOptions.nombreSegmentsEcran);
      largeurSegmentDebut.setValue(parametresOptions.largeurSegmentDebut);
      nombreSegmentsTalus.setValue(parametresOptions.nombreSegmentsTalus);
    } else {
      nombreSegmentsEcran.setValue(getHauteur());
      largeurSegmentDebut.setValue("0.01");
      final int segfin= 1000;
      nombreSegmentsTalus.setValue(segfin);
    }
  }
  // Mutateur des parametres des options
  public void setParametresOptions(final SOptions parametresOptions_) {
    parametresOptions= parametresOptions_;
    if (parametresOptions != null) {
      nombreSegmentsEcran.setValue(parametresOptions.nombreSegmentsEcran);
      largeurSegmentDebut.setValue(parametresOptions.largeurSegmentDebut);
      nombreSegmentsTalus.setValue(parametresOptions.nombreSegmentsTalus);
    }
  }
  // Accesseur des parametres des options
  public SOptions getParametresOptions() {
    //System.out.println(diapre.fp_.pnGeometrie.getOrdonnee1());
    //System.out.println(diapre.fp_.pnGeometrie.getOrdonnee2());
    if (diapre.fp_.pnGeometrie.getOrdonnee1() != ordonnee1
      || diapre.fp_.pnGeometrie.getOrdonnee2() != ordonnee2) {
      ordonnee1= diapre.fp_.pnGeometrie.getOrdonnee1();
      ordonnee2= diapre.fp_.pnGeometrie.getOrdonnee2();
      nombreSegmentsEcran.setValue(getHauteur());
      largeurSegmentDebut.setValue(0.01);
      nombreSegmentsTalus.setValue(1000);
    }
    //System.out.println(ordonnee1);
    //System.out.println(ordonnee2);
    if (parametresOptions == null) {
      parametresOptions= new SOptions();
    }
    if (nombreSegmentsEcran.getTextDiapre() != null
      && !nombreSegmentsEcran.getTextDiapre().equals("")) {
      parametresOptions.nombreSegmentsEcran= nombreSegmentsEcran.getInt();
      System.out.println(
        "Nombre de segments Ecran: " + parametresOptions.nombreSegmentsEcran);
    }
    if (largeurSegmentDebut.getTextDiapre() != null
      && !largeurSegmentDebut.getTextDiapre().equals("")) {
      parametresOptions.largeurSegmentDebut= largeurSegmentDebut.getValue();
      System.out.println(
        "Largeur du segment d�but : " + parametresOptions.largeurSegmentDebut);
    }
    if (nombreSegmentsTalus.getTextDiapre() != null
      && !nombreSegmentsTalus.getTextDiapre().equals("")) {
      parametresOptions.nombreSegmentsTalus= nombreSegmentsTalus.getInt();
      System.out.println(
        "Nombre de segments Talus : " + parametresOptions.nombreSegmentsTalus);
    }
    return parametresOptions;
  }
  /*--- message de l'assistant ---*/
  void messageAssistant(final String s) {
    message= new BuDialogMessage(appli_, DiapreImplementation.isDiapre_, s);
    message.setSize(600, 130);
    message.setResizable(false);
    message.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    final Point pos= this.getLocationOnScreen();
    pos.x= pos.x + this.getWidth() / 2 - message.getWidth() / 2;
    pos.y= pos.y + this.getHeight() / 2 - message.getHeight() / 2;
    message.setLocation(pos);
    message.setVisible(true);
  }
  public void testChampsComplet() {
    //bool=0 : inchang� , bool=1 : chang� et faux , bool=2 : chang� et bon
    if ((bool1 == 2 && bool2 == 2 && bool3 == 2)
      || (bool1 == 2 && bool2 == 0 && bool3 == 0)
      || (bool1 == 0 && bool2 == 2 && bool3 == 0)
      || (bool1 == 0 && bool2 == 0 && bool3 == 2)
      || (bool1 == 2 && bool2 == 2 && bool3 == 0)
      || (bool1 == 2 && bool2 == 0 && bool3 == 2)
      || (bool1 == 0 && bool2 == 2 && bool3 == 2)) {
      valider.setEnabled(true);
    }
    if (bool1 == 1 || bool2 == 1 || bool3 == 1) {
      valider.setEnabled(false);
    }
  }
  class DiapreValiderListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      getParametresOptions();
      //System.out.println(parametresOptions.nombreSegments);
      if ((parametresOptions.nombreSegmentsEcran < 1)
        || (parametresOptions.nombreSegmentsEcran > 5000)) {
        messageAssistant("ERREUR: le nombre de segments est compris entre 1 et 5000.");
      } else {
        setVisible(false);
      //if(nombreSegments.getTextDiapre().equals("")  || largeurSegmentDebut.getTextDiapre() == "" || largeurSegmentFin.getTextDiapre() == "" )
      //   messageAssistant("ERREUR: Vous devez compl�ter toutes les donn�es .");
      }
    }
  }
  // Saisie obligatoire de tous les parametres !!!
  class NombreSegmentsEcranAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      testChampsComplet();
      if (nombreSegmentsEcran.getTextDiapre() != null
        || nombreSegmentsEcran.getTextDiapre() != "") {
        bool1= 2;
      }
      //System.out.println("1 : "+bool1);
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      testChampsComplet();
      if (nombreSegmentsEcran.getTextDiapre() == null
        || nombreSegmentsEcran.getTextDiapre().equals("")) {
        bool1= 1;
      }
      //System.out.println("1 : "+bool1);
      testChampsComplet();
    }
  }
  class LargeurSegmentDebutAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      testChampsComplet();
      if (largeurSegmentDebut.getTextDiapre() != null
        || largeurSegmentDebut.getTextDiapre() != "") {
        bool2= 2;
      }
      //System.out.println("2 : "+bool2);
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      testChampsComplet();
      if (largeurSegmentDebut.getTextDiapre() == null
        || largeurSegmentDebut.getTextDiapre().equals("")) {
        bool2= 1;
      }
      //System.out.println("2 : "+bool2);
      testChampsComplet();
    }
  }
  class NombreSegmentsTalusAction implements FocusListener {
    public void focusGained(final FocusEvent e) {
      testChampsComplet();
      if (nombreSegmentsTalus.getTextDiapre() != null
        || nombreSegmentsTalus.getTextDiapre() != "") {
        bool3= 2;
      }
      //System.out.println("3 : "+bool3);
      testChampsComplet();
    }
    public void focusLost(final FocusEvent e) {
      testChampsComplet();
      if (nombreSegmentsTalus.getTextDiapre() == null
        || nombreSegmentsTalus.getTextDiapre().equals("")) {
        bool3= 1;
      }
      //System.out.println("3 : "+bool3);
      testChampsComplet();
    }
  }
}
