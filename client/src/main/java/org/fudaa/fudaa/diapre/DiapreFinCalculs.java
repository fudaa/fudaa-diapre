/*
 * @file         DiapreFinCalculs.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:02:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPicture;
/**
 * Fenetre d'affichage d'un graphique.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:14 $ by $Author: deniger $
 * @author       Jean de Malafosse 
 */
public class DiapreFinCalculs extends JDialog implements ActionListener {
  BuLabel lab1;
  BuLabel lab2;
  BuLabel lab3;
  String nomFichier;
  String LouC;
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  public DiapreFinCalculs(final String nomFichier_, final String LouC_) {
    nomFichier= nomFichier_;
    LouC= LouC_;
    setTitle("Information");
    setModal(true);
    setSize(650, 350);
    setLocation(300, 200);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    c.gridy= GridBagConstraints.RELATIVE;
    c.ipadx= 5;
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    //premiere colonne
    c.gridx= 0;
    c.gridy= 0;
    Image img;
    img= DiapreResource.DIAPRE.getImage("diapre-logo.gif");
    placeComposant(lm, new BuPicture(img), c);
    c.gridy= 1;
    placeComposant(lm, new BuLabel("Le calcul s'est correctement d�roul�."), c);
    if (LouC.equals("L")) { // Si on est a Long terme 2 fichiers sont g�n�r�s.
      c.gridy= 2;
      lab1=
        new BuLabel("Le calcul ayant �t� effectu� � long terme, Diapre a g�n�r� 2 fichiers de diagrammes de pressions : ");
      placeComposant(lm, lab1, c);
      c.gridy= 3;
      lab2=
        new BuLabel(
          " - " + nomFichier + "_eff.diag pour les contraintes effectives");
      placeComposant(lm, lab2, c);
      c.gridy= 4;
      lab3=
        new BuLabel(
          " - " + nomFichier + "_tot.diag pour les contraintes totales.");
      placeComposant(lm, lab3, c);
    } else { // Si on est a court terme 1 seul.
      c.gridy= 2;
      lab1=
        new BuLabel("Le calcul ayant �t� effectu� � court terme, Diapre a g�n�r� 1 fichier de diagramme de pression : ");
      placeComposant(lm, lab1, c);
      c.gridy= 3;
      lab2= new BuLabel(nomFichier + "_tot.diag pour les contraintes totales.");
      placeComposant(lm, lab2, c);
    }
    c.gridy= 5;
    placeComposant(
      lm,
      new BuLabel("Ces fichiers seront utilis�s par XPLAN pour pr�dimensionner le rideau."),
      c);
    c.gridy= 6;
    placeComposant(
      lm,
      new BuLabel("Vous pouvez visualiser les r�sultats � l'aide du menu R�sultat."),
      c);
    c.gridy= 7;
    final BuButton bok= new BuButton(" VALIDER ");
    placeComposant(lm, bok, c);
    bok.addActionListener(new DiapreokListener());
  }
  public void actionPerformed(final ActionEvent e) {
    setVisible(true);
  }
  class DiapreokListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      dispose();
    }
  }
}
