/*
 * @file DiapreImplementation.java @creation 2000-10-05 @modification $Date: 2007-05-04 13:59:05 $
 * @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231
 * Compiegne @mail devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.print.PrinterJob;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameEvent;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;

import org.fudaa.ctulu.gif.AcmeGifEncoder;
import org.fudaa.dodico.corba.diapre.ICalculDiapre;
import org.fudaa.dodico.corba.diapre.ICalculDiapreHelper;
import org.fudaa.dodico.corba.diapre.IParametresDiapre;
import org.fudaa.dodico.corba.diapre.IParametresDiapreHelper;
import org.fudaa.dodico.corba.diapre.IResultatsDiapre;
import org.fudaa.dodico.corba.diapre.IResultatsDiapreHelper;
import org.fudaa.dodico.corba.diapre.SParametresDiapre;
import org.fudaa.dodico.corba.diapre.SResultatsDiapre;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.diapre.DCalculDiapre;
import org.fudaa.dodico.fortran.FortranWriter;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.ebli.impression.EbliMiseEnPageDialog;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.ebli.impression.EbliPageable;

import org.fudaa.fudaa.commun.FudaaBrowserControl;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.aide.FudaaAidePreferencesPanel;
import org.fudaa.fudaa.commun.aide.FudaaAstucesDialog;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.utilitaire.ServeurCopieEcran;

/**
 * L'implementation du client Diapre.
 * 
 * @version $Id: DiapreImplementation.java,v 1.18 2007-05-04 13:59:05 deniger Exp $
 * @author Fouzia Elouafi , Jean de Malafosse
 */
public class DiapreImplementation extends FudaaImplementation {

  public final static String LOCAL_DIAPRE_MAN = "file:" + System.getProperty("user.dir")
      + "/manuels/Diapre/utilisation.html";
  public final static boolean IS_APPLICATION = true;
  public static ICalculDiapre SERVEUR_DIAPRE ;
  // initialis� dans Diapre.java avec un CCalculDiapre
  public static IConnexion CONNEXION_DIAPRE ;
  private IParametresDiapre diapreParams;
  // Pointe vers parametres() de SERVEUR_DIAPRE
  private IResultatsDiapre diapreResults;
  // Pointe vers resultats() de SERVEUR_DIAPRE
  // private IPersonne PERSONNE;
  protected static BuInformationsSoftware isDiapre_ = new BuInformationsSoftware();
  protected static BuInformationsDocument idDiapre_ = new BuInformationsDocument();
  // Concerne les donn�es
  protected FudaaProjet projet_; // Projet
  // en
  // cours
  // public SParametresDiapre params;
  // public SResultatsDiapre resultats;
  // public SResultatsDiapre resultats0;
  // public SParametresDiapre parametres;
  // Concerne les fenetres
  public DiapreFilleParametres fp_;
  public DiapreTableauValeurs tv_;
  public BGraphe graphique ;
  String nomGifRapp; // pour
  // photo()
  String nomGifResult;
  String nomHtml; // pour
  // html()
  public DiapreTableauInclinaison tableauInclinaison_;
  protected BuBrowserFrame fRappelDonnees_;
  protected BuBrowserFrame fStatistiquesFinales_;
  // variable
  static private boolean ouvrir ;
  static private boolean enregistrer = true;
  static private boolean ouvert ;
  protected BuAssistant assistant_;
  protected EbliFillePrevisualisation previsuFille_;
  protected BuHelpFrame aide_;
  protected BuTaskView taches_;
  protected DiapreListeEtude liste_;
  protected BuInternalFrame fGraphiques_;
  static {
    isDiapre_.name = "Diapre";
    isDiapre_.version = "0.01";
    isDiapre_.date = "18-janvier-2001";
    isDiapre_.rights = "Tous droits r�serv�s. CETMEF (c)1999,2000";
    isDiapre_.contact = "manuel.lemoine@equipement.gouv.fr";
    isDiapre_.license = "GPL2";
    isDiapre_.languages = "fr";
    isDiapre_.logo = DiapreResource.DIAPRE.getIcon("diapre-logo.gif");
    isDiapre_.banner = DiapreResource.DIAPRE.getIcon("diapre-banner.gif");
    isDiapre_.http = "http://marina.cetmef.equipement.gouv.fr/fudaa/";
    isDiapre_.update = "http://marina.cetmef.equipement.gouv.fr/fudaa/deltas/";
    isDiapre_.man = LOCAL_DIAPRE_MAN + "diapre/";
    isDiapre_.authors = new String[] { "Fouzia Elouafi et Jean de Malafosse" };
    isDiapre_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa" };
    isDiapre_.documentors = new String[] {};
    isDiapre_.testers = new String[] { "C.Barou et A. Pourplanche" };
    idDiapre_.name = "Etude";
    idDiapre_.version = "0.18";
    idDiapre_.organization = "CETMEF";
    idDiapre_.author = System.getProperty("user.name");
    idDiapre_.contact = idDiapre_.author + "@cetmef.equipement.gouv.fr";
    idDiapre_.date = FuLib.date();
    BuPrinter.INFO_LOG = isDiapre_;
    BuPrinter.INFO_DOC = idDiapre_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isDiapre_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isDiapre_;
  }

  public BuInformationsDocument getInformationsDocument() {
    return idDiapre_;
  }

  // Constructeur
  public DiapreImplementation() {
    super();
  }

  public void init() {
    super.init();
    fp_ = null;
    projet_ = null;
    aide_ = null;
    try {
      setTitle(isDiapre_.name + " " + isDiapre_.version);
      final BuMenuBar mb = BuMenuBar.buildBasicMenuBar();
      setMainMenuBar(mb);
      mb.addActionListener(this);
      mb.addMenu(construitMenuEdition(IS_APPLICATION));
      mb.addMenu(construitMenuDonnees(IS_APPLICATION));
      mb.addMenu(construitMenuCalculs(IS_APPLICATION));
      mb.addMenu(construitMenuResultats(IS_APPLICATION));
      // ToolBar personnalis�e:
      final BuToolBar tb = new BuToolBar(); // .buildBasicToolBar();
      setMainToolBar(tb);
      tb.addActionListener(this);
      tb.addSeparator();
      tb.addToolButton("Creer", "CREER", true);
      tb.addToolButton("Ouvrir", "OUVRIR", true);
      tb.addToolButton("Enregistrer", "ENREGISTRER", false);
      tb.addToolButton("Fermer ", "FERMER", false);
      tb.addToolButton("Imprimer", "IMPRIMER", false);
      tb.addToolButton("Donn�es ", "PARAMETRE", false);
      tb.addToolButton("Graphique des donn�es", "GRAPHE", false);
      tb.addToolButton("Calculer", "CALCULER", false);
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      tb.addToolButton("R�sultats", "TABLEAU", false);
      tb.addToolButton("Rapport", "PROJET", false);
      // tb.addToolButton("Photo","PHOTOGRAPHIE",false);
      tb.addToolButton("Aide", "AIDE_INDEX", true);
      removeAction("MENU_EDITION");
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", false);
      assistant_ = new DiapreAssistant();
      taches_ = new BuTaskView();
      liste_ = new DiapreListeEtude(getApp());
      final BuScrollPane sp1 = new BuScrollPane(taches_);
      final BuScrollPane sp2 = new BuScrollPane(liste_);
      sp1.setPreferredSize(new Dimension(150, 80));
      sp2.setPreferredSize(new Dimension(150, 80));
      getMainPanel().getRightColumn().addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      getMainPanel().getRightColumn().addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp1, this);
      getMainPanel().getRightColumn().addToggledComponent("Etude", "ETUDE", sp2, this);
      getMainPanel().setLogo(isDiapre_.logo);
      getMainPanel().setTaskView(taches_);
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  public void start() {
    super.start();
    if (FudaaAidePreferencesPanel.isAstucesVisibles(DiaprePreferences.DIAPRE)) {
      FudaaAstucesDialog.showDialog(DiaprePreferences.DIAPRE, this, DiapreAstuces.DIAPRE);
    }
    // ////////
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("diapre")/*, System.getProperty("user.dir")*/);
    // projet_.addFudaaProjetListener(this);
    // //////////
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + isDiapre_.name + " " + isDiapre_.version);
    /*
     * try { //diapreParams = IParametresDiapreHelper.narrow (SERVEUR_DIAPRE.parametres() ); //diapreResults =
     * IResultatsDiapreHelper.narrow (SERVEUR_DIAPRE.resultats() ); } catch( NullPointerException e ) {
     * System.err.println("Pas de connexion serveur => local"); new BuDialogMessage(getApp(), isDiapre_, "Vous n'�tes
     * pas connect� � un serveur Diapre \nVous ne pourrez pas lancer d'�tude ").activate(); }
     */
    final BuMainPanel mp = getMainPanel();
    mp.doLayout();
    mp.validate();
    // FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er une\nnouvelle �tude\nou en ouvrir une");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    DiaprePreferences.DIAPRE.applyOn(this);
  }

  protected BuMenu construitMenuEdition(final boolean _app) {
    final BuMenu ed = new BuMenu("Edition", "EDITION");
    // ed.addMenuItem("Photo du graphique" , "PHOTOGRAPHIE" , false);
    // ed.addMenuItem("Enregistrer le rapport " , "EDITER" , false);
    ed.addMenuItem("Pr�f�rences", "PREFERENCE", true);
    return ed;
  }

  // Cr�� le menu donnees.
  protected BuMenu construitMenuDonnees(final boolean _app) {
    final BuMenu menu = new BuMenu("Donn�es", "DONNEES");
    menu.addMenuItem("Entrer des donn�es", "PARAMETRE", false);
    // BuMenu m= new BuMenu("Rappel des donn�es " , "RAPPEL" );
    // m.addMenuItem("Valeurs","VALEURS",true);
    menu.addMenuItem("Rappel des donn�es - graphique", "GRAPHE", false);
    menu.addMenuItem("Rappel des donn�es - rapport", "PROJET", false);
    // menu.addSubMenu(m,true);
    return menu;
  }

  // Cr�� le menu calculs.
  protected BuMenu construitMenuCalculs(final boolean _app) {
    final BuMenu c = new BuMenu("Calculs", "CALCULS");
    c.addMenuItem("Lancer les calculs", "CALCULER", false);
    return c;
  }

  // Cr�� le menu resultats.
  protected BuMenu construitMenuResultats(final boolean _app) {
    final BuMenu r = new BuMenu("R�sultats", "RESULTATS");
    r.addMenuItem("Diagramme des pressions", "TABLEAU", false);
    r.addMenuItem("Inclinaison des segments", "LISTE", false);
    return r;
  }

  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("CREER")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir();
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("FERMER")) {
      fermer();
    } else if (action.equals("QUITTER")) {
      quitter();
    } else if (action.equals("PREFERENCE")) {
      preferences();
    } else if (action.equals("PARAMETRE")) {
      parametre();
    } else if (action.equals("GRAPHE")) {
      rappelDonnees();
    } else if (action.equals("PROJET")) {
      html();
      valeurs();
    } else if (action.equals("CALCULER")) {
      calculer();
    } else if (action.equals("TABLEAU")) {
      resultatPression();
    } else if (action.equals("LISTE")) {
      resultatInclinaison();
    } else if (("PREVISUALISER".equals(action)) || ("MISEENPAGE".equals(action)) || ("IMPRIMER".equals(action))) {
      gestionnaireImpression(action);
    } else if (action.equals("ASTUCE")) {
      FudaaAstucesDialog.showDialog(DiaprePreferences.DIAPRE, this, DiapreAstuces.DIAPRE);
    } else {
      super.actionPerformed(_evt);
    }
  }

  /*
   * public void paramStructCreated(FudaaParamEvent e){ if( e.getID()==FudaaProjetEvent.HEADER_CHANGED ) {
   * FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage()); setEnabledForAction("ENREGISTRER" ,true); } } public
   * void paramStructDeleted(FudaaParamEvent e) { if( e.getID()==FudaaProjetEvent.HEADER_CHANGED ) {
   * FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage()); setEnabledForAction("ENREGISTRER" ,true); } } public
   * void paramStructModified(FudaaParamEvent e){ if( e.getID()==FudaaProjetEvent.HEADER_CHANGED ) {
   * FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage()); setEnabledForAction("ENREGISTRER" ,true); } } public
   * void dataChanged(FudaaProjetEvent e) { switch( e.getID() ) { case FudaaProjetEvent.RESULT_ADDED: case
   * FudaaProjetEvent.RESULTS_CLEARED: { FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
   * setEnabledForAction("ENREGISTRER" ,true); break; } } } public void statusChanged(FudaaProjetEvent e) { if(
   * e.getID()==FudaaProjetEvent.HEADER_CHANGED ) { FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
   * setEnabledForAction("ENREGISTRER" ,true); } }
   */
  // Commandes activ�es d�s qu'une etude est charg�e
  protected void activerCommandesEtude() {
    setEnabledForAction("PARAMETRE", true);
    setEnabledForAction("ENREGISTRER", true);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("FERMER", true);
  }

  protected void creer() {
    // Cr�ation d'un nouveau projet et tentative d'ouverture
   // final String repertoire = System.getProperty("user.dir") + File.separator + "exemples" + File.separator + "Diapre";
    boolean bool = true;
    if (projet_ != null) {
      if (fermer()) {
        bool = true;
        // si on veut creer un nouveau projet
      } else {
        bool = false;
      }
    }
    if (bool) {
      projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("diapre")/*, repertoire*/);
      projet_.creer();
      // projet_.addFudaaProjetListener(this);
      if (projet_.estConfigure()) {
        String nomFichierVoulu = projet_.getFichier();
        System.out.println(nomFichierVoulu);
        // ajoute l'extension .Diapre si elle manque
        if (nomFichierVoulu.lastIndexOf(".diapre") != (nomFichierVoulu.length() - 7)) {
          projet_.setFichier(projet_.getFichier() + ".diapre");
          nomFichierVoulu = projet_.getFichier();
        }
        ouvrir = true;
        fp_ = new DiapreFilleParametres(getApp(), projet_);
        fp_.addInternalFrameListener(this);
        addInternalFrame(fp_);
        assistant_.addEmitters(fp_);
        setTitle(nomFichierVoulu);
        activerCommandesEtude();
      } else {
        ouvrir = false;
      }
    }
    tempConnecte();
  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {
    setEnabledForAction("FERMER", true);
  }

  public void internalFrameActivated(final InternalFrameEvent e) {
  /*
   * if(ouvrir) fp_.setVisible(false);
   */
  }

  protected void ouvrir() {
    // Cr�ation d'un nouveau projet et tentative d'ouverture
    //final String repertoire = System.getProperty("user.dir") + File.separator + "exemples" + File.separator + "Diapre";
    boolean bool = true; // cas ou projet = null
    if (projet_ != null) {
      if (fermer()) {
        bool = true; // si on veut ouvrir un nouveau projet
      } else {
        bool = false;
      }
    }
    if (bool) {
      projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("diapre")/*, repertoire*/);
      // projet_.addFudaaProjetListener(this);
      projet_.setEnrResultats(true);
      projet_.ouvrir();
      if (projet_.estConfigure()) {
        ouvrir = true;
        ouvert = true;
        fp_ = new DiapreFilleParametres(getApp(), projet_);
        fp_.addInternalFrameListener(this);
        fp_.setDefaultCloseOperation(1);
        final SParametresDiapre d = (SParametresDiapre) (projet_.getParam(DiapreResource.PARAMETRES));
        if ((d != null) && (d.sol != null) && (d.sol.nombreCouches > 0)) {
          setEnabledForAction("GRAPHE", true);
          setEnabledForAction("PROJET", true);
          setEnabledForAction("EDITER", true);
        }
        // fp_.setParametresDiapre(d);
        fp_.setVisible(false);
        addInternalFrame(fp_);
        assistant_.addEmitters(fp_);
        setTitle(projet_.getFichier());
        new BuDialogMessage(getApp(), isDiapre_, "Param�tres charg�s").activate();
        activerCommandesEtude();
      } else {
        ouvrir = false;
      }
    }
  }

  protected void enregistrer() {
    // Enregistrement des donn�es du projet
    projet_.addParam(DiapreResource.PARAMETRES, fp_.getParametresDiapre());
    if (enregistrer && !ouvert) {
      projet_.enregistreSous();
      enregistrer = false;
    } else {
      projet_.enregistre();
    }
  }

  protected void enregistrerSous() {
    // Enregistrement des param�tres
    projet_.addParam("params", fp_.getParametresDiapre());
    projet_.enregistreSous();
  }

  // true=projet fermer false=projet encore ouvert
  protected boolean fermer() {
    if (projet_.estConfigure()) {
      final String nomFichier = projet_.getFichier();
      String nomCourt;
      if (nomFichier.lastIndexOf(File.separator) == -1) {
        nomCourt = nomFichier;
      } else {
        nomCourt = nomFichier.substring(nomFichier.lastIndexOf(File.separator));
      }
      // confirmation d'enregistrement
      final BuDialogConfirmation c = new BuDialogConfirmation(getApp(), isDiapre_, "Voulez-vous enregistrer le projet \n"
          + nomCourt);
      if (c.activate() == JOptionPane.YES_OPTION) {
        enregistrer();
      }
      // confirmation de fermeture
      final BuDialogConfirmation m = new BuDialogConfirmation(getApp(), isDiapre_, "Voulez-vous vraiment fermer le projet \n"
          + nomCourt);
      if (m.activate() == JOptionPane.YES_OPTION) {
        projet_.fermer();
        setTitle(isDiapre_.name + " " + isDiapre_.version);
        ouvrir = false;
        ouvert = false;
        enregistrer = true;
        fp_ = null;
        projet_ = null;
        // On ferme toutes les fenetres du mainPanel
        final BuMainPanel mp = getMainPanel();
        JInternalFrame[] tabframe;
        tabframe = getMainPanel().getAllInternalFrames();
        for (int i = 0; i < tabframe.length; i++) {
          try {
            tabframe[i].setClosed(true);
          } catch (final PropertyVetoException e) {}
          removeInternalFrame(tabframe[i]);
          mp.repaint();
        }
        setEnabledForAction("PARAMETRE", false);
        setEnabledForAction("GRAPHE", false);
        setEnabledForAction("PROJET", false);
        setEnabledForAction("EDITER", false);
        setEnabledForAction("ENREGISTRER", false);
        setEnabledForAction("ENREGISTRERSOUS", false);
        setEnabledForAction("FERMER", false);
        setEnabledForAction("CALCULER", false);
        return true;
      }
      return false;
    }
    return true;
  }

  protected void quitter() {
    exit();
  }

  protected void importer(final String _vagId) {}

  /* Cr�ation ou affichage de la fentre pour les pr�f�rences. */
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLookPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new FudaaAidePreferencesPanel(this, DiaprePreferences.DIAPRE));
    _prefs.add(new EbliMiseEnPagePreferencesPanel());
  }

  // Cr�ation ou affichage d'une fenetre pour saisir les param�tres.
  protected void parametre() {
    if (fp_ == null) {
      // Cr�ation du classeur � onglets de saisie
      fp_ = new DiapreFilleParametres(getApp(), projet_);
      fp_.addInternalFrameListener(this);
      addInternalFrame(fp_);
      assistant_.addEmitters(fp_);
    } else {
      if (fp_.isClosed()) {
        addInternalFrame(fp_);
        setEnabledForAction("FERMER", true);
      } else {
        activateInternalFrame(fp_);
        setEnabledForAction("FERMER", true);
      }
    }
    fp_.ongletCourant = fp_.tpMain.getSelectedIndex();
  }

  protected void calculer() {
    // lancement du calcul
    new BuTaskOperation(this, "Calcul", "oprCalculer").start();
  }

  public void oprServeurCopie() {
    System.err.println("Lancement du serveur de copie d'�cran");
    new ServeurCopieEcran(getMainPanel(), "ScreenSpy");
  }

  public void oprCalculer() {
    tempConnecte();
    // Erreur et retour si aucun serveur Diapre connect�
    if (!isConnected()) {
      System.out.println("pas connecte");
      new BuDialogError(getApp(), isDiapre_, "vous n'�tes pas connect� � un serveur Diapre ! ").activate();
      return;
    }
    // Messages et initialisation des barres de progression.
    setEnabledForAction("CALCULER", false);
    final BuMainPanel mp = getMainPanel();
    mp.setMessage("Transmission des param�tres...");
    final SParametresDiapre parametresDiapre = (SParametresDiapre) projet_.getParam(DiapreResource.PARAMETRES);
    diapreParams = IParametresDiapreHelper.narrow(SERVEUR_DIAPRE.parametres(CONNEXION_DIAPRE));
    diapreParams.parametresDiapre(parametresDiapre);
    mp.setMessage("Fin transmission des param�tres...");
    mp.setProgression(0);
    mp.setMessage("Ex�cution du calcul...");
    SERVEUR_DIAPRE.calcul(CONNEXION_DIAPRE);
    diapreResults = IResultatsDiapreHelper.narrow(SERVEUR_DIAPRE.resultats(CONNEXION_DIAPRE));
    mp.setMessage("Fin ex�cution du calcul...");
    mp.setProgression(60);
    SResultatsDiapre resultatsDiapre = null;
    mp.setMessage("Lecture des r�sultats...");
    resultatsDiapre = diapreResults.resultatsDiapre(); // lecture
    mp.setMessage("Fin lecture des r�sultats...");
    mp.setProgression(70);
    projet_.setEnrResultats(true); // Sauvegarde des resultats activ�e
    projet_.addResult(DiapreResource.RESULTATS, resultatsDiapre);
    // met dans la structure
    if (projet_ == null) {
      System.out.println("projet nuls");
    } else {
      System.out.println("projet non null");
    }
    if (resultatsDiapre == null) {
      System.out.println("Resultats nuls");
    } else {
      System.out.println("Resultats non nuls");
    }
    tv_ = new DiapreTableauValeurs(getApp(), projet_);
    // init
    /*
     * tv.SResultats = new SResultatsDiapre(); tv.SResultats.pointsDiagrammePression = new
     * SPointDiagrammePression[parametres.options.nombreSegmentsEcran]; for (int i = 0 ; i
     * <parametres.options.nombreSegmentsEcran;i++) tv.SResultats.pointsDiagrammePression[i] = new
     * SPointDiagrammePression();
     */
    // lecture du .dat (numero et angle de rupture)
    /*
     * try { resultats0 = DResultatsDiapre.litResultatsDiapredat(parametres); //lecture } catch(IOException e){ //new
     * BuDialogError(getApp(), isDiapre_, u.toString()).activate(); setEnabledForAction("CALCULER" , true);
     * System.out.println(e); return; } mp.setProgression(80); projet_.setEnrResultats(true); //Sauvegarde des resultats
     * activ�e projet_.addResult("result",resultats0); //met dans la structure resultats0 =
     * (SResultatsDiapre)projet_.getResult("result");
     */
    mp.setProgression(100);
    System.out.println("Fin du calcul");
    // Fenetre de visualisation de la fin des calculs
    String nomCourt = new File(projet_.getFichier()).getName();
    // recup�ration du nom du fichier avec l'extension
    nomCourt = nomCourt.substring(0, nomCourt.lastIndexOf('.'));
    // suppression de l'extension
    final String LouC = parametresDiapre.parametresGeneraux.choixCalcul;
    final DiapreFinCalculs calc = new DiapreFinCalculs(nomCourt, LouC);
    if (LouC.equals("L")) {
      ecritureEffDiag(parametresDiapre, resultatsDiapre, nomCourt);
    }
    ecritureTotDiag(parametresDiapre, resultatsDiapre, nomCourt);
    calc.setVisible(true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("LISTE", true);
    setEnabledForAction("TABLEAU", true);
    mp.setProgression(0);
  }

  // Ecriture du fichiers _eff.diag
  protected void ecritureEffDiag(final SParametresDiapre _spd, final SResultatsDiapre _srd, final String _nomCourt) {
    final int nbSegments = _spd.options.nombreSegmentsEcran;
    System.out.println("Ecriture des fichiers .diag pour xplan");
    try {
      final FortranWriter f01 = new FortranWriter(new FileWriter(_nomCourt + "_eff.diag"));
      if (System.getProperty("os.name").startsWith("Windows")) {
        f01.setLineSeparator("\r\n"); // fichiers
        // DOS
        // (/r)
      } else {
        f01.setLineSeparator("\n");
      }
      final int[] fmtI = new int[] { 14, 14 };
      for (int i = 0; i < nbSegments; i++) {
        f01.stringField(0, _srd.pointsDiagrammePression[i].ordonneeMilieuSegment + "");
        f01.stringField(1, _srd.pointsDiagrammePression[i].pressionEffective + "");
        f01.writeFields(fmtI);
      }
      f01.flush();
      f01.close();
    } catch (final Exception e) {
      System.out.println(e);
    }
  }

  // Ecriture des fichiers _tot.diag
  protected void ecritureTotDiag(final SParametresDiapre _spd, final SResultatsDiapre _srd, final String _nomCourt) {
    final int nbSegments = _spd.options.nombreSegmentsEcran;
    System.out.println("Ecriture des fichiers .diag pour xplan");
    try {
      final FortranWriter f01 = new FortranWriter(new FileWriter(_nomCourt + "_tot.diag"));
      if (System.getProperty("os.name").startsWith("Windows")) {
        f01.setLineSeparator("\r\n"); // fichiers
        // DOS
        // (/r)
      } else {
        f01.setLineSeparator("\n");
      }
      int[] fmtI;
      fmtI = new int[] { 14, 14 };
      for (int i = 0; i < nbSegments; i++) {
        f01.stringField(0, _srd.pointsDiagrammePression[i].ordonneeMilieuSegment + "");
        f01.stringField(1, _srd.pointsDiagrammePression[i].pressionTotale + "");
        f01.writeFields(fmtI);
      }
      f01.flush();
      f01.close();
    } catch (final Exception e) {
      System.out.println(e);
    }
  }

  // Affichage d'une fenetre pour l'exploitation des r�sultats d'une �tude.
  protected void resultatInclinaison() {
    final SParametresDiapre spd = (SParametresDiapre) projet_.getParam(DiapreResource.PARAMETRES);
    final String SouD = spd.options.ligneGlissement;
    int nombreCouches;
    if (SouD.equals("D")) {
      nombreCouches = 1;
    } else {
      nombreCouches = spd.sol.nombreCouches;
    }
    tableauInclinaison_ = new DiapreTableauInclinaison(getApp(), nombreCouches, SouD);
    tableauInclinaison_.setProjet(projet_);
    tableauInclinaison_.setVisible(true);
  }

  protected void resultatPression() {
    final DiaprePrecision p = new DiaprePrecision(getApp(), getInformationsDocument());
    p.setResultatsProjet(projet_);
    p.setVisible(true);
    p.affiche();
  }

  protected void setGraph(final BGraphe graph) {
    graphique = graph;
  }

  protected void setNomGifRapp(final String nom) {
    nomGifRapp = nom;
  }

  protected void setNomGifResult(final String nom) {
    nomGifResult = nom;
  }

  protected void setNomHtml(final String nom) {
    nomHtml = nom;
  }

  // Capture du graphique de rappel des donn�es en image et codage en gif
  protected void photoRapp() {
    try {
      Image img;
      // if (graphique==null) System.out.println("graph null");
      img = graphique.getImageCache();
      if (img == null) {
        System.out.println("img null");
      }
      // fenetre de saisie du nom du fichier du .gif
      final DiapreNomGif dng = new DiapreNomGif(getApp(), 1);
      dng.setVisible(true);
      final FileOutputStream file = new FileOutputStream(nomGifRapp + ".gif");
      final AcmeGifEncoder age = new AcmeGifEncoder(img, file);
      age.encode();
      file.close();
    } catch (final Exception e) {
      System.out.println(e);
    }
  }

  // Capture du diagramme des pressions en image et codage en gif
  protected void photoResult() {
    try {
      Image img;
      // if (graphique==null) System.out.println("graph null");
      img = graphique.getImageCache();
      if (img == null) {
        System.out.println("img null");
      }
      // fenetre de saisie du nom du fichier du .gif
      final DiapreNomGif dng = new DiapreNomGif(getApp(), 2);
      dng.setVisible(true);
      final FileOutputStream file = new FileOutputStream(nomGifResult + ".gif");
      final AcmeGifEncoder age = new AcmeGifEncoder(img, file);
      age.encode();
      file.close();
    } catch (final Exception e) {
      System.out.println(e);
    }
  }

  // Enregistrement du rapport en html
  protected void html() {
    final double[] ytalus = new double[2];
    final SParametresDiapre params = fp_.getParametresDiapre();
    final DiapreFilleRappelDonnees rd = new DiapreFilleRappelDonnees(getApp());
    rd.construitRappelDonnees(params);
    rd.construitParametresGeneraux(params.parametresGeneraux);
    rd.construitSol(params.sol, ytalus, rd.nappeEau, rd.coteNappeEau);
    rd.construitEau(rd.typeCalcul, rd.nappeEau, rd.coteNappeEau);
    rd.construitSurcharges(params.surchargesProjet);
    try {
      final DiapreNomGif nom = new DiapreNomGif(getApp(), 3);
      nom.setVisible(true);
      System.out.println("Ecriture des parametres");
      final PrintWriter f01 = new PrintWriter(new FileWriter(nomHtml + ".html"));
      f01.write("<html> \n");
      f01.write("<head> \n");
      f01.write("<title>Rappel des donn&eacute;es</title></head>\n");
      f01.write("<body bgcolor=white>\n");
      f01.write("<center><h1>Rapport</h1></center>\n");
      f01.write("<br><br><br>");
      f01.write("<h1>I. Rappel des donn�es.</h1>\n");
      f01.write(rd.html1);
      f01.write(rd.html2);
      f01.write(rd.html3);
      f01.write(rd.html4);
      f01.write("<br>\n");
      f01.write("<h2>Graphique.</h2>\n");
      System.out.println(nomGifRapp);
      if (nomGifRapp == null) {
        f01.write("Cr�er le graphique pour l'afficher dans ce rapport.<br>\n");
      } else {
        f01.write("<img src=" + nomGifRapp + ".gif" + ">\n");
      }
      f01.write("<br><br><br>\n");
      f01.write("<h1>II. R�sultats.</h1>");
      if (nomGifResult == null) {
        f01
            .write("Lancer les calculs et cr�er le graphique pour l'afficher dans ce rapport.<br>\n");
      } else {
        f01.write("<img src=" + nomGifResult + ".gif" + ">\n");
      }
      f01.write("<br><br><br>\n");
      f01.write("</body> \n");
      f01.write("</html> \n");
      f01.close();
    } catch (final Exception e) {
      System.out.println(e);
    }
  }

  // Cr�ation et affichage du rappel des donn�es.
  protected void valeurs() {
    fRappelDonnees_ = new BuBrowserFrame(getImplementation());
    // fRappelDonnees_.putClientProperty ("NomFichier", null );
    fRappelDonnees_.setSize(650, 450);
    fRappelDonnees_.setTitle("Rappels des donn�es - rapport");
    addInternalFrame(fRappelDonnees_);
    activateInternalFrame(fRappelDonnees_);
    // SParametresDiapre params = fp_.getParametresDiapre();
    // utile ?
    fp_.getParametresDiapre();
    new DiapreFilleRappelDonnees(getApp());
    // fRappelDonnees_.setHtmlSource( rd.construitRappelDonnees( params ));
    final String os = System.getProperty("os.name");
    System.out.println(os);
    if (os.startsWith("Win")) {
      fRappelDonnees_.setDocumentUrl("file:/Fudaa-Diapre/" + nomHtml + ".html");
    } else {
      fRappelDonnees_.setDocumentUrl("file:/home/users/malafosse/devel/fudaa/sources/" + nomHtml + ".html");
    // new DiapreFilleRappelDonnees(getApp());
    }
  }

  protected void rappelDonnees() {
    // params=fp_.getParametresDiapre();
    new DiapreConstruitGrapheRappel(getApp(), getInformationsDocument(), (SParametresDiapre) projet_
        .getParam(DiapreResource.PARAMETRES), 0);
  }

  // Ajoute une fenetre.
  protected BuInternalFrame creerFenetreInterne() {
    final BuInternalFrame frame = new BuInternalFrame("Graphe", true, true, true, true);
    frame.setBackground(Color.white);
    frame.getContentPane().setLayout(new BorderLayout());
    frame.setBounds(200, 200, 200, 200);
    frame.setSize(400, 400);
    frame.setVisible(true);
    addInternalFrame(frame);
    return frame;
  }

  public void displayURL(String _url) {
    if ((_url == null) || (_url.length() == 0)) {
      _url = LOCAL_DIAPRE_MAN;
    }
    if ((_url.startsWith("file")) && (_url.endsWith("/"))) {
      _url = _url + "index.html";
    }
    if (BuPreferences.BU.getIntegerProperty("browser.type") == 1) {
      if (aide_ == null) {
        aide_ = new BuHelpFrame();
      }
      addInternalFrame(aide_);
      aide_.setDocumentUrl(_url);
    } else {
      if (_url == null) {
        final BuInformationsSoftware il = getInformationsSoftware();
        _url = il.http;
      }
      FudaaBrowserControl.displayURL(_url);
    }
  }

  public void contextHelp(final String _url) {
    String url = new String(_url);
    if ((url == null) || (url.length() == 0) || (url.startsWith("#"))) {
      url = "index.html";
    }
    super.contextHelp(url);
  }

  private void gestionnaireImpression(final String _commande) {
    final JInternalFrame frame = getCurrentInternalFrame();
    EbliPageable target = null;
    if (frame instanceof EbliPageable) {
      target = (EbliPageable) frame;
    } else if (frame instanceof EbliFillePrevisualisation) {
      target = ((EbliFillePrevisualisation) frame).getEbliPageable();
    } else {
      new BuDialogWarning(this, getInformationsSoftware(), FudaaLib.getS("Cette fen�tre n'est pas imprimable"))
          .activate();
      return;
    }
    if ("IMPRIMER".equals(_commande)) {
      cmdImprimer(target);
    } else if ("MISEENPAGE".equals(_commande)) {
      cmdMiseEnPage(target);
    } else if ("PREVISUALISER".equals(_commande)) {
      cmdPrevisualisation(target);
    }
  }

  public void cmdImprimer(final EbliPageable _target) {
    final PrinterJob printJob = PrinterJob.getPrinterJob();
    printJob.setPageable(_target);
    if (printJob.printDialog()) {
      try {
        printJob.print();
      } catch (final Exception PrintException) {
        PrintException.printStackTrace();
      }
    }
  }

  public void cmdMiseEnPage(final EbliPageable _target) {
    new EbliMiseEnPageDialog(_target, getApp(), getInformationsSoftware()).activate();
  }

  public void cmdPrevisualisation(final EbliPageable _target) {
    if (previsuFille_ == null) {
      previsuFille_ = new EbliFillePrevisualisation(getApp(), _target);
    }
    previsuFille_.setEbliPageable(_target);
    addInternalFrame(previsuFille_);
    try {
      previsuFille_.setMaximum(true);
    } catch (final java.beans.PropertyVetoException _e) {
      previsuFille_.setSize(100, 100);
    }
  }

  public void exit() {
    if (ouvrir) {
      final BuDialogConfirmation c = new BuDialogConfirmation(getApp(), isDiapre_, "Voulez-vous enregistrer le projet ?");
      if (c.activate() == JOptionPane.YES_OPTION) {
        enregistrer();
      }
    }
    super.exit();
  }

  private void tempConnecte() {
    if (CONNEXION_DIAPRE == null) {
      System.out.println("-- connexion nulle");
    } else {
      System.out.println("++ connexion non nulle");
    }
    if (SERVEUR_DIAPRE == null) {
      System.out.println("-- Serveur nul");
    } else {
      System.out.println("++ Serveur non nul");
    }
    if (!CONNEXION_DIAPRE.connecte()) {
      System.out.println("-- connexion non connect�e");
    } else {
      System.out.println("++ connexion connect�e");
    }
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return DiaprePreferences.DIAPRE;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    CONNEXION_DIAPRE = null;
    SERVEUR_DIAPRE = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_DIAPRE, CONNEXION_DIAPRE);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculDiapre.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculDiapre.class);
    CONNEXION_DIAPRE = c.getConnexion();
    SERVEUR_DIAPRE = ICalculDiapreHelper.narrow(c.getTache());
  }
}