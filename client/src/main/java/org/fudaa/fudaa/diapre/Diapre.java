/**
 * @file         Diapre.java
 * @creation     1999-10-01
 * @modification $Date: 2007-01-19 13:14:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.diapre;
import org.fudaa.fudaa.commun.impl.Fudaa;
/**
 * Permet de lancer l'application cliente de Diapre.
 *
 * @version      $Revision: 1.9 $ $Date: 2007-01-19 13:14:34 $ by $Author: deniger $
 * @author       Fouzia Elouafi , Jean de Malafosse
 */
public class Diapre {
  /**
   * Le main de Diapre class
   *
   * @param args   Les parametres
   */
  public static void main(final String[] args) {
    final Fudaa f=new Fudaa();
    f.launch(args,DiapreImplementation.informationsSoftware(),false);
    f.startApp(new DiapreImplementation());  }
}
